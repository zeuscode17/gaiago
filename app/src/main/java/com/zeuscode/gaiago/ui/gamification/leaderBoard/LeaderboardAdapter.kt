package com.zeuscode.gaiago.ui.gamification.leaderBoard

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.User

class LeaderboardAdapter(private val users: ArrayList<User>) :
    RecyclerView.Adapter<LeaderboardAdapter.MyViewHolder>() {
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        var position: TextView = view.findViewById(R.id.PlayerRatingTextView)
        var playerName: TextView = view.findViewById(R.id.PlayerNameTextView)
        var score: TextView = view.findViewById(R.id.PlayerScoreTextView)
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        // create a new view
        val textView = LayoutInflater.from(parent.context)
            .inflate(R.layout.player, parent, false)
        // set the view's size, margins, paddings and layout parameters

        return MyViewHolder(textView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.position.text = (position+1).toString()
        val name= "${users[users.size-position-1].name} ${users[users.size-position-1].surname}"
        holder.playerName.text = name
        holder.score.text = users[users.size-position-1].score.toString()


    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = users.size

}
