package com.zeuscode.gaiago.ui.vehicles.showVehicle

import com.zeuscode.gaiago.model.FirebaseModel

class ShowVehiclePresenter(private var view: ShowVehicleContract.View):ShowVehicleContract.Presenter {

    override fun removeVehicle(key: String) {
        FirebaseModel.deleteVehicle(key).addOnSuccessListener {
            view.onVehicleRemoved()
        }
    }
}
