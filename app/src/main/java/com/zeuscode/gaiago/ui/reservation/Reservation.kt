package com.zeuscode.gaiago.ui.reservation

import android.os.Parcel
import android.os.Parcelable

data class Reservation(
    var date: String = "",
    var startHour: Int = 0,
    var endHour: Int = 0,
    var userID: String = "",
    var vehicleID: String = "",
    var vehicleUri: String = "",
    var brandModel: String = "",
    var hourlyCost: Int = 0,
    var totalCost: Double = 0.0,
    var address: String = "",
    var ownerID: String = "",
    var key: String = "",
    var state: Int = 0,
    var userState : Int = 0,
    var ownerState : Int = 0,
    var isOwner: Boolean = false
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString()!!,
        source.readInt(),
        source.readInt(),
        source.readString()!!,
        source.readString()!!,
        source.readString()!!,
        source.readString()!!,
        source.readInt(),
        source.readDouble(),
        source.readString()!!,
        source.readString()!!,
        source.readString()!!,
        source.readInt(),
        source.readInt(),
        source.readInt(),
        1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(date)
        writeInt(startHour)
        writeInt(endHour)
        writeString(userID)
        writeString(vehicleID)
        writeString(vehicleUri)
        writeString(brandModel)
        writeInt(hourlyCost)
        writeDouble(totalCost)
        writeString(address)
        writeString(ownerID)
        writeString(key)
        writeInt(state)
        writeInt(userState)
        writeInt(ownerState)
        writeInt((if (isOwner) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Reservation> = object : Parcelable.Creator<Reservation> {
            override fun createFromParcel(source: Parcel): Reservation =
                Reservation(source)
            override fun newArray(size: Int): Array<Reservation?> = arrayOfNulls(size)
        }
    }
}
