package com.zeuscode.gaiago.ui.gamification.milestoneUnlock

import com.zeuscode.gaiago.R

data class Prize(
    var id: Int=0,
    var name: String="",
    var descr:String="",
    var value: Double=0.0,
    var image: Int=0)


val gift10 = Prize(1,"gift10", "Codice regalo 10 euro", 10.0, R.drawable.gift10)
val gift25 = Prize( 2,"gift25", "Codice regalo 25 euro", 25.0, R.drawable.gift25)
val gift50 = Prize( 3,"gift50", "Codice regalo 50 euro", 50.0, R.drawable.gift50)
val freeJourney=Prize( 4,"freeJourney", "Viaggio gratis con GaiaGo", 0.0, R.drawable.viaggio_gratis)
val amazonGift10=Prize( 5,"amazonGift10", "Codice regalo Amazon 10 euro", 10.0, R.drawable.amazonsconto2)
val discount10 = Prize( 6,"discount10", "Buono sconto 10 %", 10.0, R.drawable.gift10)
val discount25 = Prize( 7,"discount25", "Buono sconto 25 %", 25.0, R.drawable.gift25)
val discount50 = Prize( 8,"discount50", "Buono sconto 50 %", 50.0, R.drawable.gift50)
val discountZ10 = Prize( 9,"gift10", "Codice regalo Zalando 10 euro", 10.0, R.drawable.zalandosconto)
val discountC10 = Prize( 10,"gift10", "Codice regalo Carrefour 10 euro", 10.0, R.drawable.carrefoursconto)
val discountC15 = Prize( 11,"gift15", "Codice regalo Carrefour 15 euro", 15.0, R.drawable.gift25)