package com.zeuscode.gaiago.ui.gamification.miniGame

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel

class MiniGamePresenter(private var view:MiniGameContract.View) : MiniGameContract.Presenter {


    override fun loadMiniGameCar() {
        val valueEventListener= object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val miniGameCar:MiniGameCar
                if (snapshot.exists()) {
                    miniGameCar=snapshot.getValue(MiniGameCar::class.java)!!
                } else {
                    miniGameCar = MiniGameCar()
                    //saving
                    FirebaseModel.updateMiniGameCar(miniGameCar)
                }
                view.onMiniGameCarFetched(miniGameCar)
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }

        FirebaseModel.fetchMiniGameCar().addListenerForSingleValueEvent(valueEventListener)
    }

}