package com.zeuscode.gaiago.ui.vehicles.addVehicle

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.pickerDialogs.dialogs.BrandPickerDialogFragment
import com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces.BrandPickerListener
import kotlinx.android.synthetic.main.activity_add_vehicle.*
import kotlinx.android.synthetic.main.form_register_car.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place as GooglePlace
import com.zeuscode.gaiago.ui.main.MainActivity
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import com.zeuscode.gaiago.extensions.pickerDialogs.dialogs.ModelPickerDialogFragment
import com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces.ModelPickerListener
import kotlinx.android.synthetic.main.activity_add_vehicle.vehicleImageView



class AddVehicleActivity : AppCompatActivity(), AddVehicleContract.View {

    private lateinit var presenter : AddVehiclePresenter
    private val pickImage = 1
    private lateinit var vehicleID: String
    private lateinit var localPhotoUri : Uri
    private lateinit var downloadUri : Uri
    private var imageChosen=false
    private var imageUploaded=false
    private var latitude:Double? = null
    private var longitude:Double? = null
    private var placeDescription:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_vehicle)

        initView()

        // Initialize Places.
        Places.initialize(this, resources.getString(R.string.maps_api_key))
        // Create a new Places client instance.
        presenter= AddVehiclePresenter(this, Places.createClient(this))

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == pickImage && resultCode == Activity.RESULT_OK) {

            if (data!=null && data.data!=null) {
                imageChosen=true
                localPhotoUri=data.data!!

                presenter.onImageChosen(localPhotoUri)
            }
        }
    }

    override fun onImageUploaded(key:String, uri:Uri) {
        vehicleID=key
        vehicleImageView.setImageURI(localPhotoUri)
        downloadUri=uri
        imageUploaded=true
    }

    private fun saveCar(){

        val brand = brandEditText.text.toString()
        val model = modelEditText.text.toString()
        val year =  yearEditText.text.toString()
        val hourlyCost = costEditText.text.toString().toIntOrNull()

        if (brand.isNullOrBlank()) {
            showDialog("MARCA NON INSERITA", "Inserisci la marca del veicolo per continuare")
        } else if (model.isNullOrBlank()) {
            showDialog("MODELLO NON INSERITO", "Inserisci il modello del veicolo per continuare")
        } else if (year.isNullOrBlank()) {
            showDialog("ANNO D\'IMMATRICOLAZIONE NON INSERITO", "Inserisci l'anno d\'immatricolazione" +
                    " del veicolo per continuare")
        } else if (hourlyCost==null) {
            showDialog("COSTO ORARIO NON INSERITO", "Inserisci il costo orario per continuare")
        } else if (placeDescription.isNullOrBlank() || latitude==null || longitude==null
            || placesAutocomplete.text.toString().isNullOrBlank()) {
            showDialog("INDIRIZZO NON INSERITO", "Inserisci l\'indirizzo del veicolo per continuare")
        } else {
            val vehicle = Vehicle(
                brand,
                model,
                year,
                hourlyCost,
                downloadUri.toString(),
                "",
                vehicleID,
                placeDescription!!,
                latitude!!,
                longitude!!
            )

            presenter.uploadVehicle(vehicle)
        }
    }

    override fun onVehicleUploaded() {
        val intent=Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun initView() {
        initRegisterButton()
        initFabImagePicker()
        initBrandEditText()
        initModelEditText()
        initPlaceAutocomplete()
        initCostEdit()
    }

    private fun initRegisterButton() {
        registerButton.setOnClickListener {
            if (!imageChosen) {
                showDialog("IMMAGINE NON INSERITA","Caricare un\'immagine del veicolo per continuare")
            } else if (!imageUploaded){
                showDialog("IMMAGINE IN CARICAMENTO","L'immagine del veicolo è ancora in caricamento... Attendi")
            } else {
                saveCar()
            }
        }
    }

    private fun initFabImagePicker() {
        fabImagePicker.setOnClickListener {

            val getIntent = Intent(Intent.ACTION_GET_CONTENT)
            getIntent.type = "image/*"

            val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            pickIntent.type = "image/*"

            val chooserIntent = Intent.createChooser(getIntent, "Select Image")
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

            startActivityForResult(chooserIntent, pickImage)

        }
    }

    private fun initBrandEditText() {
        brandEditText.setOnClickListener {
            val fragment = BrandPickerDialogFragment()
            fragment.attach(brandPickerListener)
            fragment.show(supportFragmentManager, "dialog")
        }
    }

    private fun initModelEditText() {
        modelEditText.setOnClickListener {
            val fragment =
                ModelPickerDialogFragment(brandEditText.text.toString())
            fragment.attach(modelPickerListener)
            fragment.show(supportFragmentManager, "dialog")
        }
    }

    private fun initPlaceAutocomplete() {

        placesAutocomplete.setOnPlaceSelectedListener {
            presenter.fetchPlace(it)
        }
    }

    private fun initCostEdit() {

        val textWatcher= object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (!s.isNullOrBlank()) {
                    suggestedPrice.visibility= View.VISIBLE
                    val price = s.toString().toInt()
                    if (price < 5) {
                        suggestedPrice.text = "Prezzo ottimo."
                        suggestedPrice.setTextColor(Color.argb(255,55,104,99))

                    } else if (price > 10) {
                        suggestedPrice.text = "Prezzo sopra la media."
                        suggestedPrice.setTextColor(Color.RED)

                    } else {
                        suggestedPrice.text = "Prezzo nella media."
                        suggestedPrice.setTextColor(Color.argb(225,255,102,0))
                    }
                } else
                    suggestedPrice.visibility= View.INVISIBLE
            }
        }

        costEditText.addTextChangedListener(textWatcher)
    }


    override fun onPlaceFetched(place: GooglePlace) {
        latitude = place.latLng?.latitude
        longitude = place.latLng?.longitude
        placeDescription= place.address
    }

    private val brandPickerListener = object:
        BrandPickerListener {
        override fun onBrandPicked(brand: String) {
            brandEditText.setText(brand, TextView.BufferType.EDITABLE)
        }
    }

    private val modelPickerListener = object:
        ModelPickerListener {
        override fun onModelPicked(model: String) {
            modelEditText.setText(model, TextView.BufferType.EDITABLE)
        }
    }

    private fun showDialog(title:String, message:String) {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setNegativeButton(android.R.string.ok, null)
            .create()
            .show()

    }

}
