package com.zeuscode.gaiago.ui.reservation

import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.twoDigitsString

class ReservationsRecyclerViewAdapter(private val myReservations: ArrayList<Reservation>) :
    RecyclerView.Adapter<ReservationsRecyclerViewAdapter.MyViewHolder>() {
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        var upperBrandModelTextView: TextView = view.findViewById(R.id.upperBrandModelTextView)
        var confirmationTextView: TextView = view.findViewById(R.id.confirmationTextView)
        var dateTextView: TextView = view.findViewById(R.id.dateTextView)
        var hoursTextView: TextView = view.findViewById(R.id.hoursTextView)
        var imageView: ImageView = view.findViewById(R.id.vehicleImageView)

    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.booking, parent, false)
        // set the view's size, margins, paddings and layout parameters

        return MyViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        holder.upperBrandModelTextView.text = myReservations[position].brandModel

        holder.confirmationTextView.let {
            when (myReservations[position].state) {
                0 -> {
                    it.text = "In attesa di conferma"
                    it.setTextColor(Color.parseColor("#FFFFFF"))
                }
                1 -> {
                    it.text = "Confermata"
                    it.setTextColor(Color.parseColor("#074F1F"))
                }
                2-> {
                    it.text = "Cancellata"
                    it.setTextColor(Color.parseColor("#910A0E"))
                }
                3 -> {
                    it.text = "In corso"
                    it.setTextColor(Color.parseColor("#074F1F"))
                }
                4 -> {
                    it.text = "Terminata"
                    it.setTextColor(Color.parseColor("#074F1F"))
                }
                else -> {
                }
            }

        }

        val date="${myReservations[position].date.substring(0,2)}/${myReservations[position].date.substring(2,4)}/${myReservations[position].date.substring(4,8)}"
        holder.dateTextView.text=date
        val hours="${intToHour(myReservations[position].startHour)}-${intToHour(myReservations[position].endHour)}"
        holder.hoursTextView.text=hours
        //Uso picasso per scaricare l'immagine ed inserirla nell'imageView
        Picasso.get().load(Uri.parse(myReservations[position].vehicleUri)).into(holder.imageView)

    }

    private fun intToHour(interval:Int) :String {
        val hour=(interval/4).twoDigitsString()
        val minute=((interval%4)*15).twoDigitsString()
        return "$hour:$minute"
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myReservations.size
}