package com.zeuscode.gaiago.ui.license

interface LicenseRegistrationListener {
    fun onLicenseNumberSubmitted(number: String)
    fun onLicenseDatesSubmitted(releaseDate: Long, expirationDate: Long)
    fun onLicensePicturesSubmitted()
}
