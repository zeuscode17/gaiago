package com.zeuscode.gaiago.ui.profile.editDialogFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.User
import kotlinx.android.synthetic.main.fragment_edit_birth_date_dialog.*
import kotlinx.android.synthetic.main.fragment_edit_birth_date_dialog.view.*
import java.util.*

class EditBirthDateDialogFragment(private var user: User?) : EditProfileDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_edit_birth_date_dialog, container, false)

        user?.let { user ->
            user.birthDate?.let { birthDate ->
                val calendar = Calendar.getInstance()
                calendar.timeInMillis = birthDate
                view.datePicker.updateDate(
                        calendar[Calendar.YEAR],
                        calendar[Calendar.MONTH],
                        calendar[Calendar.DAY_OF_MONTH]
                )
            }

        }

        view.okButton.setOnClickListener {
            val newBirthDate=calendarFromDatePicker().timeInMillis
            user?.birthDate=newBirthDate
            presenter?.updateBirthDate(newBirthDate)
            this.dismiss()
        }

        view.cancelButton.setOnClickListener {
            this.dismiss()
        }

        return view
    }

    private fun calendarFromDatePicker(): Calendar {
        val dayOfMonth = datePicker.dayOfMonth
        val month = datePicker.month
        val year = datePicker.year

        val calendar = Calendar.getInstance()

        calendar.set(year, month, dayOfMonth)

        return calendar
    }

}
