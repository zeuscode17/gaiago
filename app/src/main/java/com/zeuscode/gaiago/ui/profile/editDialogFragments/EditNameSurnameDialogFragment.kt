package com.zeuscode.gaiago.ui.profile.editDialogFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.User
import kotlinx.android.synthetic.main.fragment_edit_name_surname_dialog.*
import kotlinx.android.synthetic.main.fragment_edit_name_surname_dialog.view.*

class EditNameSurnameDialogFragment(private var user: User?) : EditProfileDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_edit_name_surname_dialog, container, false)


        view.nameEditText.setText(user?.name)
        view.surnameEditText.setText(user?.surname)

        view.okButton.setOnClickListener {
            val newName=nameEditText.text.toString()
            val newSurname=surnameEditText.text.toString()
            user?.name=newName
            user?.surname=newSurname
            presenter?.updateNameSurname(newName, newSurname)
            this.dismiss()
        }

        view.cancelButton.setOnClickListener {
            this.dismiss()
        }

        return view
    }

}
