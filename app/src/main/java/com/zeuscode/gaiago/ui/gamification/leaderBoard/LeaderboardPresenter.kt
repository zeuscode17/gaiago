package com.zeuscode.gaiago.ui.gamification.leaderBoard

import android.content.ContentValues
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.User

class LeaderboardPresenter(private val view: LeaderboardContract.View) : LeaderboardContract.Presenter {

    override fun loadPlayers(users: ArrayList<User>) {
        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //In caso di dati cambiati, svuoto l'arraylist
                users.clear()

                for (singleSnapshot in dataSnapshot.children) {
                    //Create an object for each player
                    val user = singleSnapshot.getValue(User::class.java)
                    //add player to array
                    users.add(user!!)
                }

                view.notifyAdapter()
            }

            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {

                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }
        //aggiungo alla reference il listener
        FirebaseModel.fetchUsersByScore().addListenerForSingleValueEvent(valueEventListener)

    }

}