package com.zeuscode.gaiago.ui.license.dates

import com.zeuscode.gaiago.ui.license.LicenseRegistrationListener
import com.zeuscode.gaiago.utils.DateUtils
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class LicenseDatesPresenter(private var view: LicenseDatesContract.View) : LicenseDatesContract.Presenter {

    override fun onDatesSubmitted(releaseDate: String, expirationDate: String, listener: LicenseRegistrationListener) {
        if (releaseDate.isEmpty()) {
            view.showEmptyLicenseReleaseDate()
            return
        }

        if (expirationDate.isEmpty()) {
            view.showEmptyLicenseExpirationDate()
            return
        }

        val releaseDateLong = toLong(releaseDate)
        val expirationDateLong = toLong(expirationDate)

        listener.onLicenseDatesSubmitted(releaseDateLong, expirationDateLong)
    }

    private fun toLong(stringDate: String): Long {
        val f = SimpleDateFormat("dd/MM/yyyy", Locale.ITALY)
        try {
            val d = f.parse(stringDate)
            return d.time
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return 0
    }

}
