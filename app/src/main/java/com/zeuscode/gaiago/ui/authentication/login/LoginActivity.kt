package com.zeuscode.gaiago.ui.authentication.login

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.authentication.registration.RegistrationActivity
import com.zeuscode.gaiago.extensions.KeyboardVisibilityListener
import com.zeuscode.gaiago.extensions.setKeyboardVisibilityListener
import com.zeuscode.gaiago.ui.authentication.Credentials
import com.zeuscode.gaiago.ui.authentication.validators.Error
import com.zeuscode.gaiago.ui.gamification.dailyRewards.DailyRewardsActivity
import com.zeuscode.gaiago.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.form_login.*


class LoginActivity : Activity(), LoginContract.View, KeyboardVisibilityListener {

    private lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter = LoginPresenter(this)

        initView()
    }

    override fun initView() {
        //imposto listener su bottone di login
        loginButton.setOnClickListener {
            val credentials = extractCredentials()
            presenter.login(credentials)

        }

        //listener su bottone "Registrati", si viene rimandati all'activity di registrazione
        createAccountButton.setOnClickListener {
            val intent = Intent(this, RegistrationActivity::class.java)
            startActivity(intent)
        }

        passwordForgottenTextView.setOnClickListener {
            resetPassword()

        }

        setKeyboardVisibilityListener(this)
    }

    //estrae credenziali dagli editText
    private fun extractCredentials(): Credentials {
        val username = usernameEditText.text.toString().trim()
        val password = passwordEditText.text.toString().trim()
        return Credentials(username, password)
    }

    private fun resetPassword() {
        val dialogView = layoutInflater.inflate(R.layout.dialog_edit_text, null)
        val textField = dialogView.findViewById(R.id.alertTextField) as EditText
        textField.setHint(R.string.email)
        val builder = AlertDialog.Builder(this)
            .setTitle("Inserisci l\'email")
            .setView(dialogView)
            .setPositiveButton("Resetta password") { _, _ ->
                val text = textField.text.toString()
                presenter.sendResetEmail(text)
            }
            .setNeutralButton(R.string.cancel, null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    override fun onAuthenticationSuccess() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    //AlertDialog in caso di errore nel login
    override fun onAuthenticationError(error: Error) {
        AlertDialog.Builder(this)
            .setTitle(error.title)
            .setMessage(error.message)
            .setNegativeButton(android.R.string.ok, null)
            .create()
            .show()
    }

    override fun onResetSent() {
        AlertDialog.Builder(this)
            .setTitle("Controlla la tua email")
            .setMessage("Ti abbiamo inviato una mail per il reset della password!")
            .setNegativeButton(android.R.string.ok, null)
            .create()
            .show()
    }

    //listener sulla visibilità della tastiera
    override fun onKeyboardVisibilityChanged(keyboardVisible: Boolean) {
        if (keyboardVisible) {
            scrollView.scrollY = 100000
        }
    }

    override fun onAuthenticationFailure() {
        AlertDialog.Builder(this)
            .setTitle("Combinazione mail/password errata")
            .setMessage("Mail o password errate, riprovare")
            .setNegativeButton(android.R.string.ok, null)
            .create()
            .show()
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setTitle("Vuoi uscire dall\'applicazione?")
            .setMessage("Conferma per uscire dall\'applicazione")
            .setPositiveButton(R.string.confirm) { _, _ ->
                finishAffinity()
                finish()}
            .setNegativeButton(R.string.no, null)
            .create().show()
    }


}




