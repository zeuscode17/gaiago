package com.zeuscode.gaiago.ui.license

import android.content.ContentValues
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel

class LicenseRegistrationPresenter(private var view: LicenseRegistrationContract.View) : LicenseRegistrationContract.Presenter {

    private var licenseNumber: String? = null
    private var releaseDate: Long? = null
    private var expirationDate: Long? = null

    override fun onLicenseNumberSubmitted(licenseNumber: String) {
        this.licenseNumber = licenseNumber
        view.showLicenseDatesRegistration()
    }

    override fun onLicenseDatesSubmitted(releaseDate: Long, expirationDate: Long) {
        this.releaseDate = releaseDate
        this.expirationDate = expirationDate
        view.showLicensePicturesRegistration()
    }

    override fun onLicensePicturesSubmitted() {
        val license = License(licenseNumber!!, releaseDate!!, expirationDate!!)
        loadProgressBar()
        FirebaseModel.saveLicense(license)
        view.showMainActivity()
    }

    private fun loadProgressBar() {
        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI

                val progressBar=dataSnapshot.value.toString().toCharArray()
                progressBar[3]='1'
                FirebaseModel.editProgressBar(progressBar.joinToString(""))
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(ContentValues.TAG, "loadPost:onCancelled", databaseError.toException())

            }
        }

        FirebaseModel.fetchProgressBar().addListenerForSingleValueEvent(valueEventListener)
    }

}
