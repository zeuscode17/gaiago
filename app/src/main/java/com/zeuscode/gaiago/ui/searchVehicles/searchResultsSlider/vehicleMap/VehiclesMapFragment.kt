package com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider.vehicleMap

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.zeuscode.gaiago.R
import androidx.core.content.ContextCompat
import androidx.annotation.NonNull
import android.content.pm.PackageManager
import android.net.Uri
import android.util.Log
import android.view.Gravity
import android.widget.PopupWindow
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getSystemService
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import com.zeuscode.gaiago.ui.reservation.Reservation
import com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider.availableVehicles.selectedVehicle.SelectedVehicleActivity
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import kotlinx.android.synthetic.main.vehicle_popup.*
import kotlinx.android.synthetic.main.vehicle_popup.view.*

class VehiclesMapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    VehiclesMapContract.View {

    private lateinit var mapView: MapView
    private lateinit var map: GoogleMap
    private val permissionRequestAccessFineLocation = 1
    private var vehicles = ArrayList<Vehicle>()
    private lateinit var requestedLocation:LatLng
    private lateinit var reservation: Reservation

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.fragment_vehicles_map, container, false)

        mapView = view.findViewById(R.id.mapView)
        mapView.onCreate(savedInstanceState)

        mapView.getMapAsync(this)

        return view
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(requestedLocation, 15.0f))
        map.uiSettings.isMyLocationButtonEnabled=true
        googleMap.setOnMarkerClickListener(this)
        loadUserLocation()
    }

    override fun onMarkerClick(marker: Marker): Boolean {

        val i=marker.tag.toString().toInt()

        val window = PopupWindow(requireContext())

        // Initialize a new layout inflater instance
        val inflater: LayoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        // Inflate a custom view using layout inflater
        val popview = inflater.inflate(R.layout.vehicle_popup,null)

        Picasso.get().load(Uri.parse(vehicles[i].uri)).into(popview.vehicleImgView)
        val brandModel="${vehicles[i].brand} ${vehicles[i].model}"
        popview.brandModelTextView.text=brandModel

        window.contentView=popview
        window.showAtLocation(view, Gravity.CENTER,0,0)

        popview.closePopUpButton.setOnClickListener {
            window.dismiss()
        }

        popview.showVehicleButton.setOnClickListener {
            val intent= Intent(requireContext(), SelectedVehicleActivity::class.java)
            intent.putExtra("VEHICLE",vehicles[i])
            intent.putExtra("RESERVATION", reservation)
            startActivity(intent)
        }


        return false
    }

    private fun loadUserLocation() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            map.isMyLocationEnabled=true
        } else getLocationPermission()
    }


    private fun getLocationPermission() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
            permissionRequestAccessFineLocation
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        when (requestCode) {
            permissionRequestAccessFineLocation -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadUserLocation()
                }
            }
        }
    }


    fun updateVehicleList(vehicles:ArrayList<Vehicle>, reservation: Reservation) {
        this.vehicles.addAll(vehicles)
        this.reservation=reservation
    }

    fun moveCamera(latitude:Double, longitude:Double) {
        requestedLocation= LatLng(latitude,longitude)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isResumed && isVisibleToUser)
            addMarkers()
    }

    private fun addMarkers() {
        for (i in 0 until vehicles.size) {
            val marker=map.addMarker(MarkerOptions().position(LatLng(vehicles[i].latitude, vehicles[i].longitude)))
            marker.tag=i
        }
    }


    override fun onResume() {
        mapView.onResume()
        super.onResume()
    }


    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

}