package com.zeuscode.gaiago.ui.gamification.miniGame.changeCar

interface ChangeCarContract {
    interface View{
        fun onLevelFetched(level:Int)
    }

    interface Presenter{
        fun fetchUserLevel()
    }
}