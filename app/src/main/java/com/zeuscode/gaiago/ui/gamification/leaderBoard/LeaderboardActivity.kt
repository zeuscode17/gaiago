package com.zeuscode.gaiago.ui.gamification.leaderBoard

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.app.AppCompatActivity
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.User
import kotlinx.android.synthetic.main.activity_leaderboard.*

class LeaderboardActivity : AppCompatActivity(), LeaderboardContract.View {

    private lateinit var presenter: LeaderboardPresenter
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var users: ArrayList<User>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leaderboard)

        presenter=LeaderboardPresenter(this)

        users=ArrayList()
        viewAdapter= LeaderboardAdapter(users)
        setupRecyclerView()

        presenter.loadPlayers(users)
    }

    private fun setupRecyclerView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = viewAdapter
            setHasFixedSize(true)
        }
    }

    override fun notifyAdapter() {
        viewAdapter.notifyDataSetChanged()
    }



}