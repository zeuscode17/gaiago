package com.zeuscode.gaiago.ui.license.pictures

import com.zeuscode.gaiago.ui.license.LicenseRegistrationListener

interface LicensePicturesContract {

    interface View {
        fun showNoneLicenseFrontPicture()
        fun showNoneLicenseBackPicture()
        fun deleteTemporaryFiles()
        fun onUploadError()
    }

    interface Presenter {
        fun onPicturesSubmitted(frontPicturePath: String?, backPicturePath: String?, listener: LicenseRegistrationListener)
    }
}