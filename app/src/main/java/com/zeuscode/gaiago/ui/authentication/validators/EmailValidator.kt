package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.authentication.Credentials
import com.zeuscode.gaiago.ui.authentication.validators.ValidationError.EMAIL_NOT_VALID
import com.zeuscode.gaiago.ui.authentication.validators.ValidationError.EMPTY_EMAIL
import java.util.regex.Pattern

class EmailValidator :
    Validator<Credentials> {

    companion object {
        private val EMAIL_ADDRESS_PATTERN = Pattern.compile(
                "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                        "(" +
                        "\\." +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                        ")+"
        )
    }

    override fun validate(item: Credentials): ValidationError? {
        val username = item.username

        if (username.isEmpty()) {
            return EMPTY_EMAIL
        }

        if (!isValid(username)) {
            return EMAIL_NOT_VALID
        }

        return null
    }

    private fun isValid(email: String): Boolean {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches()
    }

}