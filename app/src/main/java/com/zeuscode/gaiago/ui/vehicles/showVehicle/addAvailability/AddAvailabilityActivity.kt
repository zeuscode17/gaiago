package com.zeuscode.gaiago.ui.vehicles.showVehicle.addAvailability

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.squareup.picasso.Picasso
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.main.MainActivity
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import com.zeuscode.gaiago.ui.vehicles.showVehicle.vehicleAvailabilities.VehicleAvailabilitiesActivity
import kotlinx.android.synthetic.main.activity_add_available_time_slot.*
import kotlinx.android.synthetic.main.activity_add_available_time_slot.vehicleImageView
import kotlinx.android.synthetic.main.fragment_add_available_time_slot_tabs.*

class AddAvailabilityActivity : AppCompatActivity(), AddAvailabilityContract.View {

    private lateinit var presenter:AddAvailabilityPresenter
    private val setFixedAvailabilityFragment = SetFixedAvailabilityFragment()
    private val setRepetitionFragment = SetRepetitionFragment()
    private lateinit var vehicle: Vehicle
    private lateinit var addAvailabilityPagerAdapter: AddAvailabilityPagerAdapter
    private lateinit var viewPager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_available_time_slot)

        vehicle=intent?.extras?.getParcelable("VEHICLE")!!

        initView()

        presenter= AddAvailabilityPresenter(this)

    }

    override fun initView() {
        Picasso.get().load(Uri.parse(vehicle.uri)).into(vehicleImageView)
        val brandModel="${vehicle.brand} ${vehicle.model}"
        brandModelTextView.text=brandModel
        upperBrandModelTextView.text=brandModel

        initPager()

        setAddAvailabilityButton()
    }

    private fun initPager(){
        addAvailabilityPagerAdapter = AddAvailabilityPagerAdapter(supportFragmentManager)
        viewPager = findViewById(R.id.pager)
        viewPager.adapter = addAvailabilityPagerAdapter
        tabLayout.setupWithViewPager(viewPager)
    }


    private fun setAddAvailabilityButton() {
        addAvailabilityButton.setOnClickListener {
            if (viewPager.currentItem==0) {
                addFixedAvailability()
            } else {
                addRepeatedAvailability()
            }
        }
    }

    private fun addFixedAvailability() {
        val date=setFixedAvailabilityFragment.bookingDate
        val startHour=setFixedAvailabilityFragment.bookingStartHour
        val endHour=setFixedAvailabilityFragment.bookingEndHour

        if (date.isNullOrBlank()) {
            showDialog("DATA NON INSERITA", "Inserisci la data per continuare")
        } else if (startHour==null) {
            showDialog("ORA DI INIZIO NON INSERITA", "Inserisci l\'ora di inizio per continuare")
        } else if (endHour==null) {
            showDialog("ORA DI FINE NON INSERITA", "Inserisci l\'ora di fine per continuare")
        } else if (startHour>=endHour) {
            showDialog("ORARI ERRATI", "L\'ora di inizio non può essere maggiore dell\'ora di fine")
        } else
            presenter.addFixedAvailability(vehicle, date, startHour, endHour)
    }

    private fun addRepeatedAvailability() {
        val startHour=setRepetitionFragment.bookingStartHour
        val endHour=setRepetitionFragment.bookingEndHour
        val week=setRepetitionFragment.week
        val repeatFor=setRepetitionFragment.spinner.selectedItemPosition

        if (startHour==null) {
            showDialog("ORA DI INIZIO NON INSERITA", "Inserisci l\'ora di inizio per continuare")
        } else if (endHour==null) {
            showDialog("ORA DI FINE NON INSERITA", "Inserisci l\'ora di fine per continuare")
        } else if (startHour>=endHour) {
            showDialog("ORARI ERRATI", "L\'ora di inizio non può essere maggiore dell\'ora di fine")
        }
        else if (!week.containsValue(true)) {
            showDialog("NESSUN GIORNO INSERITO", "Inserisci almeno un giorno per continuare")
        } else
            presenter.addRepeatedAvailability(vehicle, startHour, endHour, week, repeatFor)

    }

    override fun onAvailabilityUpdated() {
        AlertDialog.Builder(this)
            .setTitle("Disponibilità inserita")
            .setMessage("Inserimento avvenuto correttamente")
            .setPositiveButton("OK") { _, _ ->
                showVehicleAvailabilitiesActivity()
            }
            .create()
            .show()
    }

    private fun showVehicleAvailabilitiesActivity() {
        val intent= Intent(this, VehicleAvailabilitiesActivity::class.java)
        intent.putExtra("VEHICLE", vehicle)
        startActivity(intent)
    }

    inner class AddAvailabilityPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getCount(): Int  = 2

        override fun getItem(i: Int): Fragment {
            if (i==0)
            {
                return setFixedAvailabilityFragment
            }
            return setRepetitionFragment
        }

        override fun getPageTitle(position: Int): CharSequence {
            if (position == 0)
                return "SINGOLA"
            return "RIPETUTA"
        }
    }


    private fun showDialog(title:String, message:String) {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setNegativeButton(android.R.string.ok, null)
            .create()
            .show()

    }

}
