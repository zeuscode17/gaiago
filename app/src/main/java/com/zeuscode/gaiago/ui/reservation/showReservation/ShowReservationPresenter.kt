package com.zeuscode.gaiago.ui.reservation.showReservation

import android.content.ContentValues
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.reservation.Reservation
import com.zeuscode.gaiago.ui.vehicles.Vehicle

class ShowReservationPresenter(private var view: ShowReservationContract.View) : ShowReservationContract.Presenter {

    private lateinit var valueEventListener:ValueEventListener

    override fun editReservationStatus(key: String, status:Int) {
        FirebaseModel.editReservationStatus(key,status).addOnSuccessListener {
            view.initReservationStatus()
        }
    }

    override fun loadReservationStatus(key: String) {
        valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //In caso di dati cambiati, svuoto l'arraylist
                view.onNewStatusFetched(dataSnapshot.value.toString().toInt())

            }
                //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        FirebaseModel.fetchReservationStatus(key).addValueEventListener(valueEventListener)
    }

    override fun stopListener(key:String) {
        FirebaseModel.fetchReservationStatus(key).removeEventListener(valueEventListener)
    }


    override fun restoreAvailability(reservation: Reservation) {
        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //In caso di dati cambiati, svuoto l'arraylist
                val newAvailability=setDayAvailability(dataSnapshot.value.toString(),
                    reservation.startHour,reservation.endHour)
                FirebaseModel.addVehicleAvailability(reservation.vehicleID,reservation.date,newAvailability)
            }
            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        FirebaseModel.fetchSingleAvailability(reservation.vehicleID, reservation.date)
            .addListenerForSingleValueEvent(valueEventListener)

    }

    private fun setDayAvailability(dayAvailability:String, startHour:Int, endHour:Int):String {
        val availabilityLength=endHour-startHour
        return dayAvailability.replaceRange(startHour,endHour,"0".repeat(availabilityLength))
    }

    override fun addUserRating(userID: String, rating: Float) {
        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
               if (dataSnapshot.hasChild("rating") && dataSnapshot.hasChild("numRating")) {
                   var userRating = dataSnapshot.child("rating").value.toString().toFloat()
                   var numRating=dataSnapshot.child("numRating").value.toString().toInt()
                   userRating=((userRating*numRating)+rating)/(numRating+1)
                   numRating++
                   FirebaseModel.addUserRating(userID,userRating,numRating)
               } else {
                   FirebaseModel.addUserRating(userID,rating,1)
               }
            }
            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        FirebaseModel.fetchUserByID(userID).addListenerForSingleValueEvent(valueEventListener)
    }

    override fun addVehicleRating(vehicleID: String, rating: Float) {
        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.hasChild("rating") && dataSnapshot.hasChild("numRating")) {
                    var userRating = dataSnapshot.child("rating").value.toString().toFloat()
                    var numRating=dataSnapshot.child("numRating").value.toString().toInt()
                    userRating=((userRating*numRating)+rating)/(numRating+1)
                    numRating++
                    FirebaseModel.addVehicleRating(vehicleID,userRating,numRating)
                } else {
                    FirebaseModel.addVehicleRating(vehicleID,rating,1)
                }
            }
            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        FirebaseModel.fetchVehicleByID(vehicleID).addListenerForSingleValueEvent(valueEventListener)
    }

    override fun editOwnerStatus(key: String, status: Int) {
        FirebaseModel.editOwnerStatus(key, status)
        view.initReservationStatus()
    }

    override fun editUserStatus(key: String, status: Int) {
        FirebaseModel.editUserStatus(key, status)
        view.initReservationStatus()
    }


    override fun addXPpoints(userID: String, ownerID: String) {
        val scoreUserEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var score=dataSnapshot.value.toString().toInt()
                score+=10
                FirebaseModel.setScoreByID(userID,score)

            }
            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        val scoreOwnerEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var score=dataSnapshot.value.toString().toInt()
                score+=10
                FirebaseModel.setScoreByID(ownerID,score)
                updateProgressBar(userID)
            }
            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        FirebaseModel.fetchScoreByID(userID).addListenerForSingleValueEvent(scoreUserEventListener)
        FirebaseModel.fetchScoreByID(ownerID).addListenerForSingleValueEvent(scoreOwnerEventListener)

    }

    override fun updateProgressBar(userID: String) {

        val progressBarEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val progressBar=dataSnapshot.value.toString().toCharArray()
                if (progressBar[5]=='0') {
                    progressBar[5]='1'
                    FirebaseModel.editProgressBarByID(userID, progressBar.joinToString(""))
                }
            }
            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        FirebaseModel.fetchProgressBarById(userID).addListenerForSingleValueEvent(progressBarEventListener)
    }

}
