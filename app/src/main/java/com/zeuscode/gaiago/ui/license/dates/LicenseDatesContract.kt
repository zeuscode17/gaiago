package com.zeuscode.gaiago.ui.license.dates

import com.zeuscode.gaiago.ui.license.LicenseRegistrationListener

interface LicenseDatesContract {

    interface View {
        fun showEmptyLicenseReleaseDate()
        fun showEmptyLicenseExpirationDate()
    }

    interface Presenter {
        fun onDatesSubmitted(releaseDate: String, expirationDate: String, listener: LicenseRegistrationListener)
    }

}