package com.zeuscode.gaiago.ui.vehicles.vehicleList

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.vehicles.Vehicle

class VehiclesRecyclerViewAdapter(private val myCars: ArrayList<Vehicle>) :
    RecyclerView.Adapter<VehiclesRecyclerViewAdapter.MyViewHolder>() {
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        var textView1: TextView = view.findViewById(R.id.upperBrandModelTextView)
        var textView2: TextView = view.findViewById(R.id.brandModelTextView)
        var imageView: ImageView = view.findViewById(R.id.vehicleImageView)
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.vehicle, parent, false)
        // set the view's size, margins, paddings and layout parameters

        return MyViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.textView1.text = myCars[position].brand
        holder.textView2.text= myCars[position].model
        //Uso picasso per scaricare l'immagine ed inserirla nell'imageView
        Picasso.get().load(Uri.parse(myCars[position].uri)).into(holder.imageView)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myCars.size
}