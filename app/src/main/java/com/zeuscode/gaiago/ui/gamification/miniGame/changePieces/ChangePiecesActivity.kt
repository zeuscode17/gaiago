package com.zeuscode.gaiago.ui.gamification.miniGame.changePieces

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.gamification.miniGame.MiniGameActivity
import com.zeuscode.gaiago.ui.gamification.miniGame.MiniGameCar
import kotlinx.android.synthetic.main.activity_change_pieces.*

class ChangePiecesActivity : AppCompatActivity(), ChangePiecesContract.View {

    private lateinit var presenter:ChangePiecesPresenter
    private lateinit var miniGameCar: MiniGameCar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_pieces)

        presenter= ChangePiecesPresenter(this)

        miniGameCar=intent?.extras?.getParcelable<MiniGameCar>("MINIGAMECAR")!!

        initCar(miniGameCar)

        initSaveChangesButton()

        presenter.fetchDailyRewardIndex()
    }

    private fun initCar(miniGameCar: MiniGameCar) {
        when (miniGameCar.color) {
            "Red"->redRadioButton.isChecked=true
            "Silver"->silverRadioButton.isChecked=true
            "Yellow"->yellowRadioButton.isChecked=true
        }
        when (miniGameCar.headlights) {
            0->alogenRadioButton.isChecked=true
            1->xenoRadioButton.isChecked=true
        }
        when(miniGameCar.rims) {
            0->basicRimsRadioButton.isChecked=true
            1->premiumRimsRadioButton.isChecked=true
            2->professionalRimsRadioButton.isChecked=true
        }
        when(miniGameCar.motor) {
            0->basicMotorRadioButton.isChecked=true
            1->premiumMotorRadioButton.isChecked=true
            2->professionalMotorRadioButton.isChecked=true
        }
        when(miniGameCar.transmission) {
            0->basicTrasmissionRadioButton.isChecked=true
            1->professionalTransmissionRadioButton.isChecked=true
        }
        when(miniGameCar.suspensions) {
            0->basicsuspensionsRadioButton.isChecked=true
            1->professionalsuspensionsRadioButton.isChecked=true
        }
        when(miniGameCar.exhaust) {
            0->basicexhaustRadioButton.isChecked=true
            1->professionalexhaustRadioButton.isChecked=true
        }
    }

    private fun initSaveChangesButton() {
        saveChangesButton.setOnClickListener {
            miniGameCar.color =
                when (colorRadioGroup.checkedRadioButtonId) {
                    redRadioButton.id-> "Red"
                    silverRadioButton.id-> "Silver"
                    else -> "Yellow"
                }
            miniGameCar.headlights=
                when (headlightsRadioGroup.checkedRadioButtonId) {
                    alogenRadioButton.id -> 0
                    else -> 1
                }
            miniGameCar.rims=
                when (rimsRadioGroup.checkedRadioButtonId) {
                    basicRimsRadioButton.id->0
                    premiumRimsRadioButton.id->1
                    else -> 2
                }
            miniGameCar.motor=
                when (motorRadioGroup.checkedRadioButtonId) {
                    basicMotorRadioButton.id ->0
                    premiumMotorRadioButton.id ->1
                    else -> 2
                }

            miniGameCar.transmission=
                when (trasmissionRadioGroup.checkedRadioButtonId) {
                    basicTrasmissionRadioButton.id ->0
                    else -> 1
                }

            miniGameCar.suspensions=
                when (suspensionRadioGroup.checkedRadioButtonId) {
                    basicsuspensionsRadioButton.id ->0
                    else ->1
                }

            miniGameCar.exhaust=
                when (exhaustRadioGroup.checkedRadioButtonId) {
                    basicexhaustRadioButton.id->0
                    else -> 1
                }

            FirebaseModel.updateMiniGameCar(miniGameCar)
            val intent= Intent(this, MiniGameActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onIndexFetched(index: Int) {
        if (index<8) {
            professionalRimsRadioButton.isEnabled=false
            professionalexhaustRadioButton.isEnabled=false
            professionalsuspensionsRadioButton.isEnabled=false
            professionalMotorRadioButton.isEnabled=false
        } else if (index<32) {
            professionalexhaustRadioButton.isEnabled=false
            professionalsuspensionsRadioButton.isEnabled=false
            professionalMotorRadioButton.isEnabled=false
        } else if (index<52) {
            professionalsuspensionsRadioButton.isEnabled=false
            professionalMotorRadioButton.isEnabled=false
        } else if (index<60) {
            professionalMotorRadioButton.isEnabled=false
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent=Intent(this, MiniGameActivity::class.java)
        startActivity(intent)
    }
}
