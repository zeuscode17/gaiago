package com.zeuscode.gaiago.ui.gamification.miniGame.changeCar

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.gamification.miniGame.MiniGameActivity
import com.zeuscode.gaiago.ui.gamification.miniGame.MiniGameCar
import com.zeuscode.gaiago.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_change_car.*
import kotlinx.android.synthetic.main.form_register_car.*

class ChangeCarActivity : AppCompatActivity(), ChangeCarContract.View {

    private lateinit var presenter: ChangeCarPresenter
    private lateinit var miniGameCar:MiniGameCar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_car)

        presenter= ChangeCarPresenter(this)

        miniGameCar=intent?.extras?.getParcelable<MiniGameCar>("MINIGAMECAR")!!



        initRadioButtons()
        initChangeCarButton()

        presenter.fetchUserLevel()

    }
    private fun initRadioButtons() {
        when (miniGameCar.model) {
            "Basic"-> basicCarRadioButton.isChecked=true
            "Chevrolet"-> chevroletCarRadioButton.isChecked=true
            else -> lotusCarRadioButton.isChecked=true
        }

        basicCarRadioButton.setOnClickListener {
            chevroletCarRadioButton.isChecked=false
            lotusCarRadioButton.isChecked=false
        }
        chevroletCarRadioButton.setOnClickListener {
            basicCarRadioButton.isChecked=false
            lotusCarRadioButton.isChecked=false
        }
        lotusCarRadioButton.setOnClickListener {
            chevroletCarRadioButton.isChecked=false
            basicCarRadioButton.isChecked=false
        }
    }

    private fun initChangeCarButton() {
        changeCarButton.setOnClickListener {
            if (basicCarRadioButton.isChecked) miniGameCar.model = "Basic"
            else if (chevroletCarRadioButton.isChecked) miniGameCar.model = "Chevrolet"
            else miniGameCar.model = "Lotus"


            FirebaseModel.updateMiniGameCar(miniGameCar)
            val intent = Intent(this, MiniGameActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onLevelFetched(level:Int) {
        if (level<5) {
            chevroletCarRadioButton.isEnabled=false
        }
        if (level<10) {
            lotusCarRadioButton.isEnabled=false
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent=Intent(this, MiniGameActivity::class.java)
        startActivity(intent)
    }

}
