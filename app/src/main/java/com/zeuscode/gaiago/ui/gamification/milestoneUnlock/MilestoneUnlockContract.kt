package com.zeuscode.gaiago.ui.gamification.milestoneUnlock

import java.util.ArrayList

interface MilestoneUnlockContract {

    interface View {
        fun notifyAdapter()
        fun onPrizeAssigned(position: Int)
    }

    interface Presenter {
        fun loadPrizes(prizes: ArrayList<Prize>)
        fun assignPrize(position: Int)
        fun loadMilestoneList(choosenPrizes: ArrayList<MilestoneItem>)
    }
}