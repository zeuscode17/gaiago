package com.zeuscode.gaiago.ui.gamification.luckySpin

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.zeuscode.gaiago.R

import rubikstudio.library.model.LuckyItem
import kotlin.collections.ArrayList
import kotlin.random.Random
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_daily_rewards_dialog.view.*
import kotlinx.android.synthetic.main.activity_lucky_spin.*
import rubikstudio.library.LuckyWheelView


class LuckySpinActivity : Activity() {

    private lateinit var ref: DatabaseReference
    private lateinit var refTime: DatabaseReference
    private lateinit var refSavedTime: DatabaseReference
    private lateinit var uid: String
    private lateinit var rewardId: String
    private lateinit var mAuth: FirebaseAuth
    private lateinit var refdialog: DatabaseReference
    private lateinit var luckyWheelView: LuckyWheelView

    private var luckyItemListener= object: LuckyWheelView.LuckyRoundItemSelectedListener{
        override fun LuckyRoundItemSelected(index: Int) {
            Log.d("SELECTED", "ITEM SELECTED $index")
            inflateDialog(index)
        }

        override fun onLuckyWheelRotationStart() {
            Log.d("START", "LUCKY STARTED")
        }
    }

    val data = ArrayList<LuckyItem>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lucky_spin)

        uid = FirebaseAuth.getInstance().uid.toString()
        mAuth = FirebaseAuth.getInstance()

        ref = FirebaseDatabase.getInstance().reference.child("users/$uid/spinRewards")
        refTime = FirebaseDatabase.getInstance().reference.child("users/$uid/lastReward")
        refSavedTime = FirebaseDatabase.getInstance().reference.child("users/$uid")


        timerView.isClickable = false
        timerView.visibility = View.INVISIBLE
        buttonSpin.visibility = View.GONE


        val luckyItem1 = LuckyItem()
        luckyItem1.topText = "100"
        luckyItem1.icon = R.drawable.esperienza2
        luckyItem1.color = -0xc20
        data.add(luckyItem1)

        val luckyItem2 = LuckyItem()
        luckyItem2.topText = "200"
        luckyItem2.icon = R.drawable.amazonsconto
        luckyItem2.color = -0xc20
        data.add(luckyItem2)

        val luckyItem3 = LuckyItem()
        luckyItem3.topText = "300"
        luckyItem3.icon = R.drawable.no
        luckyItem3.color = -0xc20
        data.add(luckyItem3)

        val luckyItem4 = LuckyItem()
        luckyItem4.topText = "400"
        luckyItem4.icon = R.drawable.viaggiogratis
        luckyItem4.color = -0xc20
        data.add(luckyItem4)

        val luckyItem5 = LuckyItem()
        luckyItem5.topText = "500"
        luckyItem5.icon = R.drawable.esperienza4
        luckyItem5.color = -0xc20
        data.add(luckyItem5)

        val luckyItem6 = LuckyItem()
        luckyItem6.topText = "600"
        luckyItem6.icon = R.drawable.cerchioni2

        luckyItem6.color = -0xc20
        data.add(luckyItem6)

        val luckyItem7 = LuckyItem()
        luckyItem7.topText = "700"
        luckyItem7.icon = R.drawable.sospensioni2
        luckyItem7.color = -0xc20
        data.add(luckyItem7)

        val luckyItem8 = LuckyItem()
        luckyItem8.topText = "800"
        luckyItem8.icon = R.drawable.gomme4
        luckyItem8.color = -0xc20
        data.add(luckyItem8)

        luckyWheelView=findViewById(R.id.luckyWheel)
        luckyWheelView.setData(data)
        val rand1 = Random.nextInt(0, 8)
        luckyWheelView.setRound(rand1)
        luckyWheelView.setLuckyRoundItemSelectedListener(luckyItemListener)
        verifyTime()//verifico che sia passato abbastanza tempo tra uno spin e l'altro

    }

    //salvataggio reward
    private fun saveReward(index: Int) {

        val dateFormat = System.currentTimeMillis()
        val millisInString = dateFormat.toString()

        rewardId = ref.push().key.toString()
        val reward = Reward(index, rewardId)

        refTime.setValue(millisInString)
        ref.child(rewardId).setValue(reward).addOnCompleteListener {
        }
    }

    private fun verifyTime() {

        refSavedTime.child("lastReward").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                if (snapshot.exists()) {
                    val snap1 = snapshot.value.toString().toLong()        //data db

                    val dateFormat = System.currentTimeMillis()
                    val millisInString = dateFormat.toString().toLong() //data corrente

                    val subract = millisInString - snap1
                    //chiamo la funzone in base al tempo rimasto
                    if (subract <= 14400000) {
                        countDown(14400000 - subract)
                    } else {
                        spinner()
                    }
                } else {
                    Log.d("START", "SPINNER START")
                    spinner()
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    fun countDown(sub: Long) {

        val timer = object : CountDownTimer(sub, 1000) {
            override fun onTick(millisUntilFinished: Long) {

                timerView.visibility = View.VISIBLE
                timerView.text = ("Torna tra: " + ((millisUntilFinished / 1000) / 3600) +
                        ":" + ((millisUntilFinished / 1000 % 3600) / 60) +
                        ":" + ((millisUntilFinished / 1000) % 60))
            }
            override fun onFinish() {
                //nascondo timer
                timerView.visibility = View.INVISIBLE
                spinner()//chiamo la funzione a tempo finito
            }
        }
        timer.start()
    }

    fun spinner() {

        buttonSpin.isClickable = true
        buttonSpin.visibility = View.VISIBLE

        //devo racchiudere il tutto con  un setoncliklistener e lo uso tramite un bottone
        buttonSpin.setOnClickListener {
            val rand = Random.nextInt(0, 8)

            luckyWheelView.startLuckyWheelWithTargetIndex(rand)


            //disabilito il bottone per tot secondi
            buttonSpin.isClickable = false
            buttonSpin.visibility = View.INVISIBLE

        }
    }



    private fun inflateDialog(index:Int) {
        val nDialogView = LayoutInflater.from(this).inflate(R.layout.activity_daily_rewards_dialog, null)

        //set imageDialog with item saved in data array ListItem
        nDialogView.imageDialog.setImageResource(data[index].icon)

        //AlertDialogBuilder
        val nBuilder = AlertDialog.Builder(this)
            .setView(nDialogView)
        //show dialog
        val nAlertDialog = nBuilder.show()
        //close button click of custom layout
        nDialogView.closeButton.setOnClickListener {
            //dismiss dialog
            nAlertDialog.dismiss()
            //chiamo la funzione per salvare il risultato della spin
            saveReward(index)
        }
    }
}




