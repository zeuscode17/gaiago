package com.zeuscode.gaiago.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.main.MainListener
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment :Fragment(){

    private lateinit var mainListener: MainListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_home, container, false)

        view.searchButton.setOnClickListener {
            mainListener.onSearchClick()
        }

        view.lendButton.setOnClickListener {
            mainListener.onLendClick()
        }

        return view
    }

    fun attachListener(listener: MainListener){
        mainListener=listener
    }
}