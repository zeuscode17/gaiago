package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.R

enum class ValidationError(override val title: Int, override val message: Int) :
    Error {
    EMPTY_EMAIL(R.string.empty_email_title, R.string.empty_email_message),
    EMAIL_NOT_VALID(R.string.invalid_email_title, R.string.invalid_email_message),
    EMPTY_PASSWORD(R.string.empty_password_title, R.string.empty_password_message),
    PASSWORD_NOT_VALID(R.string.invalid_password_title, R.string.invalid_password_message),
    NOT_MATCHING_PASSWORD(R.string.mismatching_password_title, R.string.mismatching_password_message),
    EMPTY_CONFIRMATION_CODE(R.string.confirmation_code_empty_title, R.string.confirmation_code_empty_message),
    EMPTY_NAME(R.string.empty_name_title, R.string.empty_name_message),
    EMPTY_SURNAME(R.string.empty_surname_title, R.string.empty_surname_message),
    EMPTY_USERNAME(R.string.empty_username_title, R.string.empty_username_message)
}