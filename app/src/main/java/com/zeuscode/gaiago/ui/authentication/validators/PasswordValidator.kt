package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.authentication.Credentials
import com.zeuscode.gaiago.ui.authentication.validators.ValidationError.EMPTY_PASSWORD
import com.zeuscode.gaiago.ui.authentication.validators.ValidationError.PASSWORD_NOT_VALID
import java.util.regex.Pattern

class PasswordValidator :
    Validator<Credentials> {
    companion object {
        private val PASSWORD_PATTERN = Pattern.compile("(?=^.{6,}\$)((?=.*\\d)(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*\$")
    }

    override fun validate(item: Credentials): ValidationError? {
        val password = item.password

        if (password.isEmpty()) {
            return EMPTY_PASSWORD
        }

        if (!isValid(password)) {
            return PASSWORD_NOT_VALID
        }

        return null
    }

    private fun isValid(password: String): Boolean {
        return PASSWORD_PATTERN.matcher(password).matches()
    }
}