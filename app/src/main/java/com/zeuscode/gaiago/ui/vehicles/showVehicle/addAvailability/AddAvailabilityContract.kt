package com.zeuscode.gaiago.ui.vehicles.showVehicle.addAvailability

import com.zeuscode.gaiago.ui.vehicles.Vehicle

interface AddAvailabilityContract{

    interface View {
        fun initView()
        fun onAvailabilityUpdated()
    }

    interface Presenter {
        fun addFixedAvailability(vehicle: Vehicle, date: String?, startHour:Int?, endHour:Int?)
        fun addRepeatedAvailability(vehicle: Vehicle, startHour: Int?, endHour: Int?,
                                    week:LinkedHashMap<String, Boolean>, repeatFor:Int)
    }
}