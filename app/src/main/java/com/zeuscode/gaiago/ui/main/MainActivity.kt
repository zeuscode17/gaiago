package com.zeuscode.gaiago.ui.main

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.authentication.login.LoginActivity
import com.zeuscode.gaiago.ui.gamification.GamificationFragment
import com.zeuscode.gaiago.ui.home.HomeSliderFragment
import com.zeuscode.gaiago.ui.searchVehicles.SearchVehiclesFragment
import com.zeuscode.gaiago.ui.reservation.Reservation
import com.zeuscode.gaiago.ui.profile.ProfileFragment
import com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider.SearchResultsSliderFragment
import com.zeuscode.gaiago.ui.vehicles.vehicleList.VehiclesListFragment
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : FragmentActivity(), MainContract.View, MainListener {

    //inizializzo i fragments
    private var homeSliderFragment: Fragment = HomeSliderFragment(this)
    private var vehicleListFragment: Fragment = VehiclesListFragment()
    private var searchVehiclesFragment: Fragment = SearchVehiclesFragment()
    private val profileFragment: Fragment = ProfileFragment()
    private var gamificationFragment:Fragment = GamificationFragment()
    private var presenter: MainPresenter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initFragmentContainer()
        //carico il presenter
        presenter= MainPresenter(this)

        initView()
        val vlist=intent?.extras?.getBoolean("VLIST")
        if (vlist!=null && vlist) {
            show(vehicleListFragment)
            bottomNavigationView.selectedItemId=R.id.carsMenuItem
        } else
            show(homeSliderFragment)

    }

    override fun initView() {

        logOutButton.setOnClickListener {
            presenter?.onLogoutRequested()
        }

        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.homeMenuItem -> show(homeSliderFragment)
                R.id.carsMenuItem -> show(vehicleListFragment)
                R.id.bookingMenuItem -> show(searchVehiclesFragment)
                R.id.profileMenuItem -> show(profileFragment)
                R.id.gamificationMenuItem->show(gamificationFragment)
            }
            true
        }
    }

    private fun initFragmentContainer() {
        (searchVehiclesFragment as SearchVehiclesFragment).setMainListener(this)
    }

    override fun onVehiclesSearch(reservation: Reservation, latitude:Double, longitude:Double) {

        val fragment= SearchResultsSliderFragment.newInstance(reservation, latitude, longitude)

        show(fragment)
    }

    override fun onSearchClick() {
        bottomNavigationView.selectedItemId=R.id.bookingMenuItem
        show(searchVehiclesFragment)
    }

    override fun onLendClick() {
        bottomNavigationView.selectedItemId=R.id.carsMenuItem
        show(vehicleListFragment)
    }

    //AlertDialog per richiedere conferma Logout
    override fun askLogOutConfirmation() {
        AlertDialog.Builder(this)
            .setTitle(R.string.confirm_logout_title)
            .setMessage(R.string.confirm_logout_message)
            .setPositiveButton(R.string.confirm) { _, _ -> presenter?.onLogoutConfirmed() }
            .setNegativeButton(R.string.no, null)
            .create().show()
    }

    //Funzione per passare alla login
    override fun showLoginView() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    //Funzione per caricare un nuovo fragment
    private fun show(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainer, fragment)
        transaction.commit()
    }

    override fun onBackPressed() {

        val count = supportFragmentManager.backStackEntryCount

        if (count == 0) {
            AlertDialog.Builder(this)
                .setTitle("Vuoi uscire dall\'applicazione?")
                .setMessage("Conferma per uscire dall\'applicazione")
                .setPositiveButton(R.string.confirm) { _, _ ->  super.onBackPressed()}
                .setNegativeButton(R.string.no, null)
                .create().show()


        } else {
            supportFragmentManager.popBackStack()
        }

    }
}