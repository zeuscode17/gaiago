package com.zeuscode.gaiago.ui.home


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.main.MainListener
import com.zeuscode.gaiago.ui.reservation.ReservationsListFragment
import kotlinx.android.synthetic.main.fragment_home_tabs.*

class HomeSliderFragment(private var mainListener: MainListener): Fragment(){

    private lateinit var homePagerAdapter: HomePagerAdapter
    private lateinit var viewPager: ViewPager

    private var homeFragment=HomeFragment()
    private var reservationsListFragment=ReservationsListFragment()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_tabs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        homeFragment.attachListener(mainListener)
        homePagerAdapter = HomePagerAdapter(childFragmentManager)
        viewPager = view.findViewById(R.id.pager)
        viewPager.adapter = homePagerAdapter
        tabLayout.setupWithViewPager(viewPager)
    }

    inner class HomePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getCount(): Int  = 2

        override fun getItem(i: Int): Fragment {
            if (i==0)
            {
                return homeFragment
            }
            return reservationsListFragment
        }

        override fun getPageTitle(position: Int): CharSequence {
            if (position == 0)
                return "VIAGGIA"
            return "PRENOTAZIONI"
        }
    }


}