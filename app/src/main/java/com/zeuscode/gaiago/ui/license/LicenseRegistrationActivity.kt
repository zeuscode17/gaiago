package com.zeuscode.gaiago.ui.license

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.main.MainActivity
import com.zeuscode.gaiago.ui.license.dates.LicenseDatesFragment
import com.zeuscode.gaiago.ui.license.number.LicenseNumberFragment
import com.zeuscode.gaiago.ui.license.pictures.LicensePicturesFragment

class LicenseRegistrationActivity : AppCompatActivity(), LicenseRegistrationListener,
    LicenseRegistrationContract.View {

    private var presenter: LicenseRegistrationPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_license_registration)

        presenter=LicenseRegistrationPresenter(this)

        showInitialFragment()
    }

    override fun onLicenseNumberSubmitted(number: String) {
        presenter?.onLicenseNumberSubmitted(number)
    }

    override fun onLicenseDatesSubmitted(releaseDate: Long, expirationDate: Long) {
        presenter?.onLicenseDatesSubmitted(releaseDate, expirationDate)
    }

    override fun onLicensePicturesSubmitted() {
        presenter?.onLicensePicturesSubmitted()
    }

    override fun showLicenseDatesRegistration() {
        val fragment = LicenseDatesFragment()
        fragment.attach(this)
        show(fragment)
    }

    override fun showLicensePicturesRegistration() {
        val fragment = LicensePicturesFragment()
        fragment.attach(this)
        show(fragment)
    }

    override fun showMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun showInitialFragment() {
        val fragment = LicenseNumberFragment()
        fragment.attach(this)
        show(fragment)
    }

    private fun show(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainer, fragment)
        transaction.commit()
    }


}
