package com.zeuscode.gaiago.ui.gamification.dailyRewards

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel


class DailyRewardsPresenter(private var view:DailyRewardsContract.View) : DailyRewardsContract.Presenter {

    private lateinit var loadDailyRewardIndexListener:ValueEventListener

    override fun verifyTime() {
        val timeListener= object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val snap1 = snapshot.child("ritiro").value.toString().toBoolean()
                    //Log.d("LOG1", "SNAP1 $snap1")//possibilità di ritiro premio
                    //se è scattata mezzanotte
                    if (!snap1) {
                        view.ritira()
                    } else {
                        //Log.d("LOG2", "COUNTDOWN")
                        view.countDown(snapshot.child("timer").value.toString().toLong())
                    }
                } else {
                    val reward = DailyReward(0, false)
                    //saving
                    FirebaseModel.updateDailyReward(reward)
                    view.ritira()
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }

        FirebaseModel.fetchDailyRewardsTime().addListenerForSingleValueEvent(timeListener)
    }

    override fun updateDailyReward(reward: DailyReward) {
        FirebaseModel.updateDailyReward(reward)
    }

    override fun assignReward() {
        val loadRewardListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                //getting index
                val save = snapshot.value.toString().toInt()
                //increment index
                if (save <= 50) {
                    val reward = DailyReward(save+1, true)
                    //saving
                    FirebaseModel.updateDailyReward(reward)
                } else {
                    val reward = DailyReward(0, true)
                    //saving
                    FirebaseModel.updateDailyReward(reward)
                }
                FirebaseModel.saveUserReward(save)
                FirebaseModel.saveTimerStart()
            }
            override fun onCancelled(databaseError: DatabaseError) {
                //TODO FARE
            }
        }

        FirebaseModel.getDailyRewardIndex().addListenerForSingleValueEvent(loadRewardListener)
    }

    override fun loadDailyRewardIndex() {
        loadDailyRewardIndexListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                //getting index
                if (snapshot.exists()) {
                    val save = snapshot.value.toString().toInt()
                    view.saveDailyRewardIndex(save)
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                //TODO FARE
            }
        }

        FirebaseModel.getDailyRewardIndex().addValueEventListener(loadDailyRewardIndexListener)
    }

    override fun stopListener() {
        FirebaseModel.getDailyRewardIndex().removeEventListener(loadDailyRewardIndexListener)
    }



}