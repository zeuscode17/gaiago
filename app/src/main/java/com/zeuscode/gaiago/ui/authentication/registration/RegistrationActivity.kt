package com.zeuscode.gaiago.ui.authentication.registration

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.KeyboardVisibilityListener
import com.zeuscode.gaiago.extensions.setKeyboardVisibilityListener
import kotlinx.android.synthetic.main.activity_registration.*

class RegistrationActivity : FragmentActivity(), KeyboardVisibilityListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        //Registration fragment initialization
        val registrationFragment = RegistrationFragment()
        //fragment load
        show(registrationFragment)
        //Listener set on the visibility of the keyboard (clicking on the display hides the keyboard)
        setKeyboardVisibilityListener(this)
    }

    //Load function for activity fragments
    private fun show(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainer, fragment)
        transaction.commit()
    }

    //Keyboard Listener
    override fun onKeyboardVisibilityChanged(keyboardVisible: Boolean) {
        if(keyboardVisible)
            scrollView.scrollY = 10000
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setTitle("Vuoi uscire dall\'applicazione?")
            .setMessage("Conferma per uscire dall\'applicazione")
            .setPositiveButton(R.string.confirm) { _, _ ->
                finishAffinity()
                finish()}
            .setNegativeButton(R.string.no, null)
            .create().show()
    }

}