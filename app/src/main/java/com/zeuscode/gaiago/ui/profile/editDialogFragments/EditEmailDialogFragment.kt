package com.zeuscode.gaiago.ui.profile.editDialogFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.User
import kotlinx.android.synthetic.main.fragment_edit_email_dialog.*
import kotlinx.android.synthetic.main.fragment_edit_email_dialog.view.*

class EditEmailDialogFragment(private var user: User?) : EditProfileDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_edit_email_dialog, container, false)

        view.emailEditText.setText(user?.email)

        view.okButton.setOnClickListener {
            val newEmail=emailEditText.text.toString()
            user?.email=newEmail
            presenter?.updateEmail(newEmail)
            this.dismiss()
        }

        view.cancelButton.setOnClickListener {
            this.dismiss()
        }

        return view
    }

}
