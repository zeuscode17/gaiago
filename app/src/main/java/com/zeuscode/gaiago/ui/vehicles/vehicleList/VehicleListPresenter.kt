package com.zeuscode.gaiago.ui.vehicles.vehicleList

import android.content.ContentValues
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.vehicles.Vehicle

class VehicleListPresenter(private var view: VehicleListContract.View):VehicleListContract.Presenter {

    override fun loadVehicles(vehicles: ArrayList<Vehicle>)  {

        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //In caso di dati cambiati, svuoto l'arraylist
                vehicles.clear()

                for (singleSnapshot in dataSnapshot.children) {
                    //Creo un oggetto per ogni veicolo
                    val vehicle = singleSnapshot.getValue(Vehicle::class.java)
                    if (vehicle!=null)
                        vehicles.add(vehicle)
                }

                view.onVehiclesFetched()
            }
            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        FirebaseModel.fetchVehicle().addListenerForSingleValueEvent(valueEventListener)
    }

}