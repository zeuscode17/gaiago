package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.authentication.Credentials
import com.zeuscode.gaiago.ui.authentication.validators.ValidationError.EMPTY_PASSWORD

class AuthenticationPasswordValidator :
    Validator<Credentials> {

    override fun validate(item: Credentials): ValidationError? {
        val password = item.password

        if (password.isEmpty()) {
            return EMPTY_PASSWORD
        }

        return null
    }

}