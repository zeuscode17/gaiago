package com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider.availableVehicles.selectedVehicle

import android.content.ContentValues
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.reservation.Reservation

class SelectedVehiclePresenter(private var view: SelectedVehicleContract.View) :
    SelectedVehicleContract.Presenter {

    override fun onBookingConfirmed(reservation: Reservation, vehicleID:String, newAvailability:String) {
        loadProgressBar()
        FirebaseModel.insertReservation(reservation,vehicleID,newAvailability)

        view.onReservationInserted()
    }

    private fun loadProgressBar() {
        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                val progressBar=dataSnapshot.value.toString().toCharArray()
                progressBar[4]='1'
                FirebaseModel.editProgressBar(progressBar.joinToString(""))
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(ContentValues.TAG, "loadPost:onCancelled", databaseError.toException())

            }
        }
        FirebaseModel.fetchProgressBar().addListenerForSingleValueEvent(valueEventListener)
    }
}