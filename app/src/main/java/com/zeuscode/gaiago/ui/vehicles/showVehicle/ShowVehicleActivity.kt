package com.zeuscode.gaiago.ui.vehicles.showVehicle

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.squareup.picasso.Picasso
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.main.MainActivity
import com.zeuscode.gaiago.ui.vehicles.showVehicle.addAvailability.AddAvailabilityActivity
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import com.zeuscode.gaiago.ui.vehicles.showVehicle.showStats.ShowStatitisticsActivity
import com.zeuscode.gaiago.ui.vehicles.showVehicle.vehicleAvailabilities.VehicleAvailabilitiesActivity
import kotlinx.android.synthetic.main.activity_show_vehicle.*
import kotlinx.android.synthetic.main.activity_show_vehicle.brandModelTextView
import kotlinx.android.synthetic.main.activity_show_vehicle.upperBrandModelTextView
import kotlinx.android.synthetic.main.activity_show_vehicle.vehicleImageView
import kotlinx.android.synthetic.main.activity_show_vehicle.yearTextView
import kotlinx.android.synthetic.main.rating_bar.*
import java.lang.Math.round

class ShowVehicleActivity : AppCompatActivity(), ShowVehicleContract.View {

    private lateinit var presenter:ShowVehiclePresenter
    private lateinit var vehicle: Vehicle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_vehicle)

        presenter= ShowVehiclePresenter(this)

        vehicle = intent?.extras?.getParcelable("VEHICLE")!!

        initView()
    }

    override fun initView() {
        val brandModel="${vehicle.brand} ${vehicle.model}"
        upperBrandModelTextView.text=brandModel
        brandModelTextView.text=brandModel
        yearTextView.text=vehicle.year
        val hourlyCost= "${vehicle.hourlyCost} €/ora"
        hourlyCostTextView.text=hourlyCost
        vehicleRatingBar.rating=vehicle.rating

        //Carico l'immagine con Picasso
        Picasso.get().load(Uri.parse(vehicle.uri)).into(vehicleImageView)

        //bottone per la rimozione del veicolo
        removeVehicleButton.setOnClickListener {
            //Chiedo conferma
            askRemoveConfirmation(vehicle.vehicleID)
        }

        addAvailabilityButton.setOnClickListener {
            showAddAvailability()
        }

        statisticsButton.setOnClickListener {
            val intent=Intent(this, ShowStatitisticsActivity::class.java)
            intent.putExtra("VEHICLEID", vehicle.vehicleID)
            intent.putExtra("BMODEL", brandModel)
            startActivity(intent)
        }

    }

    private fun askRemoveConfirmation(key:String) {
        AlertDialog.Builder(this)
            .setTitle(R.string.confirm_remove_title)
            .setMessage(R.string.confirm_remove_message)
            .setPositiveButton(R.string.confirm) { _, _ -> presenter.removeVehicle(key) }
            .setNegativeButton(R.string.no, null)
            .create().show()
    }

    private fun showAddAvailability() {
        val intent=Intent(this, VehicleAvailabilitiesActivity::class.java)
        intent.putExtra("VEHICLE", vehicle)
        startActivity(intent)
    }

    override fun onVehicleRemoved() {
        val intent=Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent= Intent(this, MainActivity::class.java)
        intent.putExtra("VLIST", true)
        startActivity(intent)
    }
}

