package com.zeuscode.gaiago.ui.main

interface MainContract {

    interface View {
        fun initView()
        fun showLoginView()
        fun askLogOutConfirmation()
    }

    interface Presenter {
        fun onLogoutRequested()
        fun onLogoutConfirmed()
    }
}