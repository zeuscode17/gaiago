package com.zeuscode.gaiago.ui.authentication.validators

interface Validator<T> {
    fun validate(item: T): ValidationError?
}
