package com.zeuscode.gaiago.ui.authentication.login

import com.zeuscode.gaiago.ui.authentication.Credentials
import com.zeuscode.gaiago.ui.authentication.validators.Error

interface LoginContract {

    interface View {
        fun initView()
        fun onAuthenticationFailure()
        fun onAuthenticationError(error: Error)
        fun onAuthenticationSuccess()
        fun onResetSent()
    }

    interface Presenter {
        fun login(credentials: Credentials)
        fun sendResetEmail(email: String)
    }

}