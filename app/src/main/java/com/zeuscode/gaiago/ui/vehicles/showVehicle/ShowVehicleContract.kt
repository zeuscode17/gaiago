package com.zeuscode.gaiago.ui.vehicles.showVehicle

interface ShowVehicleContract {
    interface View{
        fun initView()
        fun onVehicleRemoved()
    }

    interface Presenter{
        fun removeVehicle(key:String)
    }
}