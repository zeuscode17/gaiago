package com.zeuscode.gaiago.ui.reservation.showReservation

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.PopupWindow
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.twoDigitsString
import com.zeuscode.gaiago.ui.main.MainActivity
import com.zeuscode.gaiago.ui.reservation.Reservation
import kotlinx.android.synthetic.main.activity_show_reservation.*
import kotlinx.android.synthetic.main.rating_popup.*
import kotlinx.android.synthetic.main.rating_popup.view.*

class ShowReservationActivity : AppCompatActivity(), ShowReservationContract.View {

    private lateinit var reservation: Reservation
    private lateinit var presenter: ShowReservationPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_reservation)

        presenter= ShowReservationPresenter(this)

        reservation=intent?.extras?.getParcelable("RESERVATION")!!

        initView()
    }

    override fun onStart() {
        super.onStart()
        presenter.loadReservationStatus(reservation.key)
    }

    override fun onStop() {
        super.onStop()
        presenter.stopListener(reservation.key)
    }

    override fun onNewStatusFetched(status: Int) {
        reservation.state=status
        initReservationStatus()
    }

    override fun initView() {
        //Carico l'immagine con Picasso
        Picasso.get().load(Uri.parse(reservation.vehicleUri)).into(vehicleImageView)

        upperBrandModelTextView.text=reservation.brandModel
        brandModelTextView.text=reservation.brandModel

        addressTextView.text=reservation.address

        val date="${reservation.date.substring(0,2)}/${reservation.date.substring(2,4)}/${reservation.date.substring(4,8)}"
        dateTextView.text=date

        val hours = "${intToHour(reservation.startHour)} - ${intToHour(reservation.endHour)}"

        hoursTextView.text=hours

        initReservationStatus()

    }

    override fun initReservationStatus() {
        stateTextView.let {
            when (reservation.state) {
                0 -> {
                    it.text = "In attesa di conferma"
                    it.setTextColor(resources.getColor(R.color.darkGrey, null))
                    if (reservation.isOwner)
                        initConfirmOwner()
                    else {
                        confirmReservationButton.visibility=View.GONE
                        initDeleteReservationButton()
                    }
                }
                1 -> {
                    it.text = "Confermata"
                    it.setTextColor(resources.getColor(R.color.green, null))
                    deleteReservationButton.visibility=View.GONE
                    if (reservation.isOwner)
                        initKeysOwner()
                    else
                        confirmReservationButton.visibility=View.GONE
                }
                2 -> {
                    it.text = "Cancellata"
                    it.setTextColor(resources.getColor(R.color.red,null))
                    confirmReservationButton.visibility=View.VISIBLE
                    deleteReservationButton.visibility=View.GONE
                    initStoreReservation()
                }
                3 -> {
                    it.text = "In corso"
                    it.setTextColor(resources.getColor(R.color.green, null))
                    deleteReservationButton.visibility=View.GONE
                    if (reservation.isOwner)
                        initEndOwner()
                    else
                        confirmReservationButton.visibility=View.GONE
                }
                4 -> {
                    it.text = "Terminata"
                    it.setTextColor(resources.getColor(R.color.green, null))
                    deleteReservationButton.visibility=View.GONE
                    confirmReservationButton.visibility=View.VISIBLE
                    if (reservation.isOwner) {
                        if (reservation.ownerState==0)
                            initRateUser()
                        else initStoreReservation()
                    } else {
                        if (reservation.userState==0)
                            initRateUser()
                        else initStoreReservation()
                    }
                }
                else -> {
                }
            }
        }
    }

    private fun initConfirmOwner() {
        initDeleteReservationButton()
        confirmReservationButton.setOnClickListener {
            reservation.state=1
            presenter.editReservationStatus(reservation.key, 1)
        }
    }

    private fun initDeleteReservationButton() {
        deleteReservationButton.visibility=View.VISIBLE
        deleteReservationButton.setOnClickListener {
            reservation.state=2
            presenter.editReservationStatus(reservation.key, 2)
            presenter.restoreAvailability(reservation)
        }
    }

    private fun initKeysOwner() {
        confirmReservationButton.text="CHIAVI CONSEGNATE"
        confirmReservationButton.setOnClickListener {
            reservation.state=3
            presenter.editReservationStatus(reservation.key, 3)
        }
    }

    private fun initEndOwner() {
        confirmReservationButton.text="CONCLUDI PRENOTAZIONE"
        confirmReservationButton.setOnClickListener {
            reservation.state=4
            presenter.editReservationStatus(reservation.key, 4)
            presenter.addXPpoints(reservation.userID, reservation.ownerID)
        }
    }

    private fun initRateUser() {
        confirmReservationButton.text="RECENSISCI"
        confirmReservationButton.setOnClickListener {
            showPopUp(it)
        }
    }

    private fun initStoreReservation() {
        confirmReservationButton.text="ARCHIVIA"
        confirmReservationButton.setOnClickListener {
            if (reservation.isOwner) {
                reservation.ownerState=2
                presenter.editOwnerStatus(reservation.key, 2)
            } else {
                reservation.userState=2
                presenter.editUserStatus(reservation.key,2)
            }
            val intent= Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }


    private fun showPopUp(view: View){

        val window = PopupWindow(this)
        // Initialize a new layout inflater instance
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        // Inflate a custom view using layout inflater
        val popview = inflater.inflate(R.layout.rating_popup,null)

        window.contentView=popview
        window.showAtLocation(view, Gravity.CENTER,0,0)

        popview.confirmRatingButton.setOnClickListener {
            val rating = popview.ratingBar.rating
            presenter.addUserRating(reservation.userID, rating)
            if (!reservation.isOwner) {
                presenter.addVehicleRating(reservation.vehicleID, rating)
            }
            if (reservation.isOwner) {
                reservation.ownerState=1
                presenter.editOwnerStatus(reservation.key, 1)
            } else {
                reservation.userState=1
                presenter.editUserStatus(reservation.key,1)
            }
            Toast.makeText(this,"Recensione inserita", Toast.LENGTH_SHORT).show()
            window.dismiss()
        }
    }

    private fun intToHour(interval:Int) :String {
        val hour=(interval/4).twoDigitsString()
        val minute=((interval%4)*15).twoDigitsString()
        return "$hour:$minute"
    }
}

