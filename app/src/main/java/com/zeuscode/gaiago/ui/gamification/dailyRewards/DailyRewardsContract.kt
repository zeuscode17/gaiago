package com.zeuscode.gaiago.ui.gamification.dailyRewards

interface DailyRewardsContract {

    interface View {
        fun countDown(time:Long)
        fun saveDailyRewardIndex(index:Int)
        fun ritira()
    }

    interface Presenter {
        fun verifyTime()
        fun updateDailyReward(reward: DailyReward)
        fun assignReward()
        fun loadDailyRewardIndex()
        fun stopListener()
    }

}