package com.zeuscode.gaiago.ui.vehicles.showVehicle.vehicleAvailabilities

import android.content.ContentValues
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel

class VehicleAvailabilitiesPresenter(private val view:VehicleAvailabilitiesContract.View):VehicleAvailabilitiesContract.Presenter {


    override fun loadVehicleAvailabilities(availabilities: ArrayList<Availability>, vehicleID:String)  {

        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //In caso di dati cambiati, svuoto l'arraylist
                availabilities.clear()

                for (singleSnapshot in dataSnapshot.children) {

                    if (singleSnapshot!=null) {
                        val availability = Availability(singleSnapshot.key.toString(), singleSnapshot.value.toString())
                        availabilities.add(availability)
                    }
                }

                view.onVehicleAvailabilitiesFetched()
            }
            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        FirebaseModel.fetchVehicleAvailabilities(vehicleID).addListenerForSingleValueEvent(valueEventListener)
    }
}