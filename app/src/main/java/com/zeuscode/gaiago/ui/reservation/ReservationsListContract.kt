package com.zeuscode.gaiago.ui.reservation

interface ReservationsListContract {
    interface View {
        fun onReservationsFetched()
    }

    interface Presenter {
        fun loadReservations(reservations: ArrayList<Reservation>)
        fun stopListener()
    }
}