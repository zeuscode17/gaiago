package com.zeuscode.gaiago.ui.gamification.dailyRewards

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.core.content.res.getDrawableOrThrow
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.gamification.luckySpin.Reward
import kotlinx.android.synthetic.main.activity_daily_rewards.*
import kotlinx.android.synthetic.main.activity_daily_rewards_dialog.*
import kotlinx.android.synthetic.main.activity_daily_rewards_dialog.view.*
import java.util.*
import android.widget.ImageView
import com.zeuscode.gaiago.model.FirebaseModel


class DailyRewardsActivity : Activity(), DailyRewardsContract.View {

    private lateinit var presenter: DailyRewardsPresenter
    var dailyRewardIndex:Int=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daily_rewards)

        presenter = DailyRewardsPresenter(this)

        presenter.loadDailyRewardIndex()
        presenter.verifyTime()//verifico che sia passato un giorno

    }

    override fun saveDailyRewardIndex(index: Int) {
        dailyRewardIndex=index

        val img = resources.obtainTypedArray(R.array.grid_imgs)

        imageView.setImageDrawable(img.getDrawableOrThrow(dailyRewardIndex))
        img.recycle()
        initRitiraPremio()
    }


    override fun countDown(time:Long) { //funzione che mi fa partire il conto alla rovescia dalla mezzanotte del giorno corrente fino alla mezzanotte
        //del giorno dopo, se ho ritirato il premio o se il premio è già stato ritirato in precedenza lungo l'arco della giornata

        val timeLeft=time-System.currentTimeMillis()
        ritirapremio.visibility = View.GONE //metto invisibile il bottone

        val timer = object : CountDownTimer(timeLeft, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timerView.visibility = View.VISIBLE
                timerView.text = ("Torna tra: ${millisToString(millisUntilFinished)}")
            }
            override fun onFinish() { //non appena scade la mezzanotte richiamo il ritiro del premio

                if (dailyRewardIndex <= 50) {
                    dailyRewardIndex+=1
                    val reward = DailyReward(dailyRewardIndex, false)
                    //saving
                    presenter.updateDailyReward(reward)
                }

                ritira()
            }
        }
        timer.start()
    }

    override fun ritira() {
        ritirapremio.visibility = View.VISIBLE
        ritirapremio.isClickable = true
        //nascondo la timer
        timerView.visibility = View.GONE

    }
    private fun initRitiraPremio() {
        //funzione del click del bottone
        ritirapremio.setOnClickListener {

            ritirapremio.visibility = View.GONE //bottone nascosto
            //faccio in modo che l'activity dialog riceva i premi
            //Inflate the dialog with custom view
            val nDialogView = LayoutInflater.from(this).inflate(R.layout.activity_daily_rewards_dialog, null)
            val imgd = resources.obtainTypedArray(R.array.dialog_imgs)


            val dialogImage=dailyRewardIndex/2
            nDialogView.imageDialog.setImageDrawable(imgd.getDrawableOrThrow(dialogImage))
            imgd.recycle()
            buildDailog(nDialogView)

            presenter.assignReward() //richiamo il salvataggio del primo reward
            presenter.verifyTime() //parte il conto alla rovescia
        }
    }

    private fun buildDailog(nDialogView:View) {
        //AlertDialogBuilder
        val nBuilder = AlertDialog.Builder(this)
            .setView(nDialogView)
        //show dialog
        val nAlertDialog = nBuilder.show()
        //close button click of custom layout
        nDialogView.closeButton.setOnClickListener {
            //dismiss dialog
            nAlertDialog.dismiss()
        }
    }

    //funzione per convertire in ore:minuti:secondi il mio orario in millisecondi per il countdown
    private fun millisToString(timeInMillis: Long): String {
        val hours = (timeInMillis / 1000) / 3600
        val minutes = (timeInMillis / 1000 % 3600) / 60
        val seconds = (timeInMillis / 1000) % 60

        return if (hours > 0) {
            String.format(
                Locale.getDefault(),
                "%d:%02d:%02d", hours, minutes, seconds
            )
        } else {
            String.format(
                Locale.getDefault(),
                "%02d:%02d", minutes, seconds
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.stopListener()
    }
}