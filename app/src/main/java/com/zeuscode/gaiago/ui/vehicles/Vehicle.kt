package com.zeuscode.gaiago.ui.vehicles

import android.os.Parcel
import android.os.Parcelable

@Suppress("UNCHECKED_CAST")
data class Vehicle(
        var brand: String = "",
        var model: String = "",
        var year: String = "",
        var hourlyCost: Int = 0,
        var uri: String = "",
        var owner: String = "",
        var vehicleID: String = "",
        var placeDescription: String = "",
        var latitude: Double = 0.0,
        var longitude: Double = 0.0,
        var availability: HashMap<String, String> = HashMap(),
        var rating: Float = 0.0f,
        var numRating: Int = 0
) : Parcelable {
        constructor(source: Parcel) : this(
                source.readString()!!,
                source.readString()!!,
                source.readString()!!,
                source.readInt(),
                source.readString()!!,
                source.readString()!!,
                source.readString()!!,
                source.readString()!!,
                source.readDouble(),
                source.readDouble(),
                source.readSerializable() as HashMap<String, String>,
                source.readFloat(),
                source.readInt()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
                writeString(brand)
                writeString(model)
                writeString(year)
                writeInt(hourlyCost)
                writeString(uri)
                writeString(owner)
                writeString(vehicleID)
                writeString(placeDescription)
                writeDouble(latitude)
                writeDouble(longitude)
                writeSerializable(availability)
                writeFloat(rating)
                writeInt(numRating)
        }

        companion object {
                @JvmField
                val CREATOR: Parcelable.Creator<Vehicle> = object : Parcelable.Creator<Vehicle> {
                        override fun createFromParcel(source: Parcel): Vehicle = Vehicle(source)
                        override fun newArray(size: Int): Array<Vehicle?> = arrayOfNulls(size)
                }
        }
}
