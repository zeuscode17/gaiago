package com.zeuscode.gaiago.ui.reservation.showReservation

import com.zeuscode.gaiago.ui.reservation.Reservation

interface ShowReservationContract {

    interface View {
        fun initView()
        fun initReservationStatus()
        fun onNewStatusFetched(status: Int)
    }

    interface Presenter {
        fun editReservationStatus(key: String, status:Int)
        fun loadReservationStatus(key:String)
        fun stopListener(key:String)
        fun restoreAvailability(reservation: Reservation)
        fun addUserRating(userID:String, rating:Float)
        fun addVehicleRating(vehicleID:String, rating: Float)
        fun editUserStatus(key:String, status: Int)
        fun editOwnerStatus(key: String, status: Int)
        fun addXPpoints(userID:String, ownerID:String)
        fun updateProgressBar(userID: String)
    }
}