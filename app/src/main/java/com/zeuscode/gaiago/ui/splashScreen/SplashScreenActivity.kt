package com.zeuscode.gaiago.ui.splashScreen

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.authentication.login.LoginActivity
import com.zeuscode.gaiago.ui.main.MainActivity

class SplashScreenActivity : Activity(), SplashScreenContract.View {

    private lateinit var presenter: SplashScreenPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        presenter = SplashScreenPresenter(this)

        Thread(runnable).start()
    }

    private val runnable = Runnable {

        Thread.sleep(1000)

        presenter.checkUserAuthentication()
    }

    //Activity load function
    private fun showActivity(intent: Intent) {
        startActivity(intent)
        finish()
    }

    //Main activity load function
    override fun showMainView() {
        val intent = Intent(this, MainActivity::class.java)
        showActivity(intent)
    }

    //Login activity load function
    override fun showLoginView() {
        val intent = Intent(this, LoginActivity::class.java)
        showActivity(intent)
    }

}