package com.zeuscode.gaiago.ui.license.infoDialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.zeuscode.gaiago.R
import kotlinx.android.synthetic.main.fragment_license_info_dialog.view.*

class LicenseInfoDialogFragment : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

        val view = inflater.inflate(R.layout.fragment_license_info_dialog, container, false)

        val info = arguments!![DIALOG_INFO_KEY] as LicenseDialogInfo

        view.primaryTitleTextView.text = getString(info.primaryTitle)
        view.secondaryTitleTextView.text = getString(info.secondaryTitle)
        view.imageView.setImageResource(info.image)

        view.closeButton.setOnClickListener {
            this.dismiss()
        }

        return view
    }

    companion object {
        const val DIALOG_INFO_KEY = "info"
    }
}
