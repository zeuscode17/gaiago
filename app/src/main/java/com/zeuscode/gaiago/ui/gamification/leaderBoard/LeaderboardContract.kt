package com.zeuscode.gaiago.ui.gamification.leaderBoard

import com.zeuscode.gaiago.ui.User

interface LeaderboardContract {

    interface View {
        fun notifyAdapter()
    }

    interface Presenter {
        fun loadPlayers(users: ArrayList<User>)
    }
}