package com.zeuscode.gaiago.ui.main

import com.zeuscode.gaiago.model.FirebaseModel

class MainPresenter(private var view:MainContract.View): MainContract.Presenter {

    override fun onLogoutRequested() {
        view.askLogOutConfirmation()
    }

    override fun onLogoutConfirmed() {
        FirebaseModel.signOut()
        view.showLoginView()
    }
}
