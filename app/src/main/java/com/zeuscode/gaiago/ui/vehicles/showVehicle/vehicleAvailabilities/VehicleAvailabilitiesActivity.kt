package com.zeuscode.gaiago.ui.vehicles.showVehicle.vehicleAvailabilities

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.RecyclerItemClickListener
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import com.zeuscode.gaiago.ui.vehicles.showVehicle.ShowVehicleActivity
import com.zeuscode.gaiago.ui.vehicles.showVehicle.addAvailability.AddAvailabilityActivity
import kotlinx.android.synthetic.main.activity_vehicle_availabilities.*

class VehicleAvailabilitiesActivity : AppCompatActivity(), VehicleAvailabilitiesContract.View{

    private lateinit var vehicle: Vehicle
    private lateinit var presenter: VehicleAvailabilitiesPresenter
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val availabilities : ArrayList<Availability> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicle_availabilities)

        vehicle = intent?.extras?.getParcelable("VEHICLE")!!

        initView()

        presenter= VehicleAvailabilitiesPresenter(this)

        initRecycler()
        initRecyclerListener()

        presenter.loadVehicleAvailabilities(availabilities, vehicle.vehicleID)
    }


    private fun initView() {
        val brandModel="${vehicle.brand} ${vehicle.model}"
        upperBrandModelTextView.text=brandModel
        brandModelTextView.text=brandModel

        //Carico l'immagine con Picasso
        Picasso.get().load(Uri.parse(vehicle.uri)).into(vehicleImageView)

        fab.setOnClickListener {
            val intent= Intent(this, AddAvailabilityActivity::class.java)
            intent.putExtra("VEHICLE", vehicle)
            startActivity(intent)
        }

    }

    private fun initRecycler() {
        //LinearLayoutManager, le cardView verranno disposte una sopra l'altra
        viewManager = LinearLayoutManager(this)

        viewAdapter = VehicleAvailabilitiesRecyclerViewAdapter(availabilities)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    private fun initRecyclerListener() {
        recyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                this,
                recyclerView,
                object : RecyclerItemClickListener.OnItemClickListener {

                    //Click rapido
                    override fun onItemClick(view: View, position: Int) {

                    }

                    //Click Lungo
                    override fun onLongItemClick(view: View?, position: Int) {
                    }
                })
        )
    }


    override fun onVehicleAvailabilitiesFetched() {
        viewAdapter.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, ShowVehicleActivity::class.java)
        intent.putExtra("VEHICLE", vehicle)
        startActivity(intent)
    }
}
