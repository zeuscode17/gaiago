package com.zeuscode.gaiago.ui.license.number

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.license.infoDialogs.LicenseDialogInfo
import com.zeuscode.gaiago.ui.license.infoDialogs.LicenseInfoDialogFragment
import com.zeuscode.gaiago.ui.license.LicenseRegistrationListener
import com.zeuscode.gaiago.extensions.showDialog
import kotlinx.android.synthetic.main.fragment_license_number.*
import kotlinx.android.synthetic.main.fragment_license_number.view.*
import kotlinx.android.synthetic.main.next_button.view.*

class LicenseNumberFragment : Fragment(), LicenseNumberContract.View {

    private lateinit var listener: LicenseRegistrationListener
    private lateinit var presenter: LicenseNumberPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_license_number, container, false)

        view.licenseNumberInfoButton.setOnClickListener {
            showDialogWith(LicenseDialogInfo.NUMBER_DIALOG_INFO)
        }

        view.licenseCategoryInfoButton.setOnClickListener {
            showDialogWith(LicenseDialogInfo.CATEGORY_DIALOG_INFO)
        }

        presenter = LicenseNumberPresenter(this)

        view.nextButton.setOnClickListener {
            presenter.onLicenseNumberSubmitted(licenseNumberEditText.text.toString(), licenseHolderCheckBox.isChecked, listener)
        }

        return view
    }

    override fun showEmptyLicenseNumberDialog() {
        showDialog(
                getString(R.string.empty_license_number_title),
                R.string.empty_license_number_message,
                R.string.ok,
                null
        )
    }

    override fun showUncheckedLicenseCategoryHeldDialog() {
        showDialog(
                getString(R.string.unchecked_license_category_title),
                R.string.unchecked_license_category_message,
                R.string.ok,
                null
        )
    }

    fun attach(listener: LicenseRegistrationListener) {
        this.listener = listener
    }

    private fun showDialogWith(info: LicenseDialogInfo) {
        val fragment = LicenseInfoDialogFragment()

        fragment.arguments = Bundle().apply {
            putSerializable(LicenseInfoDialogFragment.DIALOG_INFO_KEY, info)
        }

        fragment.show(fragmentManager, "dialog")
    }

}
