package com.zeuscode.gaiago.ui

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class User(
    var name: String = "",
    var surname: String = "",
    var email: String = "",
    var phoneNumber: String = "",
    var birthDate: Long? = null,
    var address: String? = "",
    var score: Long=0,
    var milestoneStat: String = "UUUUUUUUUUUUUUUUUUUUUUUUUUUU",
    var progressbar: String = "000000",
    var rating: Float =0.0f,
    var numRating: Int=0
)