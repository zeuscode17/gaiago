package com.zeuscode.gaiago.ui.vehicles.showVehicle.vehicleAvailabilities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.twoDigitsString

class VehicleAvailabilitiesRecyclerViewAdapter(private val myAvailabilities: ArrayList<Availability>) :
    RecyclerView.Adapter<VehicleAvailabilitiesRecyclerViewAdapter.MyViewHolder>() {
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var dateTextView: TextView = view.findViewById(R.id.dateTextView)
        var hoursTextView: TextView = view.findViewById(R.id.hoursTextView)
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.available_time_slot, parent, false)
        // set the view's size, margins, paddings and layout parameters

        return MyViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        val date="${myAvailabilities[position].date.substring(0,2)}/${myAvailabilities[position].date.substring(2,4)}/${myAvailabilities[position].date.substring(4,8)}"
        holder.dateTextView.text=date
        val hours=stringToHours(myAvailabilities[position].hours)
        holder.hoursTextView.text=hours

    }


    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myAvailabilities.size

    private fun stringToHours(hours:String): String {
        var i=0
        var availability=""
        while (i<hours.length) {

            if (hours[i]==('0')) {
                availability+="${intToHour(i)}-"
                while (i<hours.length && hours[i]=='0')
                    i++
                availability+="${intToHour(i)}\n"
            }
            i++
        }
        return availability.trimEnd()
    }

    private fun intToHour(interval:Int) :String {
        val hour=(interval/4).twoDigitsString()
        val minute=((interval%4)*15).twoDigitsString()
        return "$hour:$minute"
    }
}