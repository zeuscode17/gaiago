package com.zeuscode.gaiago.ui.vehicles.showVehicle.addAvailability

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.pickerDialogs.dialogs.HourPickerDialogFragment
import com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces.HourPickerListener
import kotlinx.android.synthetic.main.fragment_set_repetition.*
import kotlinx.android.synthetic.main.fragment_set_repetition.view.*
import android.widget.CompoundButton
import android.app.AlertDialog
import android.widget.Spinner
import kotlinx.android.synthetic.main.checkbox.view.*


class SetRepetitionFragment : Fragment() {

    var bookingStartHour: Int? = null
    var bookingEndHour: Int? = null
    val week= linkedMapOf(
        "Lun" to false,
        "Mar" to false,
        "Mer" to false,
        "Gio" to false,
        "Ven" to false,
        "Sab" to false,
        "Dom" to false)
    lateinit var spinner : Spinner

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        //Execution of the fragment inflation inside the fragment container
        val view = inflater.inflate(R.layout.fragment_set_repetition, container, false)

        view.bookingStartHourEditText.setOnClickListener {
            showHourPickerDialog(startHourPickerListener)
        }

        view.bookingEndHourEditText.setOnClickListener {
            showHourPickerDialog(endHourPickerListener)
        }

        view.repetitionTextView.setOnClickListener {
            week.replaceAll { _, _ -> false}

            val checkBoxView= initCheckBoxView()
            showCheckBoxDialog(checkBoxView)
        }

        spinner=view.repetitionSpinner

        return view
    }

    private val startHourPickerListener = object:
        HourPickerListener {
        override fun onHourPicked(hourPicked:String,interval:Int) {
            bookingStartHourEditText.setText(hourPicked, TextView.BufferType.EDITABLE)
            bookingStartHour=interval
        }
    }

    private val endHourPickerListener = object:
        HourPickerListener {
        override fun onHourPicked(hourPicked:String,interval:Int) {
            bookingEndHourEditText.setText(hourPicked, TextView.BufferType.EDITABLE)

            bookingEndHour= if (interval==0) 96 else interval
        }
    }

    private fun showHourPickerDialog(listener: HourPickerListener) {
        val fragment = HourPickerDialogFragment()
        fragment.attach(listener)
        fragment.show(fragmentManager, "dialog")
    }

    private fun initCheckBoxView(): View {
        val checkBoxView = View.inflate(requireContext(), R.layout.checkbox, null)
        checkBoxView.mondayCheckbox.setOnCheckedChangeListener(checkBoxListener)
        checkBoxView.tuesdayCheckbox.setOnCheckedChangeListener(checkBoxListener)
        checkBoxView.wednesdayCheckbox.setOnCheckedChangeListener(checkBoxListener)
        checkBoxView.thursdaycheckbox.setOnCheckedChangeListener(checkBoxListener)
        checkBoxView.fridayCheckbox.setOnCheckedChangeListener(checkBoxListener)
        checkBoxView.saturdayCheckbox.setOnCheckedChangeListener(checkBoxListener)
        checkBoxView.sundayCheckbox.setOnCheckedChangeListener(checkBoxListener)

        return checkBoxView
    }

    private fun selectedDays():String {
        var ret=""
        for (i in week) if (i.value) ret+=i.key+" "
        return ret
    }

    private fun showCheckBoxDialog(checkBoxView:View) {
        AlertDialog.Builder(requireContext())
            .setTitle("SELEZIONA I GIORNI")
            .setView(checkBoxView)
            .setCancelable(false)
            .setPositiveButton("Conferma") { dialog, _ ->
                dialog.dismiss()
                setSelectedDays()
            }
            .show()
    }

    private fun setSelectedDays(){
        repetitionTextView.setText(selectedDays(), TextView.BufferType.EDITABLE)
    }

    private val checkBoxListener = CompoundButton.OnCheckedChangeListener { compoundButton, isChecked ->
        when (compoundButton.id) {
            R.id.mondayCheckbox -> {
                week["Lun"]=isChecked
            }
            R.id.tuesdayCheckbox -> {
                week["Mar"]=isChecked
            }
            R.id.wednesdayCheckbox -> {
                week["Mer"]=isChecked
            }
            R.id.thursdaycheckbox -> {
                week["Gio"]=isChecked
            }
            R.id.fridayCheckbox -> {
                week["Ven"]=isChecked
            }
            R.id.saturdayCheckbox -> {
                week["Sab"]=isChecked
            }
            R.id.sundayCheckbox -> {
                week["Dom"]=isChecked
            }
        }
    }

}