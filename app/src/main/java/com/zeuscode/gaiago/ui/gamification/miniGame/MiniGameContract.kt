package com.zeuscode.gaiago.ui.gamification.miniGame

interface MiniGameContract {

    interface View {
        fun onMiniGameCarFetched(miniGameCar: MiniGameCar)
    }

    interface Presenter {
        fun loadMiniGameCar()
    }
}