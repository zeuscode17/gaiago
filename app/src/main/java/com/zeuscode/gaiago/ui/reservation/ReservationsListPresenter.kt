package com.zeuscode.gaiago.ui.reservation

import android.content.ContentValues
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel

class ReservationsListPresenter(private var view: ReservationsListContract.View):ReservationsListContract.Presenter {

    private lateinit var valueEventListener: ValueEventListener

    override fun loadReservations(reservations: ArrayList<Reservation>) {

        loadOwnerReservations(reservations)

    }

    private fun loadPersonalReservations(reservations: ArrayList<Reservation>) {

        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //In caso di dati cambiati, svuoto l'arraylist

                //nascono la textview
                for (singleSnapshot in dataSnapshot.children) {
                    //Creo un oggetto per ogni veicolo
                    val reservation = singleSnapshot.getValue(Reservation::class.java)
                    //aggiungo il veicolo all'array
                    if (reservation?.userState!=2)
                        reservations.add(reservation!!)

                }
                view.onReservationsFetched()
            }

            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        FirebaseModel.fetchPersonalReservations().addListenerForSingleValueEvent(valueEventListener)
    }

    private fun loadOwnerReservations(reservations: ArrayList<Reservation>) {

        valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                reservations.clear()
                for (singleSnapshot in dataSnapshot.children) {
                    //Creo un oggetto per ogni veicolo
                    val reservation = singleSnapshot.getValue(Reservation::class.java)

                    reservation?.isOwner=true
                    //aggiungo il veicolo all'array
                    if (reservation?.ownerState!=2)
                        reservations.add(reservation!!)
                }
                loadPersonalReservations(reservations)

            }

            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        FirebaseModel.fetchOthersReservations().addValueEventListener(valueEventListener)

    }

    override fun stopListener() {
        FirebaseModel.fetchOthersReservations().removeEventListener(valueEventListener)
    }
}