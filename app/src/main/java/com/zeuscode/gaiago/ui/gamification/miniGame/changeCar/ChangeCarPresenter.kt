package com.zeuscode.gaiago.ui.gamification.miniGame.changeCar

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel

class ChangeCarPresenter(private var view:ChangeCarContract.View): ChangeCarContract.Presenter {

    override fun fetchUserLevel() {
        val valueEventListener= object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val score=snapshot.value.toString().toInt()
                val level=score/10
                view.onLevelFetched(level)
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }

        FirebaseModel.fetchUserScore().addListenerForSingleValueEvent(valueEventListener)
    }

}