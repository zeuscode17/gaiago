package com.zeuscode.gaiago.ui.authentication.additionalInformation

import com.zeuscode.gaiago.ui.User
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.authentication.validators.CompositeValidatorFactory
import com.zeuscode.gaiago.ui.authentication.validators.Validator

class AdditionalInformationPresenter(private var view:AdditionalInformationContract.View)
    :AdditionalInformationContract.Presenter{

    val validator: Validator<User> = CompositeValidatorFactory.additionalInformationValidator()

    override fun addUserInformation(user: User) {
        validator.validate(user)?.let {
            //se errati, lancio un errore
            view.onUpdateError(it)
            return
        }
        FirebaseModel.saveUser(user)
        view.onUserInformationSaved()
    }
}