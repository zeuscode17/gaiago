package com.zeuscode.gaiago.ui.profile.editDialogFragments

import androidx.fragment.app.DialogFragment
import com.zeuscode.gaiago.ui.profile.ProfilePresenter

abstract class EditProfileDialogFragment : DialogFragment() {

    var presenter : ProfilePresenter?=null

    fun attach(presenter: ProfilePresenter) {
        this.presenter = presenter
    }
}
