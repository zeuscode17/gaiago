package com.zeuscode.gaiago.ui.license.pictures

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface
import androidx.fragment.app.Fragment
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.license.infoDialogs.LicenseDialogInfo
import com.zeuscode.gaiago.ui.license.infoDialogs.LicenseInfoDialogFragment
import com.zeuscode.gaiago.ui.license.LicenseRegistrationListener
import com.zeuscode.gaiago.extensions.rotate
import com.zeuscode.gaiago.extensions.showDialog
import kotlinx.android.synthetic.main.fragment_license_pictures.*
import kotlinx.android.synthetic.main.fragment_license_pictures.view.*
import java.io.File
import java.io.IOException




class LicensePicturesFragment : Fragment(), LicensePicturesContract.View {

    companion object {
        private const val FILE_PROVIDER_AUTHORITY = "com.zeuscode.gaiago.fileprovider"
        private const val LICENSE_FRONT_SIDE_IMAGE_FILE_NAME = "license_front_side"
        private const val LICENSE_BACK_SIDE_IMAGE_FILE_NAME = "license_back_side"
        const val REQUEST_LICENSE_FRONT_SIDE_IMAGE_CAPTURE = 1
        const val REQUEST_LICENSE_BACK_SIDE_IMAGE_CAPTURE = 2
    }

    private var licenseFrontSideImagePath: String? = null
    private var licenseBackSideImagePath: String? = null

    private lateinit var presenter: LicensePicturesPresenter
    private var listener: LicenseRegistrationListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_license_pictures, container, false)

        presenter = LicensePicturesPresenter(this)

        view.licenseFrontSidePictureButton.setOnClickListener {
            dispatchTakePictureIntent(REQUEST_LICENSE_FRONT_SIDE_IMAGE_CAPTURE)
        }

        view.licenseFrontSidePictureInfoButton.setOnClickListener {
            showDialogWith(LicenseDialogInfo.FRONT_SIDE_DIALOG_INFO)
        }

        view.licenseBackSidePictureButton.setOnClickListener {
            dispatchTakePictureIntent(REQUEST_LICENSE_BACK_SIDE_IMAGE_CAPTURE)
        }

        view.licenseBackSidePictureInfoButton.setOnClickListener {
            showDialogWith(LicenseDialogInfo.BACK_SIDE_DIALOG_INFO)
        }

        view.finishButton.setOnClickListener {
            view.indeterminateBar.visibility=View.VISIBLE
            presenter.onPicturesSubmitted(licenseFrontSideImagePath, licenseBackSideImagePath, listener!!)
        }

        return view
    }

    override fun showNoneLicenseFrontPicture() {
        showNoneLicenseFrontSidePictureDialog()
    }

    override fun showNoneLicenseBackPicture() {
        showNoneLicenseBackSidePictureDialog()
    }

    override fun onUploadError() {
        AlertDialog.Builder(requireContext())
            .setTitle("ERRORE UPLOAD")
            .setMessage("Errore upload immagini patente")
            .setNegativeButton(android.R.string.ok, null)
            .create()
            .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_LICENSE_FRONT_SIDE_IMAGE_CAPTURE) {
                setThumbnail(licenseFrontSidePictureButton, licenseFrontSideImagePath!!)
            }

            if (requestCode == REQUEST_LICENSE_BACK_SIDE_IMAGE_CAPTURE) {
                setThumbnail(licenseBackSidePictureButton, licenseBackSideImagePath!!)
            }
        }
    }

    override fun deleteTemporaryFiles() {
        File(licenseFrontSideImagePath).delete()
        File(licenseBackSideImagePath).delete()
    }

    fun attach(listener: LicenseRegistrationListener) {
        this.listener = listener
    }

    private fun setThumbnail(imageButton: ImageButton, imagePath: String) {
        imageButton.setImageBitmap(generateBitmap(imagePath, imageButton.width, imageButton.height))
    }

    private fun generateBitmap(imagePath: String, width: Int, height: Int): Bitmap? {
        val bitmapOptions = BitmapFactory.Options().apply {
            inJustDecodeBounds = true
            BitmapFactory.decodeFile(imagePath, this)
            val imageWidth: Int = outWidth
            val imageHeight: Int = outHeight

            val scaleFactor: Int = Math.min(imageWidth / width, imageHeight / height)

            inJustDecodeBounds = false
            inSampleSize = scaleFactor
        }

        BitmapFactory.decodeFile(imagePath, bitmapOptions)?.also { bitmap ->
            val exifInterface = ExifInterface(imagePath)
            val orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED)

            return when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> bitmap.rotate(90F)
                ExifInterface.ORIENTATION_ROTATE_180 -> bitmap.rotate(180F)
                ExifInterface.ORIENTATION_ROTATE_270 -> bitmap.rotate(270F)
                else -> bitmap
            }
        }
        return null
    }

    private fun dispatchTakePictureIntent(requestImage: Int) {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(context!!.packageManager)?.also { _ ->
                try {
                    createImageFile(requestImage)
                } catch (ex: IOException) {
                    null
                }?.also {
                    val imageURI: Uri = FileProvider.getUriForFile(
                            context!!,
                        FILE_PROVIDER_AUTHORITY,
                            it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI)
                    startActivityForResult(takePictureIntent, requestImage)
                }
            }
        }
    }

    private fun createImageFile(requestImage: Int): File {
        return if (requestImage == REQUEST_LICENSE_FRONT_SIDE_IMAGE_CAPTURE) {
            createImageFile(LICENSE_FRONT_SIDE_IMAGE_FILE_NAME).also {
                licenseFrontSideImagePath = it.absolutePath
            }
        } else {
            createImageFile(LICENSE_BACK_SIDE_IMAGE_FILE_NAME).also {
                licenseBackSideImagePath = it.absolutePath
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(fileName: String): File {
        val storageDirectory: File = context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File(storageDirectory, "$fileName.jpg")
    }

    private fun showNoneLicenseBackSidePictureDialog() {
        showDialog(
                getString(R.string.none_license_back_image_title),
                R.string.none_license_back_image_message,
                R.string.ok,
                null
        )
    }

    private fun showNoneLicenseFrontSidePictureDialog() {
        showDialog(
                getString(R.string.none_license_front_image_title),
                R.string.none_license_front_image_message,
                R.string.ok,
                null
        )
    }

    private fun showDialogWith(info: LicenseDialogInfo) {
        val fragment = LicenseInfoDialogFragment()

        fragment.arguments = Bundle().apply {
            putSerializable(LicenseInfoDialogFragment.DIALOG_INFO_KEY, info)
        }

        fragment.show(fragmentManager, "dialog")
    }

}
