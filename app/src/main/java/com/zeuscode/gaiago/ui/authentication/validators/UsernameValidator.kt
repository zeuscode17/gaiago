package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.authentication.Credentials

class UsernameValidator :
    Validator<Credentials> {

    override fun validate(item: Credentials): ValidationError? {
        if (item.username.isEmpty()) {
            return ValidationError.EMPTY_USERNAME
        }
        return null
    }

}