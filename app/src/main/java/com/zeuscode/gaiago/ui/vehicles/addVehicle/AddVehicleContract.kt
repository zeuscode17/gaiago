package com.zeuscode.gaiago.ui.vehicles.addVehicle

import android.net.Uri
import com.seatgeek.placesautocomplete.model.Place
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import com.google.android.libraries.places.api.model.Place as GooglePlace

interface AddVehicleContract {
    interface View {
        fun initView()
        fun onImageUploaded(key:String, uri:Uri)
        fun onVehicleUploaded()
        fun onPlaceFetched(place:GooglePlace)
    }

    interface Presenter{
        fun onImageChosen(photoUri : Uri)
        fun uploadVehicle(vehicle: Vehicle)
        fun fetchPlace(place:Place)
    }
}