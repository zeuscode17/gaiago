package com.zeuscode.gaiago.ui.vehicles.showVehicle.showStats

import android.content.ContentValues
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.reservation.Reservation
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class ShowStatisticsPresenter(private var view:ShowStatisticsContract.View) : ShowStatisticsContract.Presenter {

    override fun fetchReservations(vehicleID: String) {

        val weekDays= arrayOf(0.0,0.0,0.0,0.0,0.0,0.0,0.0)
        val formatter = DateTimeFormatter.ofPattern("ddMMyyyy")
        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (singleSnapshot in dataSnapshot.children) {
                    val reservation = singleSnapshot.getValue(Reservation::class.java)!!
                    if (reservation.state==4) {
                        val localDate = LocalDate.parse(reservation.date, formatter)
                        when (localDate.dayOfWeek) {
                            DayOfWeek.MONDAY -> weekDays[0] += reservation.totalCost
                            DayOfWeek.TUESDAY -> weekDays[1] += reservation.totalCost
                            DayOfWeek.WEDNESDAY -> weekDays[2] += reservation.totalCost
                            DayOfWeek.THURSDAY -> weekDays[3] += reservation.totalCost
                            DayOfWeek.FRIDAY -> weekDays[4] += reservation.totalCost
                            DayOfWeek.SATURDAY -> weekDays[5] += reservation.totalCost
                            DayOfWeek.SUNDAY -> weekDays[6] += reservation.totalCost
                            else -> Log.d("DAYNOT FOUND", "DAY NOT FOUND")
                        }
                    }
                }
                view.onReservationsFetched(weekDays)
            }
            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        FirebaseModel.fetchReservationsByVehicleID(vehicleID).addListenerForSingleValueEvent(valueEventListener)
    }
}