package com.zeuscode.gaiago.ui.profile

import com.zeuscode.gaiago.ui.User
import com.zeuscode.gaiago.ui.license.License

data class Profile(var license: License? = null, var user: User? = null)