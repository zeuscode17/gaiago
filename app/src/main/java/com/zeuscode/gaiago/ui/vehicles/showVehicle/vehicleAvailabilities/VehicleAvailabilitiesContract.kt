package com.zeuscode.gaiago.ui.vehicles.showVehicle.vehicleAvailabilities

interface VehicleAvailabilitiesContract{
    interface View {
        fun onVehicleAvailabilitiesFetched()
    }

    interface Presenter {
        fun loadVehicleAvailabilities(availabilities: ArrayList<Availability>, vehicleID:String)
    }
}