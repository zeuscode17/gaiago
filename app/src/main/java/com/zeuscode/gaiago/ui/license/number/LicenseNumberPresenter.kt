package com.zeuscode.gaiago.ui.license.number

import com.zeuscode.gaiago.ui.license.LicenseRegistrationListener

class LicenseNumberPresenter(private var view: LicenseNumberContract.View): LicenseNumberContract.Presenter {

    override fun onLicenseNumberSubmitted(number: String, categoryHeld: Boolean, listener: LicenseRegistrationListener) {
        if(number.isEmpty()) {
            view.showEmptyLicenseNumberDialog()
            return
        }

        if(!categoryHeld) {
            view.showUncheckedLicenseCategoryHeldDialog()
            return
        }

        listener.onLicenseNumberSubmitted(number)
    }

}
