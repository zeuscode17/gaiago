package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.User

class SurnameValidator : Validator<User> {
    override fun validate(item: User): ValidationError? {
        if (item.surname.isEmpty()) {
            return ValidationError.EMPTY_SURNAME
        }

        return null
    }
}