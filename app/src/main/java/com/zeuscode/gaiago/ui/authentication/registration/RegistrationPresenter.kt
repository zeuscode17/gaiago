package com.zeuscode.gaiago.ui.authentication.registration

import android.util.Log
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.authentication.Credentials
import com.zeuscode.gaiago.ui.authentication.validators.CompositeValidatorFactory
import com.zeuscode.gaiago.ui.authentication.validators.Error
import com.zeuscode.gaiago.ui.authentication.validators.Validator

class RegistrationPresenter(private var view: RegistrationContract.View):RegistrationContract.Presenter {

    val validator: Validator<Credentials> = CompositeValidatorFactory.registrationValidator()

    override fun onRegistrationSubmitted(credentials: Credentials) {
        validator.validate(credentials)?.let {
            view.onRegistrationFail(it)
            return
        }
        FirebaseModel.insertUser(credentials.username,credentials.password)
            .addOnCompleteListener { task->
                if (task.isSuccessful) {
                    FirebaseModel.sendVerificationEmail()
                    view.onRegistrationSuccess(credentials.username)
                } else {
                    Log.e("REGISTRATION ERR", "${task.result}")
                }
            }
    }

}