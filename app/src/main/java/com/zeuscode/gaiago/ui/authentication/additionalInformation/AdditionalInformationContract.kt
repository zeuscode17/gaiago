package com.zeuscode.gaiago.ui.authentication.additionalInformation

import com.zeuscode.gaiago.ui.User
import com.zeuscode.gaiago.ui.authentication.validators.Error

interface AdditionalInformationContract {
    interface View{
        fun initView()
        fun onUpdateError(error: Error)
        fun onUserInformationSaved()
    }

    interface Presenter{
        fun addUserInformation(user: User)
    }
}