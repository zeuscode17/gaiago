package com.zeuscode.gaiago.ui.authentication.registration

import com.zeuscode.gaiago.ui.authentication.Credentials
import com.zeuscode.gaiago.ui.authentication.validators.Error

interface RegistrationContract {

    interface View {
        fun initView(view: android.view.View)
        fun onRegistrationFail(error:Error)
        fun onRegistrationSuccess(email:String)
    }

    interface Presenter {
        fun onRegistrationSubmitted(credentials: Credentials)
    }

}