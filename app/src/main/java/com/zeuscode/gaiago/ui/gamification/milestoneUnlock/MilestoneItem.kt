package com.zeuscode.gaiago.ui.gamification.milestoneUnlock


data class MilestoneItem(
    var id: Int,
    var lvl: Int,
    var status: Char)