package com.zeuscode.gaiago.ui.authentication.validators

interface Error {
    val title: Int
    val message: Int
}