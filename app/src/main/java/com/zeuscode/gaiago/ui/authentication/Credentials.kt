package com.zeuscode.gaiago.ui.authentication

data class Credentials(val username: String, val password: String, val confirmPassword: String? = password)