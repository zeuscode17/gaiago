package com.zeuscode.gaiago.ui.profile

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.User
import com.zeuscode.gaiago.ui.authentication.login.LoginActivity
import com.zeuscode.gaiago.ui.license.License
import com.zeuscode.gaiago.ui.license.LicenseRegistrationActivity
import com.zeuscode.gaiago.utils.DateUtils
import com.zeuscode.gaiago.ui.profile.editDialogFragments.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.android.synthetic.main.license.*

class ProfileFragment: Fragment(), ProfileContract.View {

    private lateinit var presenter: ProfilePresenter
    private var user: User?=null
    private var license:License?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        presenter= ProfilePresenter(this)

        view.editNameButton.setOnClickListener {
            showDialog(EditNameSurnameDialogFragment(user))
        }

        view.editPhoneNumberButton.setOnClickListener {
            showDialog(EditPhoneNumberDialogFragment(user))
        }

        view.editEmailButton.setOnClickListener {
            showDialog(EditEmailDialogFragment(user))
        }

        view.editBirthDateButton.setOnClickListener {
            showDialog(EditBirthDateDialogFragment(user))
        }

        view.editAddressButton.setOnClickListener {
            showDialog(EditAddressDialogFragment(user))
        }

        //Listener modifica patente
        view.editLicenseButton.setOnClickListener {
            //Lancio Activity per inserimento patente
            val intent = Intent(requireContext(), LicenseRegistrationActivity::class.java)
            startActivity(intent)
        }

        view.deleteAccountButton.setOnClickListener {
            AlertDialog.Builder(requireContext())
                .setTitle("Cancella il tuo account")
                .setMessage("Sei certo di voler cancellare il tuo account?")
                .setNegativeButton("No", null)
                .setPositiveButton("Conferma") { _,_ ->
                    presenter.deleteAccount()
                }
                .create()
                .show()
        }


        return view
    }


    override fun onStart() {
        super.onStart()

        presenter.loadProfile()
    }

    private fun showDialog(fragment: EditProfileDialogFragment) {
        fragment.attach(presenter)
        fragment.show(fragmentManager, "dialog")
    }

    //carico nelle apposite textview i dati dell'utente
    override fun onProfileLoaded(profile: Profile, progressBar: CharArray) {
        if (this.isVisible) {
            license = profile.license
            user = profile.user

            updateProgressBar(progressBar)

            showProfile()
        }
    }

    override fun updateProgressBar(progress: CharArray) {

        if (!progress.contains('0'))
        {
            stepTextView.visibility=View.GONE
            textView6.visibility=View.GONE
            profileProgressBar.visibility=View.GONE
        } else {
            profileProgressBar.progress=progress.count { it==('1')}
            stepTextView.text = when (progress.indexOfFirst { it == ('0') }) {
                0 -> "Numero di cellulare"
                1 -> "Data di nascita"
                2 -> "Indirizzo di casa"
                3 -> "Inserisci la tua patente"
                4 -> "Inserisci un veicolo"
                5 -> "Effettua una prenotazione"
                else -> ""
            }
        }
    }

    override fun showProfile() {
        license?.let{
            expirationDateTextView.text = DateUtils.toStringDate(it.expirationDate)
            licenseNumberTextView.text = it.number
        }

        user?.let {
            val nameSurname= "${it.name} ${it.surname}"
            profileNameTextView.text = nameSurname
            profileEmailTextView.text = it.email
            profilePhoneNumberTextView.text = it.phoneNumber
            it.birthDate?.let { date->
                profileBirthDateTextView.text = DateUtils.toStringDate(date)
            }
            profileAddressTextView.text = it.address
        }
    }

    override fun onAccountDeleted() {
        Toast.makeText(requireContext(), "Account cancellato", Toast.LENGTH_SHORT).show()
        val intent= Intent(requireContext(), LoginActivity::class.java)
        startActivity(intent)
    }






}
