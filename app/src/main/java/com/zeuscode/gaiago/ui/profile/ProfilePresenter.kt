package com.zeuscode.gaiago.ui.profile

import android.content.ContentValues
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.ui.User
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.license.License

class ProfilePresenter(private var view: ProfileContract.View) : ProfileContract.Presenter {

    private lateinit var progressBar: CharArray

    override fun loadProfile() {
        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                val user = dataSnapshot.getValue(User::class.java)
                val license = dataSnapshot.child("license").getValue(License::class.java)
                val profile= Profile(license, user)

                progressBar=user!!.progressbar.toCharArray()

                view.onProfileLoaded(profile, progressBar)

            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(ContentValues.TAG, "loadPost:onCancelled", databaseError.toException())

            }
        }

        FirebaseModel.fetchUser().addListenerForSingleValueEvent(valueEventListener)
    }

    override fun updateNameSurname(name: String, surname: String) {
        FirebaseModel.updateNameSurname(name,surname)
        view.showProfile()
    }

    override fun updatePhoneNumber(phoneNumber: String) {
        if (progressBar[0]=='0') {
            progressBar[0]='1'
            view.updateProgressBar(progressBar)
            FirebaseModel.editProgressBar(progressBar.joinToString(""))
        }
        FirebaseModel.updatePhoneNumber(phoneNumber)
        view.showProfile()
    }

    override fun updateEmail(email: String) {
        FirebaseModel.updateEmail(email)
        view.showProfile()
    }

    override fun updateBirthDate(date: Long) {
        if (progressBar[1]=='0') {
            progressBar[1]='1'
            view.updateProgressBar(progressBar)
            FirebaseModel.editProgressBar(progressBar.joinToString(""))
        }
        FirebaseModel.updateBirthDate(date)
        view.showProfile()
    }

    override fun updateAddress(address:String) {
        if (progressBar[2]=='0') {
            progressBar[2]='1'
            view.updateProgressBar(progressBar)
            FirebaseModel.editProgressBar(progressBar.joinToString(""))
        }
        FirebaseModel.updateAddress(address)
        view.showProfile()
    }

    override fun deleteAccount() {
        FirebaseModel.deleteUser()?.addOnCompleteListener {
            if (it.isSuccessful) {
                view.onAccountDeleted()
            }
        }
    }

}