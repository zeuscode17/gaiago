package com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider.availableVehicles.selectedVehicle

import com.zeuscode.gaiago.ui.reservation.Reservation

interface SelectedVehicleContract {
    interface View {
        fun onReservationInserted()
    }

    interface Presenter {

        fun onBookingConfirmed(reservation: Reservation, vehicleID:String, newAvailability:String)
    }

}