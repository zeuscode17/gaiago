package com.zeuscode.gaiago.ui.license.pictures

import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.license.LicenseRegistrationListener

class LicensePicturesPresenter(private var view: LicensePicturesContract.View) : LicensePicturesContract.Presenter {

    private var frontPicturePath: String? = null
    private var backPicturePath: String? = null
    private var listener: LicenseRegistrationListener? = null

    override fun onPicturesSubmitted(frontPicturePath: String?, backPicturePath: String?, listener: LicenseRegistrationListener) {

        if(frontPicturePath.isNullOrEmpty()) {
            view.showNoneLicenseFrontPicture()
            return
        }

        if(backPicturePath.isNullOrEmpty()) {
            view.showNoneLicenseBackPicture()
            return
        }

        this.frontPicturePath=frontPicturePath
        this.backPicturePath=backPicturePath
        this.listener=listener

        saveFront()
    }

    private fun saveFront() {
        FirebaseModel.saveFrontLicensePicture(frontPicturePath!!)
            .addOnFailureListener {
            view.onUploadError()
        }.addOnSuccessListener {
            saveBack()
        }
    }

    private fun saveBack() {
        FirebaseModel.saveBackLicensePicture(backPicturePath!!)
            .addOnFailureListener {
            view.onUploadError()
        }.addOnSuccessListener {
                view.deleteTemporaryFiles()
                listener!!.onLicensePicturesSubmitted()
        }
    }

}
