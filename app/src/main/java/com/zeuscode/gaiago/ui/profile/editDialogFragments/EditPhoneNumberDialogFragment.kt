package com.zeuscode.gaiago.ui.profile.editDialogFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.User
import kotlinx.android.synthetic.main.fragment_edit_phone_number_dialog.*
import kotlinx.android.synthetic.main.fragment_edit_phone_number_dialog.view.*

class EditPhoneNumberDialogFragment(private var user: User?) : EditProfileDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_edit_phone_number_dialog, container, false)


        view.phoneNumberEditText.setText(user?.phoneNumber)

        view.okButton.setOnClickListener {
            val newPhoneNumber=phoneNumberEditText.text.toString()
            user?.phoneNumber=newPhoneNumber
            presenter?.updatePhoneNumber(newPhoneNumber)
            this.dismiss()
        }

        view.cancelButton.setOnClickListener {
            this.dismiss()
        }

        return view
    }
}
