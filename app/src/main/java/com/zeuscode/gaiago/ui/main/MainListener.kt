package com.zeuscode.gaiago.ui.main

import com.zeuscode.gaiago.ui.reservation.Reservation

interface MainListener {
    fun onVehiclesSearch(reservation: Reservation, latitude: Double, longitude:Double)
    fun onSearchClick()
    fun onLendClick()
}

