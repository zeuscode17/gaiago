package com.zeuscode.gaiago.ui.splashScreen

interface SplashScreenContract{

    interface View{
        fun showMainView()
        fun showLoginView()
    }

    interface Presenter{
        fun checkUserAuthentication()
    }
}