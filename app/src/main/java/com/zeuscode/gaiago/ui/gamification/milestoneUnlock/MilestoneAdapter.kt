package com.zeuscode.gaiago.ui.gamification.milestoneUnlock

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.zeuscode.gaiago.R
import java.util.ArrayList
import kotlinx.android.synthetic.main.milestone_grid_item.view.*


class MilestoneAdapter(private var context: Context, private var choosenPrizes: ArrayList<MilestoneItem>,
                       private var prizesDB: ArrayList<Prize>): BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val milestonePrize = choosenPrizes[position]
        val prize = prizesDB[milestonePrize.id]

        val im= when (milestonePrize.status) {
            'U' -> R.drawable.padlock
            'R' -> R.drawable.tick
            else -> R.drawable.logonew
        }

        if (convertView==null) {
            val inflator = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflator.inflate(R.layout.milestone_grid_item, null)

            view.textLvl.text="Lv. ${milestonePrize.lvl}"
            view.prizeImageView.setImageResource(prize.image)

            view.itemStatusImageView.setImageResource(im)
            return view
        }

        convertView.textLvl.text="Lv. ${milestonePrize.lvl}"
        convertView.prizeImageView.setImageResource(prize.image)
        convertView.itemStatusImageView.setImageResource(im)

        return convertView

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Any {
        return choosenPrizes[position]
    }

    override fun getCount(): Int {
        return choosenPrizes.size
    }
}
