package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.authentication.Credentials
import com.zeuscode.gaiago.ui.User

class CompositeValidatorFactory {

    companion object {
        fun registrationValidator(): Validator<Credentials> {
            return CompositeValidator(
                arrayOf(
                    EmailValidator(),
                    PasswordValidator(),
                    ConfirmPasswordValidator()
                )
            )
        }

        fun additionalInformationValidator(): Validator<User> {
            return CompositeValidator(
                arrayOf(
                    NameValidator(),
                    SurnameValidator()
                )
            )
        }

        fun authenticationValidator(): Validator<Credentials> {
            return CompositeValidator(
                arrayOf(
                    UsernameValidator(),
                    AuthenticationPasswordValidator()
                )
            )
        }
    }


}
