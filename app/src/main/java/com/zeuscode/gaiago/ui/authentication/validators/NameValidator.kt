package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.User

class NameValidator : Validator<User> {
    override fun validate(item: User): ValidationError? {
        if (item.name.isEmpty()) {
            return ValidationError.EMPTY_NAME
        }
        return null
    }
}