package com.zeuscode.gaiago.ui.authentication.validators

class CompositeValidator<T>(private val validators: Array<Validator<T>>):
    Validator<T> {

    override fun validate(item: T) : ValidationError? {

        validators.forEach {
            it.validate(item)?.let {
                return it
            }
        }

        return null
    }
}