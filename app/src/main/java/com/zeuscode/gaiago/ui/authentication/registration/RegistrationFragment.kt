package com.zeuscode.gaiago.ui.authentication.registration

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.authentication.Credentials
import com.zeuscode.gaiago.ui.authentication.additionalInformation.AdditionalInformationActivity
import com.zeuscode.gaiago.ui.authentication.validators.Error
import kotlinx.android.synthetic.main.fragment_registration.*
import kotlinx.android.synthetic.main.next_button.view.*



class RegistrationFragment : Fragment(), RegistrationContract.View {

    private lateinit var presenter: RegistrationPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_registration, container, false)

        presenter= RegistrationPresenter(this)

        initView(view)

        return view
    }

    override fun initView(view: View) {
        //Setting the GO button listener
        view.nextButton.setOnClickListener {
            presenter.onRegistrationSubmitted(extractCredentials())
        }
    }

    //Credential extraction function
    private fun extractCredentials(): Credentials {
        val username = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
        val confirmPassword = confirmPasswordEditText.text.toString()
        //an object of type "Credentials" is returned
        return Credentials(username, password, confirmPassword)
    }

    //In case of registration failure an alert dialogue with the error description is shown
    override fun onRegistrationFail(error: Error) {
        AlertDialog.Builder(context)
            .setTitle(error.title)
            .setMessage(error.message)
            .setNegativeButton(android.R.string.ok, null)
            .create()
            .show()
    }

    override fun onRegistrationSuccess(email:String) {
        //Activity load for additional information
        val intent = Intent(requireContext(), AdditionalInformationActivity::class.java)
        //Passo all'intent l'email, verrà salvata successivamente nell'area personale//?//
        intent.putExtra("email", email)
        startActivity(intent)
    }


}