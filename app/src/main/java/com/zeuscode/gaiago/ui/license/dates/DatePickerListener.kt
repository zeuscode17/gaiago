package com.zeuscode.gaiago.ui.license.dates

interface DatePickerListener {
    fun onDatePicked(date: Long)
}
