package com.zeuscode.gaiago.ui.vehicles.showVehicle.showStats

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.zeuscode.gaiago.R
import kotlinx.android.synthetic.main.activity_statistics.*


class ShowStatitisticsActivity : AppCompatActivity(), ShowStatisticsContract.View {

    private lateinit var presenter:ShowStatisticsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistics)

        presenter=ShowStatisticsPresenter(this)

        val vehicleID = intent?.extras?.getString("VEHICLEID")!!
        val brandModel= intent?.extras?.getString("BMODEL")!!

        upperBrandModelTextView.text=brandModel

        presenter.fetchReservations(vehicleID)

    }

    override fun onReservationsFetched(weekDays:Array<Double>) {
        var totalIncome=0.0
        for (i in weekDays) totalIncome+=i
        totalIncomeTextView.text="$totalIncome €"
        monIncomeTextView.text="${weekDays[0]} €"
        tueIncomeTextView.text="${weekDays[1]} €"
        wedIncomeTextView.text="${weekDays[2]} €"
        thuIncomeTextView.text="${weekDays[3]} €"
        friIncomeTextView.text="${weekDays[4]} €"
        satIncomeTextView.text="${weekDays[5]} €"
        sunIncomeTextView.text="${weekDays[6]} €"

    }
}
