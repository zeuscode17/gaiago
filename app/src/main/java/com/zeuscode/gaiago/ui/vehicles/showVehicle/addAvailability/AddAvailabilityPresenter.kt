package com.zeuscode.gaiago.ui.vehicles.showVehicle.addAvailability

import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

class AddAvailabilityPresenter(private var view: AddAvailabilityContract.View):AddAvailabilityContract.Presenter {

    private val totallyFull=  "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"

    override fun addFixedAvailability(vehicle: Vehicle, date: String?, startHour: Int?, endHour: Int?) {

        val newAvailability = if (vehicle.availability.containsKey(date)) {
            setDayAvailability(vehicle.availability[date]!!,startHour!!,endHour!!)
        } else {
            setDayAvailability(totallyFull,startHour!!,endHour!!)
        }

        FirebaseModel.addVehicleAvailability(vehicle.vehicleID,date!!,newAvailability)
            .addOnSuccessListener {
                view.onAvailabilityUpdated()
            }

    }

    override fun addRepeatedAvailability(
        vehicle: Vehicle,
        startHour: Int?,
        endHour: Int?,
        week: LinkedHashMap<String, Boolean>,
        repeatFor: Int
    ) {
        val availability=setDayAvailability(totallyFull,startHour!!,endHour!!)
        var startDate = LocalDate.now()
        val days=ArrayList<String>()

        val endDate= when (repeatFor) {
            0->startDate.plusWeeks(1)
            1->startDate.plusWeeks(2)
            2->startDate.plusWeeks(3)
            3->startDate.plusMonths(1)
            else -> startDate.plusMonths(2)
        }


        val formatter = DateTimeFormatter.ofPattern("ddMMyyyy")
        while (!startDate.isEqual(endDate))
        {
            startDate=startDate.plusDays(1)
            val dow = startDate.dayOfWeek
            val dayName = dow.getDisplayName(TextStyle.SHORT, Locale.ITALIAN).capitalize()
            if (week[dayName]==true) days.add(startDate.format(formatter))
        }

        FirebaseModel.addRepeatedAvailability(vehicle.vehicleID,days,availability)

        view.onAvailabilityUpdated()

    }

    private fun setDayAvailability(dayAvailability:String, startHour:Int, endHour:Int):String {
        val availabilityLength=endHour-startHour
        return dayAvailability.replaceRange(startHour,endHour,"0".repeat(availabilityLength))
    }
}
