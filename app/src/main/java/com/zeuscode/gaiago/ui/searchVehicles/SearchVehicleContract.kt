package com.zeuscode.gaiago.ui.searchVehicles

import com.seatgeek.placesautocomplete.model.Place
import com.google.android.libraries.places.api.model.Place as GooglePlace

interface SearchVehicleContract {

    interface View {
        fun onPlaceFetched(place: GooglePlace)
    }

    interface Presenter {
        fun fetchPlace(place: Place)
    }

}