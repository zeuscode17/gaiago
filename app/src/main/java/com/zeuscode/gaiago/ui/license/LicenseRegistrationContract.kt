package com.zeuscode.gaiago.ui.license

interface LicenseRegistrationContract {

    interface View {
        fun showLicenseDatesRegistration()
        fun showLicensePicturesRegistration()
        fun showMainActivity()
    }

    interface Presenter {
        fun onLicenseNumberSubmitted(licenseNumber: String)
        fun onLicenseDatesSubmitted(releaseDate: Long, expirationDate: Long)
        fun onLicensePicturesSubmitted()
    }
}