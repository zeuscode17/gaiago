package com.zeuscode.gaiago.ui.vehicles.vehicleList

import com.zeuscode.gaiago.ui.vehicles.Vehicle

interface VehicleListContract {
    interface View {
        fun onVehiclesFetched()
    }

    interface Presenter {
        fun loadVehicles(vehicles:ArrayList<Vehicle>)
    }
}