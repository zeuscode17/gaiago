package com.zeuscode.gaiago.ui.license.dates

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.license.infoDialogs.LicenseDialogInfo
import com.zeuscode.gaiago.ui.license.infoDialogs.LicenseInfoDialogFragment
import com.zeuscode.gaiago.ui.license.LicenseRegistrationListener
import com.zeuscode.gaiago.extensions.showDialog
import com.zeuscode.gaiago.extensions.twoDigitsString
import kotlinx.android.synthetic.main.fragment_license_dates.*
import kotlinx.android.synthetic.main.fragment_license_dates.view.*
import kotlinx.android.synthetic.main.next_button.view.*
import java.util.*

class LicenseDatesFragment : Fragment(), LicenseDatesContract.View {

    private lateinit var listener: LicenseRegistrationListener
    private lateinit var presenter: LicenseDatesPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_license_dates, container, false)

        presenter = LicenseDatesPresenter(this)

        view.nextButton.setOnClickListener {
            presenter.onDatesSubmitted(licenseReleaseDateEditText.text.toString(), licenseExpirationDateEditText.text.toString(), listener)
        }

        view.licenseReleaseDateEditText.setOnClickListener {
            val fragment = LicenseDatePickerDialogFragment()
            fragment.attach(validityDatePickerListener)
            fragment.show(fragmentManager, "dialog")
        }

        view.licenseExpirationDateEditText.setOnClickListener {
            val fragment = LicenseDatePickerDialogFragment()
            fragment.attach(expirationDatePickerListener)
            fragment.show(fragmentManager, "dialog")
        }

        view.licenseValidityInfoButton.setOnClickListener {
            showDialogWith(LicenseDialogInfo.VALIDITY_DATE_DIALOG_INFO)
        }

        view.licenseExpirationInfoButton.setOnClickListener {
            showDialogWith(LicenseDialogInfo.EXPIRATION_DATE_DIALOG_INFO)
        }

        return view
    }

    override fun showEmptyLicenseReleaseDate() {
        showEmptyLicenseReleaseDateDialog()
    }

    override fun showEmptyLicenseExpirationDate() {
        showEmptyLicenseDateExpirationDialog()
    }

    private fun showEmptyLicenseDateExpirationDialog() {
        showDialog(
                getString(R.string.empty_license_expiration_date_title),
                R.string.empty_license_expiration_date_message,
                R.string.ok,
                null
        )
    }

    private fun showEmptyLicenseReleaseDateDialog() {
        showDialog(
                getString(R.string.empty_license_release_date_title),
                R.string.empty_license_release_date_message,
                R.string.ok,
                null
        )
    }

    fun attach(listener: LicenseRegistrationListener) {
        this.listener = listener
    }

    private val validityDatePickerListener = object : DatePickerListener {
        override fun onDatePicked(date: Long) {
            licenseReleaseDateEditText.setText(displayedDateFrom(date), TextView.BufferType.EDITABLE)
        }
    }

    private val expirationDatePickerListener = object : DatePickerListener {
        override fun onDatePicked(date: Long) {
            licenseExpirationDateEditText.setText(displayedDateFrom(date), TextView.BufferType.EDITABLE)
        }
    }

    private fun displayedDateFrom(date: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = date

        val dayOfMonth = calendar[Calendar.DAY_OF_MONTH].twoDigitsString()
        val month = (calendar[Calendar.MONTH] + 1).twoDigitsString()
        val year = calendar[Calendar.YEAR]

        return "$dayOfMonth/$month/$year"
    }

    private fun showDialogWith(info: LicenseDialogInfo) {
        val fragment = LicenseInfoDialogFragment()

        fragment.arguments = Bundle().apply {
            putSerializable(LicenseInfoDialogFragment.DIALOG_INFO_KEY, info)
        }

        fragment.show(fragmentManager, "dialog")
    }

}
