package com.zeuscode.gaiago.ui.gamification

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.gamification.dailyRewards.DailyRewardsActivity
import com.zeuscode.gaiago.ui.gamification.leaderBoard.LeaderboardActivity
import com.zeuscode.gaiago.ui.gamification.luckySpin.LuckySpinActivity
import com.zeuscode.gaiago.ui.gamification.milestoneUnlock.MilestoneUnlockActivity
import com.zeuscode.gaiago.ui.gamification.miniGame.MiniGameActivity
import kotlinx.android.synthetic.main.fragment_gamification.view.*

class GamificationFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_gamification, container, false)

        initLeaderBoard(view)
        initMilestoneUnlock(view)
        initDailyRewards(view)
        initMinigame(view)

       setUserScore(view)
        return view
    }

    private fun setUserScore(view: View){
        val postListenerUser = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var score=dataSnapshot.value.toString()
                view.TextViewScore.text="$score punti esperienza"
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(ContentValues.TAG, "loadPost:onCancelled", databaseError.toException())
            }
        }
        //aggiungo listener alla reference
        FirebaseModel.fetchUserScore().addListenerForSingleValueEvent(postListenerUser)
    }
    private fun initLeaderBoard(view: View){
        view.leaderBoardCardView.setOnClickListener {
            val intent = Intent(requireContext(), LeaderboardActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initMilestoneUnlock(view: View) {
        view.milestoneCardView.setOnClickListener {
            val intent = Intent(requireContext(), MilestoneUnlockActivity::class.java)
            startActivity(intent)
        }
    }
    private fun initDailyRewards(view: View) {
        view.dailyRewardCardView.setOnClickListener {
            val intent = Intent(requireContext(), DailyRewardsActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initMinigame(view: View) {
        view.miniGameCardView.setOnClickListener {
            val intent= Intent(requireContext(), MiniGameActivity::class.java)
            startActivity(intent)
        }
    }

}