package com.zeuscode.gaiago.ui.searchVehicles

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.libraries.places.api.Places
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.pickerDialogs.dialogs.DatePickerDialogFragment
import com.zeuscode.gaiago.extensions.pickerDialogs.dialogs.HourPickerDialogFragment
import com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces.DatePickerListener
import com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces.HourPickerListener
import com.zeuscode.gaiago.extensions.twoDigitsString
import com.zeuscode.gaiago.ui.main.MainListener
import com.zeuscode.gaiago.ui.reservation.Reservation
import kotlinx.android.synthetic.main.fragment_vehicle_search.*
import kotlinx.android.synthetic.main.fragment_vehicle_search.view.*
import java.util.*
import com.google.android.libraries.places.api.model.Place as GooglePlace

class SearchVehiclesFragment : Fragment(), SearchVehicleContract.View {

    private lateinit var presenter: SearchVehiclePresenter
    private var mainListener: MainListener? = null
    private var reservationDate: String? = null
    private var reservationStartHour: Int? = null
    private var reservationEndHour: Int? = null
    private var latitude:Double? = null
    private var longitude:Double? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_vehicle_search, container, false)

        // Initialize Places.
        Places.initialize(requireContext(), resources.getString(R.string.maps_api_key))
        // Create a new Places client instance.
        presenter = SearchVehiclePresenter(this, Places.createClient(requireContext()))

        view.bookingDateEditText.setOnClickListener {
            val fragment = DatePickerDialogFragment()
            fragment.attach(validityDatePickerListener)
            fragment.show(fragmentManager, "dialog")
        }

        view.bookingStartHourEditText.setOnClickListener {
            val fragment = HourPickerDialogFragment()
            fragment.attach(startHourPickerListener)
            fragment.show(fragmentManager, "dialog")
        }

        view.bookingEndHourEditText.setOnClickListener {
            val fragment = HourPickerDialogFragment()
            fragment.attach(endHourPickerListener)
            fragment.show(fragmentManager, "dialog")
        }

        view.searchButton.setOnClickListener {
            checkField()
        }

        view.placesAutocomplete.setOnPlaceSelectedListener {
            presenter.fetchPlace(it)
        }

        return view
    }

    fun setMainListener(listener: MainListener) {
        this.mainListener = listener
    }

    override fun onPlaceFetched(place: com.google.android.libraries.places.api.model.Place) {
        latitude = place.latLng?.latitude
        longitude = place.latLng?.longitude
    }

    private fun checkField() {
        if (reservationDate.isNullOrBlank())
            showDialog("DATA NON INSERITA", "Inserisci la data per continuare")
        else if (reservationStartHour==null)
            showDialog("ORA DI INIZIO NON INSERITA", "Inserisci l\'ora di inizio per continuare")
        else if (reservationEndHour==null)
            showDialog("ORA DI FINE NON INSERITA", "Inserisci l\'ora di fine per continuare")
        else if (reservationStartHour!!>=reservationEndHour!!)
            showDialog("ORE INSERITE SCORRETTAMENTE", "L'ora di inizio non può essere maggiore dell'ora di fine")
        else if (latitude==null || longitude==null)
            showDialog("INDIRIZZO NON INSERITO", "Inserisci un indirizzo per continuare")
        else searchVehicles()
    }

    private fun searchVehicles() {
            val reservation =
                Reservation(reservationDate!!, reservationStartHour!!, reservationEndHour!!)

            mainListener?.onVehiclesSearch(reservation, latitude!!, longitude!!)

    }

    private val validityDatePickerListener = object :
        DatePickerListener {
        override fun onDatePicked(date: Long) {
            bookingDateEditText.setText(displayedDateFrom(date), TextView.BufferType.EDITABLE)
        }
    }

    private fun displayedDateFrom(date: Long): String {

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = date

        val dayOfMonth = calendar[Calendar.DAY_OF_MONTH].twoDigitsString()
        val month = (calendar[Calendar.MONTH] + 1).twoDigitsString()
        val year = calendar[Calendar.YEAR]

        reservationDate="$dayOfMonth$month$year"

        return "$dayOfMonth/$month/$year"
    }

    private val startHourPickerListener = object:
        HourPickerListener {
        override fun onHourPicked(hourPicked:String,interval:Int) {
            bookingStartHourEditText.setText(hourPicked, TextView.BufferType.EDITABLE)
            reservationStartHour=interval
        }
    }

    private val endHourPickerListener = object:
        HourPickerListener {
        override fun onHourPicked(hourPicked:String,interval:Int) {
            bookingEndHourEditText.setText(hourPicked, TextView.BufferType.EDITABLE)
            reservationEndHour=interval
        }
    }

    private fun showDialog(title:String, message:String) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setNegativeButton(android.R.string.ok, null)
            .create()
            .show()

    }

}
