package com.zeuscode.gaiago.ui.authentication.additionalInformation

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.User
import com.zeuscode.gaiago.ui.authentication.validators.Error
import com.zeuscode.gaiago.extensions.KeyboardVisibilityListener
import com.zeuscode.gaiago.extensions.setKeyboardVisibilityListener
import com.zeuscode.gaiago.ui.introslider.WelcomeActivity
import com.zeuscode.gaiago.ui.main.MainActivity
import kotlinx.android.synthetic.main.form_additional_information.*
import kotlinx.android.synthetic.main.activity_additional_information.*

class AdditionalInformationActivity : Activity(), KeyboardVisibilityListener, AdditionalInformationContract.View {

    private lateinit var presenter: AdditionalInformationContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_additional_information)

        presenter= AdditionalInformationPresenter(this)

        initView()

        setKeyboardVisibilityListener(this)
    }

    override fun initView() {
        createAccountButton.setOnClickListener {
            presenter.addUserInformation(extractUserInformation())
        }
    }

    //estraggo nome e cognome dai campi dati
    //l'email viene richiesta dagli extra dell'intent della registrazione
    private fun extractUserInformation(): User {
        val name = nameEditText.text.toString()
        val surname = surnameEditText.text.toString()
        val email= intent?.extras?.get("email").toString()

        return User(name, surname, email)
    }

    override fun onUserInformationSaved() {
        val intent = Intent(this, WelcomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    //AlertDialog in caso di errore
    override fun onUpdateError(error: Error) {
        AlertDialog.Builder(this)
            .setTitle(error.title)
            .setMessage(error.message)
            .setNegativeButton(android.R.string.ok, null)
            .create()
            .show()
    }

    override fun onKeyboardVisibilityChanged(keyboardVisible: Boolean) {
        if (keyboardVisible)
            scrollView.scrollY = 10000
    }

}