package com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider.availableVehicles

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.RecyclerItemClickListener
import com.zeuscode.gaiago.ui.reservation.Reservation
import com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider.availableVehicles.selectedVehicle.SelectedVehicleActivity
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import com.zeuscode.gaiago.ui.vehicles.vehicleList.VehiclesRecyclerViewAdapter
import kotlinx.android.synthetic.main.fragment_available_vehicles.*

class AvailableVehiclesFragment : Fragment() {

    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val vehicles : ArrayList<Vehicle> = ArrayList()

    private lateinit var reservation: Reservation

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_available_vehicles, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //carico la recycler view
        loadRecycler()
        loadRecyclerListener()

    }

    //Funzione per caricare la recycler
    private fun loadRecycler() {

        viewManager = LinearLayoutManager(requireContext())

        viewAdapter = VehiclesRecyclerViewAdapter(vehicles)

        recyclerView.apply {
            //Uso questa impostazione se so che il layout delle card non cambierà
            //migliora le performance
            setHasFixedSize(true)
            // Specifico il layout manager
            layoutManager = viewManager
            // Specifico l'adapter
            adapter = viewAdapter
        }

    }

    private fun loadRecyclerListener() {

        recyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                requireContext(),
                recyclerView,
                object : RecyclerItemClickListener.OnItemClickListener {

                    //Click rapido
                    override fun onItemClick(view: View, position: Int) {
                        //Creo un Intent per mostrare il veicolo selezionato
                        val intent = Intent(context, SelectedVehicleActivity::class.java)

                        intent.putExtra("VEHICLE", vehicles[position])
                        intent.putExtra("RESERVATION", reservation)
                        // start your next activity
                        startActivity(intent)
                    }

                    //Click Lungo
                    override fun onLongItemClick(view: View?, position: Int) {
                        //do whatever..
                    }
                })
        )
    }

    override fun onStart() {
        super.onStart()
        viewAdapter.notifyDataSetChanged()
    }

    fun updateVehicleList(vehicles: ArrayList<Vehicle>, reservation: Reservation) {
        this.vehicles.addAll(vehicles)
        this.reservation=reservation
        if (this.isVisible)
            viewAdapter.notifyDataSetChanged()
    }




}