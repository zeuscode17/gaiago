package com.zeuscode.gaiago.ui.reservation

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.RecyclerItemClickListener
import com.zeuscode.gaiago.ui.reservation.showReservation.ShowReservationActivity
import kotlinx.android.synthetic.main.fragment_reservations_list.*

class ReservationsListFragment : Fragment(), ReservationsListContract.View {

    private lateinit var presenter: ReservationsListPresenter
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val reservations : ArrayList<Reservation> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_reservations_list, container, false)

        presenter=ReservationsListPresenter(this)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //carico la recycler view
        if (reservations.isEmpty()) noReservationTextView.visibility=View.VISIBLE
        else noReservationTextView.visibility=View.GONE
        initRecycler()
        initRecyclerListener()
    }

    override fun onStart() {
        super.onStart()
        presenter.loadReservations(reservations)
    }

    override fun onStop() {
        super.onStop()
        presenter.stopListener()
    }

    //Funzione per caricare la recycler
    private fun initRecycler() {
        //LinearLayoutManager, le cardView verranno disposte una sopra l'altra
        viewManager = LinearLayoutManager(requireContext())

        viewAdapter = ReservationsRecyclerViewAdapter(reservations)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    private fun initRecyclerListener() {
        recyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                requireContext(),
                recyclerView,
                object : RecyclerItemClickListener.OnItemClickListener {

                    //Click rapido
                    override fun onItemClick(view: View, position: Int) {

                        showReservation(position)
                    }

                    //Click Lungo
                    override fun onLongItemClick(view: View?, position: Int) {
                    }
                })
        )
    }


    override fun onReservationsFetched() {
        if (this.isVisible) {
            if (reservations.isEmpty()) noReservationTextView.visibility = View.VISIBLE
            else noReservationTextView.visibility = View.GONE
            viewAdapter.notifyDataSetChanged()
        }
    }

    private fun showReservation(position: Int) {
        val intent = Intent(context, ShowReservationActivity::class.java)
        //Passo tutti i parametri del veicolo
        intent.putExtra("RESERVATION",reservations[position])
        // start your next activity
        startActivity(intent)
    }

}



