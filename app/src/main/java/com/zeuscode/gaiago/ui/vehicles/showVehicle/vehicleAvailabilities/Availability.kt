package com.zeuscode.gaiago.ui.vehicles.showVehicle.vehicleAvailabilities

data class Availability(
    var date:String="",
    var hours:String=""
)