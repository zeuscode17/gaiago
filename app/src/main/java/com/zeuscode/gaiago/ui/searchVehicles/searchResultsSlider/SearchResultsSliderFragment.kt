package com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.reservation.Reservation
import com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider.availableVehicles.AvailableVehiclesFragment
import com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider.vehicleMap.VehiclesMapFragment
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import kotlinx.android.synthetic.main.fragment_search_results_tabs.view.*

class SearchResultsSliderFragment : Fragment(), SearchResultsSliderContract.View {

    private lateinit var presenter: SearchResultsSliderPresenter
    private lateinit var searchResultsPagerAdapter: SearchResultsAdapter
    private lateinit var viewPager: ViewPager
    private val vehicles : ArrayList<Vehicle> = ArrayList()
    private var availableVehiclesFragment = AvailableVehiclesFragment()
    private var vehiclesMapFragment = VehiclesMapFragment()
    private lateinit var reservation:Reservation

    companion object {
        @JvmStatic
        fun newInstance(reservation: Reservation, latitude:Double, longitude:Double) = SearchResultsSliderFragment().apply {
            arguments = Bundle().apply {
                putParcelable("RESERVATION", reservation)
                putDouble("LATITUDE", latitude)
                putDouble("LONGITUDE", longitude)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_search_results_tabs, container, false)
        presenter= SearchResultsSliderPresenter(this)
        reservation=arguments?.getParcelable("RESERVATION")!!
        val latitude = arguments?.getDouble("LATITUDE")!!
        val longitude= arguments?.getDouble("LONGITUDE")!!
        vehiclesMapFragment.moveCamera(latitude,longitude)


        presenter.loadVehicles(vehicles,reservation,latitude,longitude)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        searchResultsPagerAdapter = SearchResultsAdapter(childFragmentManager)
        viewPager = view.findViewById(R.id.pager)
        viewPager.adapter = searchResultsPagerAdapter
        view.tabLayout.setupWithViewPager(viewPager)
    }


    override fun onVehiclesFetched() {
        availableVehiclesFragment.updateVehicleList(vehicles, reservation)
        vehiclesMapFragment.updateVehicleList(vehicles, reservation)
    }

    inner class SearchResultsAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getCount(): Int  = 2

        override fun getItem(i: Int): Fragment {
            if (i==0)
            {
                return availableVehiclesFragment
            }
            return vehiclesMapFragment
        }

        override fun getPageTitle(position: Int): CharSequence {
            if (position == 0)
                return "LISTA"
            return "MAPPA"
        }
    }

}