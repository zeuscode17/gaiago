package com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider.availableVehicles.selectedVehicle

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.reservation.Reservation
import com.zeuscode.gaiago.ui.main.MainActivity
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import kotlinx.android.synthetic.main.activity_selected_vehicle.*
import kotlinx.android.synthetic.main.rating_bar.*


class SelectedVehicleActivity : AppCompatActivity(),
    SelectedVehicleContract.View {

    private lateinit var presenter: SelectedVehiclePresenter
    private lateinit var vehicle: Vehicle
    private lateinit var reservation: Reservation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selected_vehicle)

        presenter=
            SelectedVehiclePresenter(
                this
            )

        getExtras()

        initView()
    }

    private fun getExtras() {
        vehicle=intent?.extras?.getParcelable("VEHICLE")!!
        reservation=intent?.extras?.getParcelable("RESERVATION")!!
    }

    private fun initView() {

        Picasso.get().load(Uri.parse(vehicle.uri)).into(vehicleImageView)

        val brandModel="${vehicle.brand} ${vehicle.model}"
        upperBrandModelTextView.text=brandModel
        brandModelTextView.text=brandModel
        yearTextView.text=vehicle.year
        vehicleRatingBar.rating=vehicle.rating
        val hourlyCost= "${vehicle.hourlyCost} €/ora"
        hourlyCostTextView.text=hourlyCost
        val totalCost=vehicle.hourlyCost.toDouble()*((reservation.endHour-reservation.startHour).toDouble()/4)
        totalCostTextView.text=String.format("%.2f", totalCost)
        bookVehicleButton.setOnClickListener {

            val date=reservation.date
            val newAvailability=bookVehicle(vehicle.availability[date]!!,reservation.startHour, reservation.endHour)

            reservation.address=vehicle.placeDescription
            reservation.hourlyCost=vehicle.hourlyCost
            reservation.totalCost=totalCost
            reservation.vehicleID=vehicle.vehicleID
            reservation.ownerID=vehicle.owner
            reservation.vehicleUri=vehicle.uri
            reservation.brandModel=brandModel
            vehicle.availability[date]=newAvailability

            bookingConfirmDialog(newAvailability)
        }

    }

    private fun bookVehicle(dayAvailability:String,startHour:Int,endHour:Int) :String{
        val bookingLength=endHour-startHour
        return dayAvailability.replaceRange(startHour,endHour,"1".repeat(bookingLength))
    }

    override fun onReservationInserted() {
        Toast.makeText(this,"PRENOTAZIONE EFFETTUATA", Toast.LENGTH_LONG).show()
        val intent= Intent(this,MainActivity::class.java)
        startActivity(intent)
    }

    private fun bookingConfirmDialog(newAvailability:String) {
        AlertDialog.Builder(this)
            .setTitle("CONFERMA PRENOTAZIONE")
            .setMessage("Vuoi confermare la prenotazione?")
            .setPositiveButton("Sì") { _, _ ->
                presenter.onBookingConfirmed(reservation, vehicle.vehicleID, newAvailability)
            }
            .setNegativeButton("No", null)
            .create()
            .show()
    }
}
