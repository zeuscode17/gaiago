package com.zeuscode.gaiago.ui.license

data class License(
        val number: String="",
        val releaseDate: Long=0,
        val expirationDate: Long=0
)