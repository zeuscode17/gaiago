package com.zeuscode.gaiago.ui.gamification.miniGame

import android.os.Parcel
import android.os.Parcelable

data class MiniGameCar(
    var model: String = "Basic",
    var color: String = "Red",
    var headlights: Int = 0,
    var rims: Int = 0,
    var motor: Int = 0,
    var transmission: Int = 0,
    var suspensions: Int = 0,
    var exhaust: Int = 0

) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString()!!,
        source.readString()!!,
        source.readInt(),
        source.readInt(),
        source.readInt(),
        source.readInt(),
        source.readInt(),
        source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(model)
        writeString(color)
        writeInt(headlights)
        writeInt(rims)
        writeInt(motor)
        writeInt(transmission)
        writeInt(suspensions)
        writeInt(exhaust)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<MiniGameCar> = object : Parcelable.Creator<MiniGameCar> {
            override fun createFromParcel(source: Parcel): MiniGameCar = MiniGameCar(source)
            override fun newArray(size: Int): Array<MiniGameCar?> = arrayOfNulls(size)
        }
    }
}
