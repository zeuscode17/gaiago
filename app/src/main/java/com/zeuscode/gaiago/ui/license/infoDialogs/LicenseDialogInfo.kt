package com.zeuscode.gaiago.ui.license.infoDialogs

import com.zeuscode.gaiago.R
import java.io.Serializable

data class LicenseDialogInfo(val primaryTitle: Int, val secondaryTitle: Int, val image: Int) : Serializable {
    companion object {
        val NUMBER_DIALOG_INFO = LicenseDialogInfo(
            R.string.license_number_info_title_1,
            R.string.license_number_info_title_2,
            R.drawable.ic_license_number_info
        )
        val CATEGORY_DIALOG_INFO = LicenseDialogInfo(
            R.string.license_category_info_title_1,
            R.string.license_category_info_title_2,
            R.drawable.ic_license_category_info
        )
        val VALIDITY_DATE_DIALOG_INFO = LicenseDialogInfo(
            R.string.license_category_info_title_1,
            R.string.license_validity_date_title_info_2,
            R.drawable.ic_license_validity_date_info
        )
        val EXPIRATION_DATE_DIALOG_INFO = LicenseDialogInfo(
            R.string.license_category_info_title_1,
            R.string.license_expiration_date_info_title_2,
            R.drawable.ic_license_expiration_date_info
        )
        val FRONT_SIDE_DIALOG_INFO = LicenseDialogInfo(
            R.string.license_front_side_info_title_1,
            R.string.license_front_side_info_title_2,
            R.drawable.ic_license_front_side
        )
        val BACK_SIDE_DIALOG_INFO = LicenseDialogInfo(
            R.string.license_back_side_info_title_1,
            R.string.license_back_side_info_title_2,
            R.drawable.ic_license_back_side
        )
    }
}

