package com.zeuscode.gaiago.ui.gamification.miniGame.changePieces

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel

class ChangePiecesPresenter(private var view:ChangePiecesContract.View) : ChangePiecesContract.Presenter {

    override fun fetchDailyRewardIndex() {
        val valueEventListener= object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val index=snapshot.value.toString().toInt()
                view.onIndexFetched(index)
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }

        FirebaseModel.getDailyRewardIndex().addListenerForSingleValueEvent(valueEventListener)
    }

}