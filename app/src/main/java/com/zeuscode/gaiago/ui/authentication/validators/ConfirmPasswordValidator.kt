package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.authentication.Credentials
import com.zeuscode.gaiago.ui.authentication.validators.ValidationError.NOT_MATCHING_PASSWORD

class ConfirmPasswordValidator :
    Validator<Credentials> {
    override fun validate(item: Credentials): ValidationError? {
        if (item.password != item.confirmPassword) {
            return NOT_MATCHING_PASSWORD
        }
        return null
    }
}