package com.zeuscode.gaiago.ui.vehicles.addVehicle

import android.content.ContentValues
import android.net.Uri
import android.util.Log
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.UploadTask
import com.seatgeek.placesautocomplete.model.Place
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import java.util.*

class AddVehiclePresenter(private var view: AddVehicleContract.View, private var placesClient: PlacesClient):AddVehicleContract.Presenter {

    override fun onImageChosen(photoUri : Uri) {
        val vehicleID = FirebaseModel.pushVehicleKey()
        val ref= FirebaseModel.vehicleImageUpload(vehicleID)
        val uploadTask=ref.putFile(photoUri)

        uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation ref.downloadUrl
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                view.onImageUploaded(vehicleID,task.result!!)
            } else {
                // TODO Gestire Errore
            }
        }
    }

    override fun uploadVehicle(vehicle: Vehicle) {
        vehicle.owner= FirebaseModel.getUid()!!
        FirebaseModel.insertVehicle(vehicle).addOnCompleteListener {
            if (it.isSuccessful) {
                loadProgressBar()
                view.onVehicleUploaded()
            } else {
                //TODO Gestire errore
            }
        }
    }

    override fun fetchPlace(place: Place) {
        // Specify the fields to return.
        val placeFields = Arrays.asList(
            com.google.android.libraries.places.api.model.Place.Field.ID,
            com.google.android.libraries.places.api.model.Place.Field.LAT_LNG,
            com.google.android.libraries.places.api.model.Place.Field.ADDRESS
        )
        // Construct a request object, passing the place ID and fields array.
        val request = FetchPlaceRequest.builder(place.place_id, placeFields)
            .build()
        // Add a listener to handle the response.
        placesClient.fetchPlace(request).addOnSuccessListener { response ->
            view.onPlaceFetched(response.place)
        }.addOnFailureListener { exception ->
            if (exception is ApiException) {
                Log.e("Place not found: " , exception.message)
            }
        }
    }

    private fun loadProgressBar() {
        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                val progressBar=dataSnapshot.value.toString().toCharArray()
                progressBar[4]='1'
                FirebaseModel.editProgressBar(progressBar.joinToString(""))
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(ContentValues.TAG, "loadPost:onCancelled", databaseError.toException())

            }
        }
        FirebaseModel.fetchProgressBar().addListenerForSingleValueEvent(valueEventListener)
    }
}