package com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider

import com.zeuscode.gaiago.ui.reservation.Reservation
import com.zeuscode.gaiago.ui.vehicles.Vehicle

interface SearchResultsSliderContract {

    interface View {
        fun onVehiclesFetched()
    }

    interface Presenter {
        fun loadVehicles(vehicles:ArrayList<Vehicle>, reservation: Reservation, latitude:Double,
                         longitude:Double)
    }
}