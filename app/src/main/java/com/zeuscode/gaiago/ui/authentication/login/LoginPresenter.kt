package com.zeuscode.gaiago.ui.authentication.login

import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.authentication.Credentials
import com.zeuscode.gaiago.ui.authentication.validators.CompositeValidatorFactory
import com.zeuscode.gaiago.ui.authentication.validators.EmailValidator
import com.zeuscode.gaiago.ui.authentication.validators.Validator


class LoginPresenter(private var view: LoginContract.View) : LoginContract.Presenter {

    private val validator: Validator<Credentials> = CompositeValidatorFactory.authenticationValidator()
    private val emailValidator: Validator<Credentials> = EmailValidator()

    override fun login(credentials: Credentials) {
        validator.validate(credentials)?.let {
            view.onAuthenticationError(it)
            return
        }
        FirebaseModel.authenticateUser(credentials.username, credentials.password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    view.onAuthenticationSuccess()
                } else {
                    view.onAuthenticationFailure()
                }
            }
    }

    override fun sendResetEmail(email: String) {
        val credentials=Credentials(email, "")
        emailValidator.validate(credentials)?.let {
            view.onAuthenticationError(it)
            return
        }
        FirebaseModel.sendResetEmail(email)
        view.onResetSent()
    }

}