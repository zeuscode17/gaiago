package com.zeuscode.gaiago.ui.profile

interface ProfileContract {
    interface View {
        fun onProfileLoaded(profile: Profile, progressBar: CharArray)
        fun showProfile()
        fun updateProgressBar(progress: CharArray)
        fun onAccountDeleted()
    }

    interface Presenter {
        fun loadProfile()
        fun updateAddress(address: String)
        fun updateBirthDate(date: Long)
        fun updateEmail(email:String)
        fun updateNameSurname(name:String, surname:String)
        fun updatePhoneNumber(phoneNumber:String)
        fun deleteAccount()
    }
}