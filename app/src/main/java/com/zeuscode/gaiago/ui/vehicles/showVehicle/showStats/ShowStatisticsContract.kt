package com.zeuscode.gaiago.ui.vehicles.showVehicle.showStats

interface ShowStatisticsContract {
    interface View {
        fun onReservationsFetched(weekDays:Array<Double>)
    }

    interface Presenter {
        fun fetchReservations(vehicleID:String)
    }
}