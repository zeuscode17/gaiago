package com.zeuscode.gaiago.ui.gamification.miniGame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.gamification.miniGame.changeCar.ChangeCarActivity
import com.zeuscode.gaiago.ui.gamification.miniGame.changePieces.ChangePiecesActivity
import com.zeuscode.gaiago.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_mini_game.*

class MiniGameActivity : AppCompatActivity(), MiniGameContract.View {

    private lateinit var presenter: MiniGamePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mini_game)

        presenter=MiniGamePresenter(this)

        presenter.loadMiniGameCar()


    }

    override fun onMiniGameCarFetched(miniGameCar: MiniGameCar) {
        changePiecesButton.setOnClickListener {
            val intent= Intent(this, ChangePiecesActivity::class.java)
            intent.putExtra("MINIGAMECAR", miniGameCar)
            startActivity(intent)
        }
        changeVehicleButton.setOnClickListener {
            val intent= Intent(this, ChangeCarActivity::class.java)
            intent.putExtra("MINIGAMECAR", miniGameCar)
            startActivity(intent)
        }
        if (miniGameCar.model=="Basic")
                setBasicCar(miniGameCar)
        else if (miniGameCar.model=="Chevrolet")
            setChevrolet(miniGameCar)
        else setLotus(miniGameCar)
    }

    private fun setBasicCar(miniGameCar: MiniGameCar) {
        if (miniGameCar.color=="Red") {
            if (miniGameCar.headlights == 0) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.car_red_yellow)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.car_red_1_yellow)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.car_red_2_yellow)
            }
            else if (miniGameCar.headlights == 1) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.car_red_blue)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.car_red_1_blue)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.car_red_2_blue)
            }
        }
        else if (miniGameCar.color=="Silver") {
            if (miniGameCar.headlights == 0) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.car_silver_yellow)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.car_silver_1_yellow)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.car_silver_2_yellow)
            }
            else if (miniGameCar.headlights == 1) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.car_silver_blue)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.car_silver_1_blue)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.car_silver_2_blue)
            }
        }
        else if (miniGameCar.color=="Yellow") {
            if (miniGameCar.headlights == 0) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.car_silver_yellow)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.car_silver_1_yellow)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.car_silver_2_yellow)
            } else if (miniGameCar.headlights == 1) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.car_silver_blue)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.car_silver_1_blue)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.car_silver_2_blue)
            }
        }

        var speedText: Int
        var accelerationText: Double
        var weightText=1300
        var maneuverabilityText=2
        when (miniGameCar.motor) {
            0-> {
                speedText=140
                accelerationText=1.0
            }
            1-> {
                speedText=200
                accelerationText=1.2
            }
            else -> {
                speedText=240
                accelerationText=1.4
            }
        }
        if (miniGameCar.exhaust==1) {
            speedText+=20
            accelerationText+=0.2
        }
        if (miniGameCar.transmission==1) {
            speedText+=10
            accelerationText+=0.2
            weightText-=100
        }
        if (miniGameCar.suspensions==1) {
            maneuverabilityText+=2
        }
        speedTextView.text="$speedText km/h"
        val accelerationTxt= String.format("%.2f", accelerationText)
        accelerationTextView.text= "$accelerationTxt G"
        maneuverabilityTextView.text="$maneuverabilityText"
        weightTextView.text="$weightText kg"

    }

    private fun setChevrolet(miniGameCar: MiniGameCar) {
        if (miniGameCar.color=="Red") {
            if (miniGameCar.headlights == 0) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_red_corvette_yellow)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_red_corvette_1_yellow)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_red_corvette_2_yellow)
            }
            else if (miniGameCar.headlights == 1) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_red_corvette_blue)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_red_corvette_1_blue)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_red_corvette_2_blue)
            }
        }
        else if (miniGameCar.color=="Silver") {
            if (miniGameCar.headlights == 0) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_silver_corvette_yellow)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_silver_corvette_1_yellow)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_silver_corvette_2_yellow)
            }
            else if (miniGameCar.headlights == 1) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_silver_corvette_blue)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_silver_corvette_1_blue)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_silver_corvette_2_blue)
            }
        }
        else if (miniGameCar.color=="Yellow") {
            if (miniGameCar.headlights == 0) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_yellow_corvette_yellow)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_yellow_corvette_1_yellow)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_yellow_corvette_2_yellow)
            } else if (miniGameCar.headlights == 1) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_yellow_corvette_blue)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_yellow_corvette_1_blue)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.chevrolet_yellow_corvette_2_blue)
            }
        }

        var speedText: Int
        var accelerationText: Double
        var weightText=1100
        var maneuverabilityText=4
        when (miniGameCar.motor) {
            0-> {
                speedText=160
                accelerationText=1.5
            }
            1-> {
                speedText=220
                accelerationText=1.7
            }
            else -> {
                speedText=260
                accelerationText=1.9
            }
        }
        if (miniGameCar.exhaust==1) {
            speedText+=20
            accelerationText+=0.2
        }
        if (miniGameCar.transmission==1) {
            speedText+=10
            accelerationText+=0.2
            weightText-=100
        }
        if (miniGameCar.suspensions==1) {
            maneuverabilityText+=2
        }
        speedTextView.text="$speedText km/h"
        val accelerationTxt= String.format("%.2f", accelerationText)
        accelerationTextView.text= "$accelerationTxt G"
        maneuverabilityTextView.text="$maneuverabilityText"
        weightTextView.text="$weightText kg"
    }

    private fun setLotus(miniGameCar: MiniGameCar) {
        if (miniGameCar.color=="Red") {
            if (miniGameCar.headlights == 0) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.lotus_red_yellow)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.lotus_red_1_yellow)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.lotus_red_2_yellow)
            }
            else if (miniGameCar.headlights == 1) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.lotus_red_blue)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.lotus_red_1_blue)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.lotus_red_2_blue)
            }
        }
        else if (miniGameCar.color=="Silver") {
            if (miniGameCar.headlights == 0) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.lotus_silver_yellow)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.lotus_silver_1_yellow)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.lotus_silver_2_yellow)
            }
            else if (miniGameCar.headlights == 1) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.lotus_silver_blue)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.lotus_silver_1_blue)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.lotus_silver_2_blue)
            }
        }
        else if (miniGameCar.color=="Yellow") {
            if (miniGameCar.headlights == 0) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.lotus_yellow_yellow)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.lotus_yellow_1_yellow)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.lotus_yellow_2_yellow)
            } else if (miniGameCar.headlights == 1) {
                if (miniGameCar.rims == 0)
                    vehicleImageView.setImageResource(R.drawable.lotus_yellow_blue)
                else if (miniGameCar.rims == 1)
                    vehicleImageView.setImageResource(R.drawable.lotus_yellow_1_blue)
                else if (miniGameCar.rims == 2)
                    vehicleImageView.setImageResource(R.drawable.lotus_yellow_2_blue)
            }
        }

        var speedText: Int
        var accelerationText: Double
        var weightText=1000
        var maneuverabilityText=6
        when (miniGameCar.motor) {
            0-> {
                speedText=180
                accelerationText=1.7
            }
            1-> {
                speedText=240
                accelerationText=1.9
            }
            else -> {
                speedText=280
                accelerationText=2.1
            }
        }
        if (miniGameCar.exhaust==1) {
            speedText+=20
            accelerationText+=0.2
        }
        if (miniGameCar.transmission==1) {
            speedText+=10
            accelerationText+=0.2
            weightText-=100
        }
        if (miniGameCar.suspensions==1) {
            maneuverabilityText+=2
        }
        speedTextView.text="$speedText km/h"
        val accelerationTxt= String.format("%.2f", accelerationText)
        accelerationTextView.text= "$accelerationTxt G"
        maneuverabilityTextView.text="$maneuverabilityText"
        weightTextView.text="$weightText kg"
    }


    override fun onBackPressed() {
        super.onBackPressed()
        val intent=Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

}
