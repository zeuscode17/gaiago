package com.zeuscode.gaiago.ui.searchVehicles

import android.util.Log
import com.google.android.gms.common.api.ApiException
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.seatgeek.placesautocomplete.model.Place
import java.util.*

class SearchVehiclePresenter(private var view:SearchVehicleContract.View, private var placesClient: PlacesClient) : SearchVehicleContract.Presenter {

    override fun fetchPlace(place: Place) {
        // Specify the fields to return.
        val placeFields = Arrays.asList(
            com.google.android.libraries.places.api.model.Place.Field.ID,
            com.google.android.libraries.places.api.model.Place.Field.LAT_LNG
        )
        // Construct a request object, passing the place ID and fields array.
        val request = FetchPlaceRequest.builder(place.place_id, placeFields)
            .build()
        // Add a listener to handle the response.
        placesClient.fetchPlace(request).addOnSuccessListener { response ->
            view.onPlaceFetched(response.place)
        }.addOnFailureListener { exception ->
            if (exception is ApiException) {
                Log.e("Place not found: " , exception.message)
            }
        }
    }

}