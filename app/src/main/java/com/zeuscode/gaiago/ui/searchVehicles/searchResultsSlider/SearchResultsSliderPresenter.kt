package com.zeuscode.gaiago.ui.searchVehicles.searchResultsSlider

import android.content.ContentValues
import android.location.Location
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.reservation.Reservation
import com.zeuscode.gaiago.ui.vehicles.Vehicle

class SearchResultsSliderPresenter(private var view:SearchResultsSliderContract.View) : SearchResultsSliderContract.Presenter {

    override fun loadVehicles(vehicles:ArrayList<Vehicle>, reservation: Reservation, latitude:Double, longitude:Double) {

        val startHour=reservation.startHour
        val endHour=reservation.endHour
        val bookingDate=reservation.date
        reservation.userID= FirebaseModel.getUid()!!
        val askedLocation= Location("")
        askedLocation.latitude=latitude
        askedLocation.longitude=longitude

        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //In caso di dati cambiati, svuoto l'arraylist
                vehicles.clear()

                for (singleSnapshot in dataSnapshot.children) {
                    //Creo un oggetto per ogni veicolo
                    val dayAvailability= singleSnapshot.child("availability/$bookingDate").value.toString()

                    if (!dayAvailability.substring(startHour,endHour).contains("1")) {
                        val vehicle = singleSnapshot.getValue(Vehicle::class.java)
                        if (vehicle != null) {
                            val vehicleLocation= Location("")
                            vehicleLocation.longitude=vehicle.longitude
                            vehicleLocation.latitude=vehicle.latitude
                            val distanceBetween=askedLocation.distanceTo(vehicleLocation)
                            if (distanceBetween<30000 && vehicle.owner!= FirebaseModel.getUid()) {
                                vehicles.add(vehicle)
                            }
                        }

                    }
                }
                //Avviso l'adapter di un cambio nei dati
                view.onVehiclesFetched()
            }

            //Errore nella richiesta al database
            override fun onCancelled(databaseError: DatabaseError) {

                Log.e(ContentValues.TAG, "onCancelled", databaseError.toException())
            }
        }

        //aggiungo alla reference il listener
        FirebaseModel.fetchAvailableVehicles(bookingDate).addListenerForSingleValueEvent(valueEventListener)

    }


}