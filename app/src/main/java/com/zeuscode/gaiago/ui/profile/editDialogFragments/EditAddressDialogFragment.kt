package com.zeuscode.gaiago.ui.profile.editDialogFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.User
import kotlinx.android.synthetic.main.fragment_edit_address_dialog.*
import kotlinx.android.synthetic.main.fragment_edit_address_dialog.view.*

class EditAddressDialogFragment(private val user: User?) : EditProfileDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_edit_address_dialog, container, false)


        view.addressEditText.setText(user?.address)

        view.okButton.setOnClickListener {
            val newAddress=addressEditText.text.toString()
            user?.address=newAddress
            presenter?.updateAddress(newAddress)
            this.dismiss()
        }

        view.cancelButton.setOnClickListener {
            this.dismiss()
        }

        return view
    }

}
