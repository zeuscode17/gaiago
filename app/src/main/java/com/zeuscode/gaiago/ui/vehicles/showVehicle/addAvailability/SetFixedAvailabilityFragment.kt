package com.zeuscode.gaiago.ui.vehicles.showVehicle.addAvailability

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.pickerDialogs.dialogs.DatePickerDialogFragment
import com.zeuscode.gaiago.extensions.pickerDialogs.dialogs.HourPickerDialogFragment
import com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces.DatePickerListener
import com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces.HourPickerListener
import com.zeuscode.gaiago.extensions.twoDigitsString
import kotlinx.android.synthetic.main.fragment_set_fixed_availability.*
import kotlinx.android.synthetic.main.fragment_set_fixed_availability.view.*
import java.util.*

class SetFixedAvailabilityFragment : Fragment() {

    var bookingDate: String? = null
    var bookingStartHour: Int? = null
    var bookingEndHour: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        //Execution of the fragment inflation inside the fragment container
        val view = inflater.inflate(R.layout.fragment_set_fixed_availability, container, false)

        view.bookingDateEditText.setOnClickListener {
            val fragment = DatePickerDialogFragment()
            fragment.attach(validityDatePickerListener)
            fragment.show(fragmentManager, "dialog")
        }

        view.bookingStartHourEditText.setOnClickListener {
            val fragment = HourPickerDialogFragment()
            fragment.attach(startHourPickerListener)
            fragment.show(fragmentManager, "dialog")
        }

        view.bookingEndHourEditText.setOnClickListener {
            val fragment = HourPickerDialogFragment()
            fragment.attach(endHourPickerListener)
            fragment.show(fragmentManager, "dialog")
        }

        return view
    }

    private val validityDatePickerListener = object :
        DatePickerListener {
        override fun onDatePicked(date: Long) {
            bookingDateEditText.setText(displayedDateFrom(date), TextView.BufferType.EDITABLE)
        }
    }

    private fun displayedDateFrom(date: Long): String {

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = date

        val dayOfMonth = calendar[Calendar.DAY_OF_MONTH].twoDigitsString()
        val month = (calendar[Calendar.MONTH] + 1).twoDigitsString()
        val year = calendar[Calendar.YEAR]

        bookingDate="$dayOfMonth$month$year"

        return "$dayOfMonth/$month/$year"
    }

    private val startHourPickerListener = object:
        HourPickerListener {
        override fun onHourPicked(hourPicked:String,interval:Int) {
            bookingStartHourEditText.setText(hourPicked, TextView.BufferType.EDITABLE)
            bookingStartHour=interval
        }
    }

    private val endHourPickerListener = object:
        HourPickerListener {
        override fun onHourPicked(hourPicked:String,interval:Int) {
            bookingEndHourEditText.setText(hourPicked, TextView.BufferType.EDITABLE)
            if (interval==0) bookingEndHour=96
            else bookingEndHour=interval
        }
    }


}