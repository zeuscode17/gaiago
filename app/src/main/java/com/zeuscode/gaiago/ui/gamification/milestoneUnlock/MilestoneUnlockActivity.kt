package com.zeuscode.gaiago.ui.gamification.milestoneUnlock

import android.content.Context
import com.zeuscode.gaiago.R
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_milestone_unlock.*
import kotlinx.android.synthetic.main.milestone_grid_item.view.*
import kotlinx.android.synthetic.main.milestone_popup.view.*
import kotlinx.android.synthetic.main.milestone_popup.view.prizeImageView
import java.util.ArrayList


class MilestoneUnlockActivity : AppCompatActivity(), MilestoneUnlockContract.View {

    private lateinit var presenter:MilestoneUnlockPresenter


    private var choosenPrizes = ArrayList<MilestoneItem>() //Milestone items
    private val prizes = ArrayList<Prize>()
    private lateinit var myAdapter: MilestoneAdapter

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_milestone_unlock)

        presenter= MilestoneUnlockPresenter(this)

        initGridView()

        presenter.loadPrizes(prizes) //carica i premi desiderati per la milestone nell'array prizes
        presenter.loadMilestoneList(choosenPrizes) //stabilisco premo,livello richiesto e stato del premio per ogni premio in prizes

    }// Fine onCreate

    private fun initGridView() {
        myAdapter = MilestoneAdapter(
            this,
            choosenPrizes,
            prizes
        )
        milestone_gridview.adapter = myAdapter

        milestone_gridview.setOnItemClickListener { _, view, position, _ ->
            retrievePrize(position,view)
        }
    }

    override fun notifyAdapter() {
        myAdapter.notifyDataSetChanged()
    }

    private fun retrievePrize(position: Int, view: View){

        when (choosenPrizes[position].status){
            'U' -> Toast.makeText(this, "Premio non disponibile!", Toast.LENGTH_SHORT).show()
            'R' -> Toast.makeText(this, "Premio già ritirato!", Toast.LENGTH_SHORT).show()
            'A' -> showPopUp(position, view)
        }
    }

    private fun showPopUp(position: Int, view: View){

        val window = PopupWindow(this)
        // Initialize a new layout inflater instance
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        // Inflate a custom view using layout inflater
        val popview = inflater.inflate(R.layout.milestone_popup,null)

        setPopUp(popview, position)
        window.contentView=popview
        window.showAtLocation(view, Gravity.CENTER,0,0)

        popview.prizeCloseButton.setOnClickListener {
            presenter.assignPrize(position) //consegna il premio -> aggiorna stato premio in Db e choosenPrizes
            view.itemStatusImageView.setImageResource(R.drawable.tick) //segnala che il premio è stato ritirato
            window.dismiss()
        }
    }

    override fun onPrizeAssigned(position: Int) {
        Toast.makeText(applicationContext, "Premio consegnato!", Toast.LENGTH_SHORT).show()
        choosenPrizes[position].status='R' //aggiorno lo stato in choosenPrizes
    }

    private fun setPopUp(view: View, position: Int){
        view.prizeImageView.setImageResource(prizes[position].image)
        view.prizeName.text=prizes[position].descr
    }


}