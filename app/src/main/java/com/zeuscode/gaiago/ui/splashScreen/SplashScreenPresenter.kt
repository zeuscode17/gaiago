package com.zeuscode.gaiago.ui.splashScreen

import com.zeuscode.gaiago.model.FirebaseModel

class SplashScreenPresenter(private var view: SplashScreenContract.View):SplashScreenContract.Presenter {

    override fun checkUserAuthentication() {
        if (FirebaseModel.userIsLogged())
            view.showMainView()
        else
            view.showLoginView()
    }
}