package com.zeuscode.gaiago.ui.license.number

import com.zeuscode.gaiago.ui.license.LicenseRegistrationListener

interface LicenseNumberContract {

    interface View {
        fun showEmptyLicenseNumberDialog()
        fun showUncheckedLicenseCategoryHeldDialog()
    }

    interface Presenter {
        fun onLicenseNumberSubmitted(number: String, categoryHeld: Boolean, listener: LicenseRegistrationListener)
    }

}