package com.zeuscode.gaiago.ui.gamification.miniGame.changePieces

interface ChangePiecesContract {
    interface View {
        fun onIndexFetched(index:Int)
    }

    interface Presenter {
        fun fetchDailyRewardIndex()
    }
}