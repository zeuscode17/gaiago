package com.zeuscode.gaiago.ui.vehicles.vehicleList

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.RecyclerItemClickListener
import kotlinx.android.synthetic.main.fragment_vehicles_list.*
import com.zeuscode.gaiago.ui.vehicles.addVehicle.AddVehicleActivity
import com.zeuscode.gaiago.ui.vehicles.showVehicle.ShowVehicleActivity
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import kotlinx.android.synthetic.main.fragment_vehicles_list.view.*

class VehiclesListFragment : Fragment(), VehicleListContract.View {

    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val vehicles : ArrayList<Vehicle> = ArrayList()
    private lateinit var presenter:VehicleListContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_vehicles_list, container, false)

        view.fab.setOnClickListener {
            val intent = Intent(context, AddVehicleActivity::class.java)
            startActivity(intent)
        }

        presenter=VehicleListPresenter(this)

        return view
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initRecycler()
        initRecyclerListener()
    }

    override fun onStart() {
        super.onStart()
        presenter.loadVehicles(vehicles)

    }

    //Funzione per caricare la recycler
    private fun initRecycler() {

        viewManager = LinearLayoutManager(requireContext())
        viewAdapter = VehiclesRecyclerViewAdapter(vehicles)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    private fun initRecyclerListener() {

        recyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                requireContext(),
                recyclerView,
                object : RecyclerItemClickListener.OnItemClickListener {
                    //Click rapido
                    override fun onItemClick(view: View, position: Int) {
                        showVehicle(vehicles[position])
                    }
                    //Click Lungo
                    override fun onLongItemClick(view: View?, position: Int) {
                    }
                })
        )
    }

    override fun onVehiclesFetched() {
        if (this.isVisible) {
            if (vehicles.isNotEmpty())
                loadVehicleTextView.visibility = View.GONE
            else loadVehicleTextView.visibility = View.VISIBLE

            viewAdapter.notifyDataSetChanged()
        }
    }

    private fun showVehicle(vehicle:Vehicle) {
        val intent = Intent(context, ShowVehicleActivity::class.java)
        intent.putExtra("VEHICLE", vehicle)
        startActivity(intent)
    }

}



