package com.zeuscode.gaiago.ui.introslider

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {

    private lateinit var introViewPagerAdapter: IntroScreenViewPagerAdapter
    private lateinit var introBullets: Array<TextView>
    private lateinit var introSliderLayouts: IntArray


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        //Get the intro slides
        introSliderLayouts = intArrayOf(
            R.layout.intro_screen_1,
            R.layout.intro_screen_2,
            R.layout.intro_screen_3,
            R.layout.intro_screen_4)

        // adding bottom introBullets
        makeIIntroBullets(0)

        introViewPagerAdapter = IntroScreenViewPagerAdapter()

        introViewPager.adapter = introViewPagerAdapter
        introViewPager.addOnPageChangeListener(introViewPagerListener)

        btnSkip.setOnClickListener { applicationStartup() }

        btnNext.setOnClickListener {
            // checking for last page
            // if last page home screen will be launched
            val current = getItem(+1)
            if (current < introSliderLayouts.size) {
                // move to next screen
                introViewPager.currentItem = current
            } else {
                applicationStartup()
            }
        }
        // making notification bar transparent
        setTransperantStatusBar()
    }


    private fun makeIIntroBullets(currentPage: Int) {

        val arraySize = introSliderLayouts.size
        introBullets = Array(arraySize) { textboxInit() }

        val colorsActive = resources.getIntArray(R.array.array_intro_bullet_active)
        val colorsInactive = resources.getIntArray(R.array.array_intro_bullet_inactive)

        introBulletsLayout.removeAllViews()

        for (i in 0 until introBullets.size) {
            introBullets[i] = TextView(this)
            introBullets[i].text = HtmlCompat.fromHtml("&#9679;",0)
            introBullets[i].textSize = 30F
            introBullets[i].setTextColor(colorsInactive[currentPage])
            introBulletsLayout.addView(introBullets[i])
        }

        if (introBullets.isNotEmpty())
            introBullets[currentPage].setTextColor(colorsActive[currentPage])
    }

    private fun textboxInit(): TextView {
        return TextView(applicationContext)
    }

    private fun getItem(i: Int): Int {
        return introViewPager!!.currentItem + i
    }

    private fun applicationStartup() {
        startActivity(Intent(this@WelcomeActivity, MainActivity::class.java))
        finish()
    }

    private var introViewPagerListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {

        override fun onPageSelected(position: Int) {

            makeIIntroBullets(position)
            /*Based on the page position change the button text*/
            if (position == introSliderLayouts.size - 1) {
                btnNext.text = getString(R.string.done_button_title)
                btnSkip.visibility = View.GONE
            } else {
                btnNext.text = getString(R.string.next_button_title)
                btnSkip.visibility = View.VISIBLE
            }

        }
        override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {
            //Do nothing for now
        }
        override fun onPageScrollStateChanged(arg0: Int) {
            //Do nothing for now
        }
    }

    private fun setTransperantStatusBar() {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT
    }

    inner class IntroScreenViewPagerAdapter : PagerAdapter() {

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val layoutInflater: LayoutInflater = LayoutInflater.from(applicationContext)
            val view = layoutInflater.inflate(introSliderLayouts[position], container, false)
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return introSliderLayouts.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }

        override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
            val view = obj as View
            container.removeView(view)
        }

    }
}
