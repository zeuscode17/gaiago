package com.zeuscode.gaiago.ui.gamification.milestoneUnlock

import android.content.ContentValues
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.zeuscode.gaiago.model.FirebaseModel
import com.zeuscode.gaiago.ui.User
import java.util.ArrayList

class MilestoneUnlockPresenter(private var view: MilestoneUnlockContract.View) : MilestoneUnlockContract.Presenter {

    private val lvls = intArrayOf(5, 10, 15, 20, 25, 30, 35, 40, 45,50)

    override fun loadPrizes(prizes: ArrayList<Prize>) {
        prizes.add(gift10) //1
        prizes.add(gift25)
        prizes.add(gift50)
        prizes.add(freeJourney)
        prizes.add(amazonGift10)
        prizes.add(discount10)
        prizes.add(discount25)
        prizes.add(discount50)
        prizes.add(discountZ10)
        prizes.add(discountC10) //10


        view.notifyAdapter()
    }

    override fun assignPrize(position: Int) {
        //Carico lo stato dei premi milestone dell'utente in asincrono dal database
        val postListenerUser = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                var list=dataSnapshot.value.toString()
                list=list.replaceRange(position..position, "R")
                FirebaseModel.setMilestoneStat(list) //segnalo il ritiro del premio al DB

                view.onPrizeAssigned(position)
            }
            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(ContentValues.TAG, "loadPost:onCancelled", databaseError.toException())
            }
        }
        //aggiungo listener alla reference
        FirebaseModel.fetchMilestoneStat().addListenerForSingleValueEvent(postListenerUser)
    }

    override fun loadMilestoneList(choosenPrizes: ArrayList<MilestoneItem>) {


        //Carico lo stato dei premi milestone dell'utente in asincrono dal database
        val postListenerUser = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the U
                choosenPrizes.clear()

                val user = dataSnapshot.getValue(User::class.java)
                user?.let {
                    var list=it.milestoneStat
                    val lv=it.score
                    for (i in (0 until lvls.size)) {

                        if (lv/10>=lvls[i] && list[i]=='U') { //se l'utente ha raggiunto un nuovo livello sblocco il premio

                            list=list.replaceRange(i..i,"A")

                            FirebaseModel.setMilestoneStat(list) //aggiorno stato in DB

                            choosenPrizes.add(MilestoneItem(i,lvls[i], list[i]))
                        }else{
                            choosenPrizes.add(MilestoneItem(i,lvls[i], list[i]))
                        }
                        // Log.d("TAG1", "LISTAMILE ${it.milestoneStat}")
                    }
                }
                view.notifyAdapter()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(ContentValues.TAG, "loadPost:onCancelled", databaseError.toException())
            }
        }
        //aggiungo listener alla reference
        FirebaseModel.fetchUser().addListenerForSingleValueEvent(postListenerUser)
    }

}