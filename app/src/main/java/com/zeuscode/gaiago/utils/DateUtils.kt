package com.zeuscode.gaiago.utils

import java.text.DateFormat.getDateInstance
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateUtils {

    companion object {
/*
        private val format = getDateInstance().apply {
            timeZone = TimeZone.getTimeZone("Europe/Rome")
        }
*/
        private val format=SimpleDateFormat("dd/MM/yyyy", Locale.ITALY)

        fun toLong(stringDate: String): Long {
            try {
                val d = format.parse(stringDate)
                return d.time
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return 0
        }

        fun toStringDate(mills: Long): String {
            val date = Date(mills)
            return format.format(date)
        }
    }
}