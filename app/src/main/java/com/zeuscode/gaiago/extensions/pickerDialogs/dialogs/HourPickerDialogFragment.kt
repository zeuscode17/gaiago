package com.zeuscode.gaiago.extensions.pickerDialogs.dialogs

import android.content.res.Resources.getSystem
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces.HourPickerListener
import kotlinx.android.synthetic.main.fragment_date_picker_dialog.view.closeButton
import kotlinx.android.synthetic.main.fragment_hour_picker_dialog.*
import kotlinx.android.synthetic.main.fragment_hour_picker_dialog.view.*
import android.widget.NumberPicker
import android.widget.TimePicker
import com.zeuscode.gaiago.extensions.twoDigitsString
import java.text.DecimalFormat


class HourPickerDialogFragment : DialogFragment() {

    private var listener: HourPickerListener? = null
    private val INTERVAL = 15
    private val FORMATTER = DecimalFormat("00")
    private var picker: TimePicker? = null // set in onCreate
    private var minutePicker: NumberPicker? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

        val view = inflater.inflate(R.layout.fragment_hour_picker_dialog, container, false)

        view.hourPicker.setIs24HourView(true)

        picker=view.hourPicker

        setMinutePicker()

        view.closeButton.setOnClickListener {
            val hour = hourPicker.hour
            val minute = getMinute().twoDigitsString()
            val interval= hour*4+minutePicker!!.value
            listener?.onHourPicked("$hour:$minute",interval)

            this.dismiss()
        }

        return view
    }

    fun attach(listener: HourPickerListener) {
        this.listener = listener
    }

    private fun setMinutePicker() {
        val numValues = 60 / INTERVAL
        val displayedValues = arrayOfNulls<String>(numValues)
        for (i in 0 until numValues) {
            displayedValues[i] = FORMATTER.format(i * INTERVAL)
        }

        val minute = picker?.findViewById<NumberPicker>(getSystem().getIdentifier("minute", "id", "android"))
        if (minute != null) {
            minutePicker = minute
            minutePicker!!.minValue = 0
            minutePicker!!.maxValue = numValues - 1
            minutePicker!!.displayedValues = displayedValues
        }
    }

    private fun getMinute(): Int {
        return if (minutePicker != null) {
            minutePicker!!.value * INTERVAL
        } else {
            picker!!.minute
        }
    }


}