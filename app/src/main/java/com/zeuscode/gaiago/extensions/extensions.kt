package com.zeuscode.gaiago.extensions

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.Matrix
import android.view.View
import android.view.ViewTreeObserver
import androidx.fragment.app.Fragment
import java.math.BigDecimal

fun Fragment.showDialog(title: String, message: String, positiveButtonText: String, positiveButtonAction: (DialogInterface, Int) -> Unit, negativeButtonText: String, negativeButtonAction: (DialogInterface, Int) -> Unit) {
    AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveButtonText, positiveButtonAction)
            .setNegativeButton(negativeButtonText, negativeButtonAction)
            .setCancelable(false)
            .create().show()
}

fun Fragment.showDialog(title: String, message: Int, positiveButtonText: Int, positiveButtonAction: Nothing?) {
    AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveButtonText, positiveButtonAction)
            .setCancelable(false)
            .create().show()
}


fun Activity.setKeyboardVisibilityListener(keyboardVisibilityListener: KeyboardVisibilityListener) {

    val contentView = findViewById<View>(android.R.id.content)

    var mAppHeight = 0

    contentView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {

        private var mPreviousHeight: Int = 0

        override fun onGlobalLayout() {
            val newHeight = contentView.height

            if (newHeight == mPreviousHeight)
                return

            mPreviousHeight = newHeight

            if (newHeight >= mAppHeight) {
                mAppHeight = newHeight
            }

            if (newHeight != 0) {
                if (mAppHeight > newHeight) {
                    keyboardVisibilityListener.onKeyboardVisibilityChanged(true)
                } else {
                    keyboardVisibilityListener.onKeyboardVisibilityChanged(false)
                }
            }

        }

    })

}

val Activity.displayDensity: Float
    get() = this.resources.displayMetrics.density

interface KeyboardVisibilityListener {

    fun onKeyboardVisibilityChanged(keyboardVisible: Boolean)

}

fun BigDecimal.stringWithComma() = this.toString().replace(".", ",")

fun Int.twoDigitsString() = String.format("%02d", this)

fun Bitmap.rotate(angle: Float): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(angle)
    return Bitmap.createBitmap(this, 0, 0, this.width, this.height,
            matrix, true)
}