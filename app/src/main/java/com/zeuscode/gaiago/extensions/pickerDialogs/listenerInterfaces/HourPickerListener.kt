package com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces

interface HourPickerListener {
    fun onHourPicked(hourPicked:String,interval:Int)
}