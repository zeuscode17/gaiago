package com.zeuscode.gaiago.extensions.pickerDialogs.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.zeuscode.gaiago.R
import kotlinx.android.synthetic.main.fragment_date_picker_dialog.view.closeButton
import android.widget.ArrayAdapter
import com.zeuscode.gaiago.model.BrandModel
import com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces.BrandPickerListener
import kotlinx.android.synthetic.main.fragment_brand_search_dialog.*
import kotlinx.android.synthetic.main.fragment_brand_search_dialog.view.*


class BrandPickerDialogFragment : DialogFragment() {

    private var listener: BrandPickerListener? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

        val view = inflater.inflate(R.layout.fragment_brand_search_dialog, container, false)


        val adapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_dropdown_item_1line, BrandModel.brands)

        view.brandAutoCompleteTextView.setAdapter<ArrayAdapter<String>>(adapter)

        view.closeButton.setOnClickListener {
            val brand = brandAutoCompleteTextView.text
            listener?.onBrandPicked(brand.toString())
            this.dismiss()
        }

        return view
    }

    fun attach(listener: BrandPickerListener) {
        this.listener = listener
    }

}