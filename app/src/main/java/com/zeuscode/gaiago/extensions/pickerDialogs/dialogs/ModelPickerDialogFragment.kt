package com.zeuscode.gaiago.extensions.pickerDialogs.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import com.zeuscode.gaiago.model.BrandModel
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces.ModelPickerListener
import kotlinx.android.synthetic.main.fragment_model_search_dialog.*
import kotlinx.android.synthetic.main.fragment_model_search_dialog.view.*

class ModelPickerDialogFragment(private val brand:String) : DialogFragment() {

    private var listener: ModelPickerListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

        val view = inflater.inflate(R.layout.fragment_model_search_dialog, container, false)

        if (BrandModel.brandMap.containsKey(brand)) {
            val adapter = ArrayAdapter<String>(
                requireContext(),
                android.R.layout.simple_dropdown_item_1line,
                BrandModel.brandMap[brand]!!
            )

            view.modelAutoCompleteTextView.setAdapter<ArrayAdapter<String>>(adapter)

        }
        view.closeButton.setOnClickListener {
            val model = modelAutoCompleteTextView.text
            listener?.onModelPicked(model.toString())
            this.dismiss()
        }

        return view
    }

    fun attach(listener: ModelPickerListener) {
        this.listener = listener
    }

}