package com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces

interface DatePickerListener {
    fun onDatePicked(date: Long)
}
