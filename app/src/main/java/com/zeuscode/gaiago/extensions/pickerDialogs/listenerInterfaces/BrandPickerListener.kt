package com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces

interface BrandPickerListener {
    fun onBrandPicked(brand: String)
}