package com.zeuscode.gaiago.extensions.pickerDialogs.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.zeuscode.gaiago.R
import com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces.DatePickerListener
import kotlinx.android.synthetic.main.fragment_date_picker_dialog.*
import kotlinx.android.synthetic.main.fragment_date_picker_dialog.view.*
import java.util.*

class DatePickerDialogFragment : DialogFragment() {

    private var listener: DatePickerListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

        val view = inflater.inflate(R.layout.fragment_date_picker_dialog, container, false)

        view.datePicker.minDate=(System.currentTimeMillis() - 1000)

        view.closeButton.setOnClickListener {
            val calendar = calendarFromDatePicker()

            listener?.onDatePicked(calendar.timeInMillis)

            this.dismiss()
        }

        return view
    }

    fun attach(listener: DatePickerListener) {
        this.listener = listener
    }

    private fun calendarFromDatePicker(): Calendar {
        val dayOfMonth = datePicker.dayOfMonth
        val month = datePicker.month
        val year = datePicker.year

        val calendar = Calendar.getInstance()

        calendar.set(year, month, dayOfMonth)

        return calendar
    }
}
