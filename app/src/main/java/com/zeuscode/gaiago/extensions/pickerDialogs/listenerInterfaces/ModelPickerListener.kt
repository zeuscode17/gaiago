package com.zeuscode.gaiago.extensions.pickerDialogs.listenerInterfaces

interface ModelPickerListener {
    fun onModelPicked(model: String)
}