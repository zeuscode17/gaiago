package com.zeuscode.gaiago.model

object BrandModel {
    val brands = arrayOf("ABARTH", "ALFA ROMEO", "ASTON MARTIN", "AUDI","BMW","CHEVROLET","CHRYSLER",
        "CITROEN","CORVETTE","DACIA", "FERRARI", "FIAT","FORD", "HYUNDAI", "JAGUAR", "JEEP","KIA", "LAMBORGHINI","LANCIA",
        "LAND ROVER", "MASERATI", "MAZDA", "MERCEDES BENZ", "MITSUBISHI", "NISSAN", "OPEL", "PEUGEOT", "PORSCHE", "RENAULT",
        "SEAT", "SKODA", "SMART", "SUBARU", "SUZUKI", "TESLA", "TOYOTA", "VOLKSWAGEN", "VOLVO")

    val abarth = arrayOf("124 Spider", "500", "595", "695", "Grande Punto", "Punto Evo")
    val alfaromeo = arrayOf("145","146","147","155","156","159","164","166","33","4C","75","Alfa 6","Alfa 90","Alfasud",
        "Alfetta","Alfetta GT/GTV","Brera","Giulia","Giulietta","GT","Gtv/Spider","MiTo","Spider","Sprint","Stelvio",
        "SZ/RZ","8c","Gtv","Sz")
    val astonmartin = arrayOf("DB","DB11","V12","V8 Vintage","Vanquish","Vantage","Virage","V8","Vantage")
    val audi = arrayOf("100", "200","5000","V8","80","90","4000","Cabrio","A1","A2","A3","A4","A5","A3 Sportback e-tron",
        "A4 allroad","A5 Coupé","A6","A6 allroad","A7","A8","Coupé/Quattro","e-tron","Q2","Q3","Q5","Q7","Q8","R8","TT")
    val bmw = arrayOf("i3","i8","Serie 1","Serie 2","Serie 2 Active Tourer","Serie 2 Gran Tourer","Serie 3","Serie 3 GT",
        "Serie 4","Serie 4 GC","Serie 5","Serie 6","Serie 6 GT","Serie 7","Serie 8","X1","X2","X3","X4","X5","X6","X7",
        "Z3","Z4","Z8","2002")
    val chevrolet = arrayOf("Aveo","Camaro","Captiva","Corvette","Cruze","Kalos","Lacetti","Matiz","Nubira","Orlando","Spark",
        "Tacuma","Tahoe","Trax","Astro","Caprice","Chevy Van","El Camino","Express","G","Impala","K30","Silverado","Ssr",
        "Suburban", "Balzer")
    val chrysler = arrayOf("300 C","Crossfire","Cruiser","Serbing","Stratus","Viper","Voy./G.Voyager","Le Baron","Grand Voyager",
        "Vayager")
    val citroen = arrayOf("2CV","Berling","Bx","C-Crosser","C-Elyseé","C-Zero","C1","C2","C3","C3 Aircross","C3 Picasso",
        "C4","C4 Aircross","C4 Cactus","C4 Picasso","C4 SpaceTourer","C5","C5 Aircross","C6","C8","CX","E-Mehari",
        "Grand C4 Picasso","Grand C4 SpaceTourer","GS/GSA","Jumper","Jumpy","Mehari","Nemo","Saxo","SpaceTourer","Visa",
        "XM","Xsara","ZX")
    val corvette = arrayOf("Corvette C6","C1","C3","C4","C5","C7","Zr 1")
    val dacia = arrayOf("Dokker","Duster","Lodgy","Logan","Sandero")
    val ferrari = arrayOf("208","308","328","GTO","348","360","456","458","488","512 BB","550","575M","599","612",
        "812 Superfast","California","Dino 208 GT/4","F12","F355","F430","FF","GTC4 LUSSO","LaFerrari","Mondial",
        "Portofino","Testarossa/512 TR","246","250","330","365","400","Superamerica","512")
    val fiat = arrayOf("124 spider","126","127","131","500","500L","500X","600","900","Argenta","Barchetta","Bravo",
        "Brava","Campagnola","Cinquecento","Coupé","Croma","Doblò","Ducato","Fiorino","Freemont","Fullback","Grande Punto",
        "Idea","Marea","Multipla","Palio","Panda","Punto","Punto Evo","QUBO","Regata","Ritmo","Scudo","Sedici","Seicento",
        "Stilo","Strada","Talento","Tempra","Tipo","Ulysse","Uno","X1/9","130","Dino","Topolino","1500","Balilla")
    val ford = arrayOf("B-Max","C-Max","C-Max Business","Cougar","Courier","EcoSport","Edge","Escort","Fiesta","Focus",
        "Focus Business","Focus C-Max","Fusion","Galaxy","Galaxy Business","Ka","Ka Business","Ka+","Kuga","Mondeo",
        "Mondeo Business","Mustang","Puma","Ranger","S-Max","S-Max Business","Sierra","Tourneo Connect","Tourneo Courier",
        "Tourneo Custom","Transit","Transit Connect","Transit Courier","Transit Custom","Bus","Aerostar","Capri","Crown",
        "Econoline","Explorer","F 150","F 250","F 350","Granada","Grand C-Max","Gt","Maverick","Mercury","Thunderbird","Taunus")
    val hyundai = arrayOf("Accent","Atos","Coupé","Galloper","Genesis","Getz","H-1","H350","i10","i20","i30","i40","Ioniq",
        "ix20","ix35","Kona","Matrix","Santa Fe","Terracan","Trajet","Tucson")
    val jaguard = arrayOf("E-Pace","F-Pace","F-Type","I-Pace","S-Type","X-Type","XE","XF","XJ","XK","E-Type","Mk li")
    val jeep = arrayOf("Cherokee","Commander","Compass","Compass 2° Serie","Grand Cherokee","Patriot","Renegade",
        "Wrangler","Cj","Wagoneer","Willys")
    val kia = arrayOf("Carens","Carnival","cee'd","Cerato","Niro","Optima","Picanto","ProCreed","Rio","Sorento","Soul",
        "Sportage","Stinger","Stonic","Venga")
    val lamborghini = arrayOf("Aventador","Countach","Diablo","Gallardo","Huracan","Murcielago","Urus","Urraco","Lm","Miura")
    val lancia = arrayOf("Beta Montecarlo","Beta Coupé Spider HPE","Dedra","Delta","Delta S4","Flavia","Gamma","K",
        "Lybra","Musa","Phedra","Prisma","Thema","Thesis","Voyager","Y","Ypsilon","Z","Flaminia","Fulvia","Beta")
    val landrover = arrayOf("Defender","Discovery","Discovery Sport","Freelander","Range Rover","Range Rover Evoque",
        "Range Rover Sport","Range Rover Velar","Serie I","Serie li")
    val maserati = arrayOf("Coupé","Ghibli","GranCabrio","Gransport","GranTurismo","Levante","Quattroporte","Spyder",
        "4200","Indy","Merak")
    val mazda = arrayOf("B-2500","BT-50","CX-3","CX-5","CX-7","Mazda2","Mazda3","Mazda5","Mazda6","MX-5","Premacy",
        "RX-8", "E Series")
    val mercedes = arrayOf("190","Citan","Classe A","Classe B","Classe C","Classe CL","Classe CLA","ClasseCLC",
        "Classe CLK","Classe CLS","Classe E","Classe G","Classe GL","Classe GLA","Classe GLC","Classe GLE","Classe GLK",
        "Classe GLS","Classe M","Classe R","Classe S","Classe SL","Classe SLC","Classe SLK","Classe V","Classe X","GT",
        "GT Coupé 4","Marco Polo","Serie 200-280","Serie 200-320","Serie E","Serie S","Serie SEC","Serie SL","SLR","SLS",
        "Sprinter","Vaneo","Viano","Vito","300","350","380","400","450","500","560","600","200","220","230","240","250","280")
    val mistsubishi = arrayOf("3000 GT","ASX","Colt","Eclipse","Eclipse Cross","Grandis","i-MiEV","L200","Lancer",
        "Outlander","Pajero","Pajero Pinin","Pajero Sport","Space Star")
    val nissan = arrayOf("200 SX","Almera","Almera Tino","Alteon","Cabstar","Cube","Evalia","GT-R","Juke","Kubistar",
        "Leaf","Maxima QX","Micra","Murano","Navara","Note","NV200","NV300","NV400","Pathfinder","Patrol","Patrol GR",
        "Safari","Pick-up","Pixo","Primastar","Primera","Pulsar","Qashqai","Serena","Terrano","Terrano II","Trade",
        "Vanette","X-Trail","Z","King Cab")
    val opel = arrayOf("Adam","Agila","Antara","Astra","Calibra","Cascada","Combo","Combo Tour","Corsa","Crossland X",
        "Frontera","Grandland X","GT","Insignia","Kadett E","Karl","Meriva","Mokka","Movano","Omega","Signum","Tigra",
        "Vectra","Vivaro","Zafira","Rekord")
    val peugeot = arrayOf("1007","106","107","108","2008","205","206","207","208","3008","306","307","308","4007","405",
        "406","407","5008","508","607","807","Bipper","Boxer","Expert","iOn","Partner","Ranch","RCZ","Rifter","Traveller","404")
    val porsche = arrayOf("718","901","911","912","911 Carrera","924","944","928","968","Boxster","Cayenne","Cayman",
        "Macan","Panamera","356","914","964","991","993","996","997","911 Turbo S")
    val renault = arrayOf("19","4","5","Alaskan","Avantime","Captur","Clio","Escape","Express","Grand Scenic","Kadjar",
        "Kangoo","Koleos","Laguna","Master","Modus","Megane","Scenic","Talisman","Trafic","Twingo","Wind","ZOE","Twizy")
    val seat = arrayOf("Alhmbra","Altea","Arona","Arosa","Ateca","Cordoba","Exeo","Ibiza","Inca","Leon","Mii","Tarraco","Toledo")
    val skoda = arrayOf("Citigo","Fabia","Karoq","Kodiaq","Octavia","4&4","Pick-up","Rapid","Rapid Spaceback","Roomster",
        "Superb","Yeti")
    val smart = arrayOf("City-coupé","cabrio","Forfour","Fortwo","Roadster","Crossblade")
    val subaru = arrayOf("BRZ","Forester","Impreza","Justy","Legacy","Levorg","Outblack","Trezia","Vivio","WRX","XV","Libero")
    val suzuki = arrayOf("Alto","Baleno","Celerio","Grand Vitara","Ignis","Jimmy","Liana","S-Cross","Samurai","SJ400",
        "Samurai","Splash","Swift","SX4","Vitara","Sidekick","Wagon R+","Sj Samurai")
    val tesla = arrayOf("Model 3","Model S","Model X")
    val toyota = arrayOf("4 Runner","Hilux","Auris","Avensis","Aygo","C-HR","Celica","Corolla","GT86","iQ","Land Cruiser",
        "MR2","Prius","Proace","RAV4","Urban Cruiser","Verso","Verso-S","Yaris","Fj","Fj Cruiser")
    val volkswagen = arrayOf("Amarok","Arteon","Bora","Caddy","Calidfornia","Caravelle","CC","Corrado","Crafter","Eos",
        "Fox","Golf","Jetta","LT","Lupo","Maggiolino","Multivan","New Beetle","Passat","Phaeton","Polo","Scirocco",
        "Sharan","T-Roc","Tiguan","Tiguan Alispace","Touareg","Touran","Transporter","up!","LKSWAGEN 181","LSKWAGEN Buggy",
        "LSKWAGEN T1","LSKWAGEN T2","LSKWAGEN T3","LSKWAGEN T3 Caravelle","LSKWAGEN T3 Multivan","LSKWAGEN T4 California",
        "LSKWAGEN T5","LSKWAGEN T5 California","LSKWAGEN T5 Caravelle","LSKWAGEN T5 Multivan")
    val volvo = arrayOf("240","C30","C70","S40","S60","S60 Cross Country","S80","S90","Serie 40","Serie 400","Serie 70",
        "Serie 800","Serie 900","V40","V40 Cross Country","V50","V60","V60 Cross Country","V70","XC70","V90",
        "V90 Cross Country","XC40","XC60","XC70","XC90","245","Amazon")

    val brandMap= hashMapOf<String,Array<String>>("ABARTH" to abarth, "ALFA ROMEO" to alfaromeo, "ASTON MARTIN" to astonmartin,
        "AUDI" to audi,"BMW" to bmw,"CHEVROLET" to chevrolet,"CHRYSLER" to chrysler, "CITROEN" to citroen,"CORVETTE" to corvette,
        "DACIA" to dacia, "FERRARI" to ferrari, "FIAT" to fiat,"FORD" to ford, "HYUNDAI" to hyundai, "JAGUAR" to jaguard, "JEEP" to jeep,
        "KIA" to kia, "LAMBORGHINI" to lamborghini,"LANCIA" to lancia, "LAND ROVER" to landrover, "MASERATI" to maserati, "MAZDA" to mazda,
        "MERCEDES BENZ" to mercedes, "MITSUBISHI" to mistsubishi, "NISSAN" to nissan, "OPEL" to opel, "PEUGEOT" to peugeot, "PORSCHE" to porsche,
        "RENAULT" to renault, "SEAT" to seat, "SKODA" to skoda, "SMART" to smart, "SUBARU" to subaru, "SUZUKI" to suzuki, "TESLA" to tesla,
        "TOYOTA" to toyota, "VOLKSWAGEN" to volkswagen, "VOLVO" to volvo
    )
}