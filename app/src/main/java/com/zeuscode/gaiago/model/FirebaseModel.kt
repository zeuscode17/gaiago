package com.zeuscode.gaiago.model

import android.net.Uri
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.SignInMethodQueryResult
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.zeuscode.gaiago.ui.User
import com.zeuscode.gaiago.ui.gamification.dailyRewards.DailyReward
import com.zeuscode.gaiago.ui.gamification.luckySpin.Reward
import com.zeuscode.gaiago.ui.gamification.miniGame.MiniGameCar
import com.zeuscode.gaiago.ui.reservation.Reservation
import com.zeuscode.gaiago.ui.license.License
import com.zeuscode.gaiago.ui.vehicles.Vehicle
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

object FirebaseModel {

    private val mAuth : FirebaseAuth = FirebaseAuth.getInstance()
    private val dbRef : DatabaseReference = FirebaseDatabase.getInstance().reference
    private val storRef : StorageReference = FirebaseStorage.getInstance().reference

    private const val totallyFree = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    private const val totallyFull=  "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"


    fun authenticateUser(email: String, pwd: String): Task<AuthResult> {
        return mAuth.signInWithEmailAndPassword(email, pwd)
    }

    fun userIsLogged() : Boolean {
        return mAuth.currentUser !=null
    }

    fun checkNewEmail(email: String): Task<SignInMethodQueryResult> {
        return mAuth.fetchSignInMethodsForEmail(email)
    }

    fun getUid(): String? {
        return mAuth.uid
    }

    fun insertUser(email: String, pwd: String): Task<AuthResult> {
        return mAuth.createUserWithEmailAndPassword(email, pwd)
    }

    fun isEmailVerified(): Boolean {
        return mAuth.currentUser!!.isEmailVerified
    }

    fun sendResetEmail(email: String) {
        mAuth.sendPasswordResetEmail(email)
    }

    fun sendVerificationEmail() {
        mAuth.currentUser!!.sendEmailVerification()
    }

    fun signOut() {
        mAuth.signOut()
    }

    fun deleteUser(): Task<Void>? {
        dbRef.child("users").child(getUid()!!).removeValue()
        return mAuth.currentUser?.delete()
    }

    fun fetchUser() : Query{
        return dbRef.child("users").child(getUid()!!)
    }

    fun fetchUserByID(userID:String) : Query {
        return dbRef.child("users").child(userID)
    }

    fun fetchScoreByID(userID: String) : Query {
        return dbRef.child("users").child(userID).child("score")
    }

    fun setScoreByID(userID: String, score:Int) {
        dbRef.child("users").child(userID).child("score").setValue(score)
    }

    fun fetchVehicle(): Query {
        return dbRef.child("vehicles")
            .orderByChild("owner")
            .equalTo(getUid())
    }

    fun fetchVehicleByID(vehicleID: String) : Query {
        return dbRef.child("vehicles/$vehicleID")
    }

    fun fetchSingleAvailability(vehicleID: String, date:String) : Query {
        return dbRef.child("vehicles/$vehicleID/availability/$date")
    }

    fun pushVehicleKey() : String {
        return dbRef.child("vehicles").push().key!!
    }

    fun insertVehicle(vehicle: Vehicle): Task<Void> {
        return dbRef.child("vehicles").child(vehicle.vehicleID).setValue(vehicle)
    }

    fun vehicleImageUpload(vehicleID:String): StorageReference {
        return storRef.child("images/vehicles/$vehicleID")
    }

    fun deleteVehicle(key:String):Task<Void> {
        return dbRef.child("vehicles/$key").removeValue()
    }

    fun addVehicleAvailability(vehicleID: String, date:String, availability:String) : Task<Void> {
        return dbRef.child("vehicles/$vehicleID/availability/$date")
            .setValue(availability)
    }

    fun addRepeatedAvailability(vehicleID:String, days:ArrayList<String>, availability: String) {

        for (i in days) {
            dbRef.child("vehicles/$vehicleID/availability/$i")
                .setValue(availability)
        }
    }

    fun fetchPersonalReservations() : Query {
        return dbRef.child("reservations")
            .orderByChild("userID")
            .equalTo(getUid())
    }

    fun fetchOthersReservations() : Query {
        return dbRef.child("reservations")
            .orderByChild("ownerID")
            .equalTo(getUid())
    }

    fun saveUser(user: User) {
        dbRef.child("users").child(getUid()!!).setValue(user)
    }

    fun updateNameSurname(name:String, surname:String){
        dbRef.child("users").child(getUid()!!)
            .child("name").setValue(name)
        dbRef.child("users").child(getUid()!!)
            .child("surname").setValue(surname)
    }

    fun updateEmail(email:String) {
        dbRef.child("users").child(getUid()!!)
            .child("email").setValue(email)
    }

    fun updatePhoneNumber(phoneNumber : String) {
        dbRef.child("users").child(getUid()!!)
            .child("phoneNumber").setValue(phoneNumber)
    }

    fun updateBirthDate(birthDate:Long) {
        dbRef.child("users").child(getUid()!!)
            .child("birthDate").setValue(birthDate)
    }

    fun updateAddress(address: String) {
        dbRef.child("users").child(getUid()!!)
            .child("address").setValue(address)
    }

    fun saveFrontLicensePicture(frontPicturePath: String) : UploadTask {
        val frontImageRef = storRef.child("images/licenses/${getUid()}/front")
        return frontImageRef.putFile(Uri.fromFile(File(frontPicturePath)))
    }

    fun saveBackLicensePicture(backPicturePath: String) : UploadTask {
        val frontImageRef = storRef.child("images/licenses/${getUid()}/back")
        return frontImageRef.putFile(Uri.fromFile(File(backPicturePath)))
    }

    fun saveLicense(license: License) {
        dbRef.child("users/${getUid()}/license").setValue(license)
    }

    fun fetchAvailableVehicles(bookingDate : String) : Query {
        return dbRef.child("vehicles")
            .orderByChild("availability/$bookingDate")
            .startAt(totallyFree)
            .endAt(totallyFull)
    }

    fun insertReservation(reservation: Reservation, vehicleID:String, newAvailability:String) {

        val key= dbRef.child("reservations").push().key
        reservation.key=key!!

        dbRef.child("reservations/$key").setValue(reservation)

        dbRef.child("vehicles/$vehicleID/availability/${reservation.date}").setValue(newAvailability)

        //TODO Si potrebbe separare i set value e aggiungere dei listener sul presenter
    }

    fun fetchUsersByScore(): Query {
        return dbRef.child("users").orderByChild("score")
    }

    fun fetchVehicleAvailabilities(vehicleID: String) : Query {
        return dbRef.child("vehicles/$vehicleID/availability")
    }

    fun fetchPrizes() : Query {
        return dbRef.child("prizesDB")
    }

    fun fetchMilestoneStat() : Query {
        return dbRef.child("users").child(getUid()!!).child("milestoneStat")
    }

    fun fetchUserScore() : Query {
        return dbRef.child("users").child(getUid()!!).child("score")
    }

    fun setMilestoneStat(list:String) {
        dbRef.child("users").child(getUid()!!).child("milestoneStat").setValue(list)
    }

    fun fetchDailyRewardsTime() : Query {
        return dbRef.child("users/${getUid()}/dailyReward")
    }

    fun getDailyRewardIndex() : Query {
        return dbRef.child("users/${getUid()}/dailyReward/index")
    }

    fun updateDailyReward(reward:DailyReward) {
        dbRef.child("users/${getUid()}/dailyReward/index").setValue(reward.index)
        dbRef.child("users/${getUid()}/dailyReward/ritiro").setValue(reward.isRitiro)
    }

    fun saveUserReward(save:Int) {

        val rewardId = dbRef.child("users/${getUid()}/userRewards").push().key.toString()
        val reward = Reward(save, rewardId)
        dbRef.child("users/${getUid()}/userRewards").child(rewardId).setValue(reward)
    }

    fun saveTimerStart() {
        val c = Calendar.getInstance()
        c.add(Calendar.DAY_OF_MONTH, 1)
        c.set(Calendar.HOUR_OF_DAY, 0)
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.SECOND, 0)
        c.set(Calendar.MILLISECOND, 0)

        dbRef.child("users/${getUid()}/dailyReward/timer").setValue(c.timeInMillis)
    }

    fun editProgressBar(progress:String) {
        dbRef.child("users").child(getUid()!!).child("progressbar").setValue(progress)
    }
    fun fetchProgressBar() : Query {
        return dbRef.child("users").child(getUid()!!).child("progressbar")
    }

    fun fetchProgressBarById(userID:String) : Query {
        return dbRef.child("users").child(userID).child("progressbar")
    }

    fun editProgressBarByID(userID:String,progress:String) {
        dbRef.child("users").child(userID).child("progressbar").setValue(progress)
    }

    fun editReservationStatus(key:String, status:Int) : Task<Void> {
        return dbRef.child("reservations/$key/state").setValue(status)
    }

    fun fetchReservationStatus(key:String) : Query {
        return dbRef.child("reservations/$key/state")
    }

    fun addUserRating(userID:String, rating:Float, numRating:Int) {
        dbRef.child("users").child(userID).child("rating").setValue(rating)
        dbRef.child("users").child(userID).child("numRating").setValue(numRating)
    }

    fun addVehicleRating(vehicleID: String, rating:Float, numRating:Int) {
        dbRef.child("vehicles").child(vehicleID).child("rating").setValue(rating)
        dbRef.child("vehicles").child(vehicleID).child("numRating").setValue(numRating)
    }

    fun editUserStatus(reservationID:String, status:Int) {
        dbRef.child("reservations/$reservationID/userState").setValue(status)
    }

    fun editOwnerStatus(reservationID:String, status:Int) {
        dbRef.child("reservations/$reservationID/ownerState").setValue(status)
    }

    fun fetchReservationsByVehicleID(vehicleID: String) : Query  {
        return dbRef.child("reservations")
            .orderByChild("vehicleID")
            .equalTo(vehicleID)
    }

    fun updateMiniGameCar(miniGameCar: MiniGameCar) {
        dbRef.child("users").child(getUid()!!).child("minigamecar").setValue(miniGameCar)
    }

    fun fetchMiniGameCar() : Query {
        return dbRef.child("users").child(getUid()!!).child("minigamecar")
    }
}
