package com.zeuscode.gaiago.ui.authentication.validators

import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when` as whenever

class CompositeValidatorTest {

    companion object {
        private val ANY_VALIDATION_ERROR = ValidationError.EMPTY_EMAIL
    }

    private lateinit var firstValidator: Validator<Any>
    private lateinit var secondValidator: Validator<Any>
    private lateinit var compositeValidator: CompositeValidator<Any>

    @Before
    fun setUp() {
        firstValidator = mock()
        secondValidator = mock()
        compositeValidator = CompositeValidator(arrayOf(firstValidator, secondValidator))
    }


    @Test
    fun returnNullWhenValidatorsNotReturnErrors() {
        whenever(firstValidator.validate(Any())).thenReturn(null)
        whenever(secondValidator.validate(Any())).thenReturn(null)

        val error = compositeValidator.validate(Any())

        assertNull(error)
    }

    @Test
    fun returnValidationErrorWhatReturnsFromFirstValidator() {
        val anyItem = Any()

        whenever(firstValidator.validate(anyItem)).thenReturn(ANY_VALIDATION_ERROR)
        whenever(secondValidator.validate(anyItem)).thenReturn(null)

        val error = compositeValidator.validate(anyItem)

        assertEquals(ANY_VALIDATION_ERROR, error)
    }

    @Test
    fun returnValidationErrorWhatReturnsFromSecondValidator() {
        val anyItem = Any()

        whenever(firstValidator.validate(anyItem)).thenReturn(null)
        whenever(secondValidator.validate(anyItem)).thenReturn(ANY_VALIDATION_ERROR)

        val error = compositeValidator.validate(anyItem)

        assertEquals(ANY_VALIDATION_ERROR, error)
    }

    @Test
    fun returnOnlyFirstErrorReturned() {
        val anyItem = Any()

        val firstError = ValidationError.EMPTY_EMAIL
        val secondError = ValidationError.EMPTY_PASSWORD

        whenever(firstValidator.validate(anyItem)).thenReturn(firstError)
        whenever(secondValidator.validate(anyItem)).thenReturn(secondError)

        val error = compositeValidator.validate(anyItem)

        assertEquals(firstError, error)
    }

}