package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.User
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class SurnameValidatorTest {
    private val name = "Mario"
    private val validator = SurnameValidator()

    @Test
    fun valid() {
        val user = User(name, "Rossi", "any@email.com")

        assertNull(validator.validate(user))
    }

    @Test
    fun emptyName() {
        val user = User(name, "", "any@email.com")

        assertEquals(ValidationError.EMPTY_SURNAME, validator.validate(user))
    }
}