package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.User
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class NameValidatorTest {
    private val surname = "Rossi"
    private val validator = NameValidator()

    @Test
    fun valid() {
        val user = User("Mario", surname, "any@email.com")

        assertNull(validator.validate(user))
    }

    @Test
    fun emptyName() {
        val user = User("", surname, "any@email.com")

        assertEquals(ValidationError.EMPTY_NAME, validator.validate(user))
    }
}