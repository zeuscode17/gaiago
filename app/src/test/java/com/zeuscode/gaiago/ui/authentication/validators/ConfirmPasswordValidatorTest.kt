package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.authentication.Credentials
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class ConfirmPasswordValidatorTest {

    @Test
    fun returnNullWhenPasswordAreTheSame() {
        val validator = ConfirmPasswordValidator()

        assertNull(validator.validate(Credentials("anyEmail", "password", "password")))
    }

    @Test
    fun returnNotMatchingPasswordValidationErrorWhenPasswordsAreNotTheSame() {
        val validator = ConfirmPasswordValidator()

        assertEquals(ValidationError.NOT_MATCHING_PASSWORD, validator.validate(Credentials("anyEmail", "password", "different")))
    }
}