package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.authentication.Credentials
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class AuthenticationPasswordValidatorTest {

    companion object {
        private const val ANY_USERNAME = "anyUsername"
    }

    @Test
    fun validPassword() {
        val validator = AuthenticationPasswordValidator()

        assertNull(validator.validate(Credentials(ANY_USERNAME, "validPassword")))
    }

    @Test
    fun emptyPassword() {
        val validator = AuthenticationPasswordValidator()

        assertEquals(ValidationError.EMPTY_PASSWORD, validator.validate(Credentials(ANY_USERNAME, "")))
    }
}

