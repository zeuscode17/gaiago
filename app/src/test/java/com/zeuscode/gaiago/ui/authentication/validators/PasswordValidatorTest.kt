package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.authentication.Credentials
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class PasswordValidatorTest {
    companion object {
        private const val USERNAME = "anyUsername"
        private const val CONFIRM_PASSWORD = "anyPassword"
    }

    @Test
    fun returnNullWhenPasswordIsValid() {
        val validator = PasswordValidator()

        val validationError = validator.validate(Credentials(USERNAME, "Valid!1", CONFIRM_PASSWORD))

        assertNull(validationError)
    }

    @Test
    fun returnEmptyPasswordValidationErrorWhenPasswordIsEmpty() {
        val validator = PasswordValidator()

        val validationError = validator.validate(Credentials(USERNAME, "", CONFIRM_PASSWORD))

        assertEquals(ValidationError.EMPTY_PASSWORD, validationError)
    }

    @Test
    fun returnPasswordNotValidValidationErrorWhenPasswordIsTooShort() {
        val validator = PasswordValidator()

        val validationError = validator.validate(Credentials(USERNAME, "sho1!", CONFIRM_PASSWORD))

        assertEquals(ValidationError.PASSWORD_NOT_VALID, validationError)
    }

    @Test
    fun returnPasswordNotValidValidationErrorWhenPasswordNotContainsAtLeastOneUpperCaseLetter() {
        val validator = PasswordValidator()

        val validationError = validator.validate(Credentials(USERNAME, "password1!", CONFIRM_PASSWORD))

        assertEquals(ValidationError.PASSWORD_NOT_VALID, validationError)
    }

    @Test
    fun returnPasswordNotValidValidationErrorWhenPasswordNotContainsAtLeastOneLoverCaseLetter() {
        val validator = PasswordValidator()

        val validationError = validator.validate(Credentials(USERNAME, "PASSWORD1!", CONFIRM_PASSWORD))

        assertEquals(ValidationError.PASSWORD_NOT_VALID, validationError)
    }

    @Test
    fun returnPasswordNotValidValidationErrorWhenPasswordNotContainsAtLeastOneNumber() {
        val validator = PasswordValidator()

        val validationError = validator.validate(Credentials(USERNAME, "passworD!", CONFIRM_PASSWORD))

        assertEquals(ValidationError.PASSWORD_NOT_VALID, validationError)
    }

    @Test
    fun returnPasswordNotValidValidationErrorWhenPasswordNotContainsAtLeastOneSpecialChar() {
        val validator = PasswordValidator()

        val validationError = validator.validate(Credentials(USERNAME, "passworD1", CONFIRM_PASSWORD))

        assertEquals(ValidationError.PASSWORD_NOT_VALID, validationError)
    }

}