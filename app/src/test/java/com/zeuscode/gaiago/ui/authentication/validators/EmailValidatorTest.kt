package com.zeuscode.gaiago.ui.authentication.validators

import com.zeuscode.gaiago.ui.authentication.Credentials
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class EmailValidatorTest {
    companion object {
        private const val PASSWORD = "anyPassword"
    }

    private val validator = EmailValidator()

    @Test
    fun returnNullWhenValidationIsSuccess() {
        val validationError = validator.validate(Credentials("valid@email.it", PASSWORD, PASSWORD))

        assertNull(validationError)
    }

    @Test
    fun returnEmptyEmailValidationErrorWhenEmailIsEmpty() {
        val validationError = validator.validate(Credentials("", PASSWORD, PASSWORD))

        assertEquals(ValidationError.EMPTY_EMAIL, validationError)
    }

    @Test
    fun returnEmailNotValidWhenEmailIsNotValid() {
        assertValidationOfNotValidEmail("email@it")
        assertValidationOfNotValidEmail("invalid.email")
        assertValidationOfNotValidEmail("invalid.email@")
        assertValidationOfNotValidEmail("@email.com")
        assertValidationOfNotValidEmail("email@.it")
    }

    private fun assertValidationOfNotValidEmail(email: String) {
        assertEquals(ValidationError.EMAIL_NOT_VALID, validator.validate(Credentials(email, PASSWORD, PASSWORD)))
    }

}